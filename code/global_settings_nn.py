"""
Global settings for the neural network
"""

import os

EPOCHS = 300
BATCH_SIZE = 50
VERBOSE = 1

ROOT_DIR = os.path.join(os.getcwd(), "neuralnet")
MODEL_DIR = os.path.join(ROOT_DIR, "Models")
if not os.path.isdir(MODEL_DIR):
    os.makedirs(MODEL_DIR)

NAME = "nn"
#NAME = "nn_point00"
MODELFILE = os.path.join(ROOT_DIR, f"Models/keras_model_{NAME}.h5")
SCALER = os.path.join(ROOT_DIR, f"Models/default_scaler_{NAME}.pkl")
HISTORY = os.path.join(ROOT_DIR, f"Models/loss_history_{NAME}.pkl")

PRED_DIR = os.path.join(ROOT_DIR, "Predictions/{0}".format(NAME))
KDE_DIR = os.path.join(ROOT_DIR, "KDEs/{0}/point12".format(NAME))
OPT_DIR = os.path.join(ROOT_DIR, "Optimization")
TEMPLATE_DIR = os.path.join(ROOT_DIR, "Templates/{0}/point12".format(NAME))

#TEMPLATE_DIR = os.path.join(ROOT_DIR, "Templates/{0}/point00".format(NAME))
