import os

ROOT_DIR = os.path.join(os.getcwd())
DATA_DIR = os.path.join(ROOT_DIR, "data")
TRAIN_DIR = os.path.join(DATA_DIR, "train_data_balanced/point12")
TEST_DIR = os.path.join(DATA_DIR, "test_data_scaled/")
#TEMP_DATA = os.path.join(DATA_DIR, "template_data/point12")
TEMP_DATA = (os.path.join(DATA_DIR, "x_templ.npy"), os.path.join(DATA_DIR, "y_templ.npy"))

#TEMP_DATA = os.path.join(DATA_DIR, "template_data/point00")

SHAPLEY_DIR = os.path.join(ROOT_DIR, "Shapley")
SHAPLEY_VALUES = os.path.join(SHAPLEY_DIR, "values")
SHAPLEY_DATA = os.path.join(SHAPLEY_DIR,"data")


EPOCHS = 2000
BATCH_SIZE = 80

LUMI = 149
BKG = ["ttZ", "ttW", "ZZ", "WZ", "ZWW", "WWW", "WWbb"]
SIGNAL = ["qcd", "ew", "chichi"]
POINTS = ["point00", "point12", "point13", "point14", "point15", "point16",
        "point20", "point30", "point40", "point50"]

FEATURES_TO_USE = ["tau1_pt",
                   "tau1_phi",
                   "tau1_eta",
                   "tau2_pt",
                   "tau2_phi",
                   "tau2_eta",
                   "mu1_pt",
                   "mu1_phi",
                   "mu1_eta",
                   "MET",
                   "MET_phi",
                   "ht",
                   "n_electrons",
                   "n_muons",
                   "n_taus",
                   "n_jets"]


# --- Visual settings
LINESTYLES = ("solid", "dashed")

BLACK = 'black'
GREEN = '#20A387'
YELLOW = '#FDE725'
PURPLE = '#440154'
BLUE = '#39568C'
RED = '#E05263'

def get_color(key):
    if "signal" in key.lower():
        return GREEN
    elif "background" in key.lower() or "xgb" in key.lower():
        return PURPLE
    elif "nn" in key.lower():
        return BLUE
    else: return BLACK

def get_linestyle(key):
    if "background" in key.lower() or "xgb" in key.lower():
        return "dashed"
    elif "signal" in key.lower() or "nn" in key.lower():
        return "solid"
    else: return '.'

COLORS = [GREEN,
          PURPLE,
          BLUE,
          RED,
          YELLOW,
          BLACK]

NBINS = 50

LABELDICT = {"class1_no_qcd"    : "Signal class (no QCD)",
             "class0_no_qcd"    : "Background class (no QCD)",
             "0.0_xgboost"   : "Background (XGBoost)",
             "1.0_xgboost"   : "Signal (XGBoost)",
             "0.0_nn"        : "Background (DNN)",
             "1.0_nn"        : "Signal (DNN)",
             "class0_nn" : "Background (DNN)",
             "class1_nn" : "Signal (DNN)",
             "class0_xg" : "Background (XGB)",
             "class1_xg" : "Signal (XGB)",
             "0.0"              : r"$p_{\mathrm{bkg}}(y)$",
             "1.0"              : r"$p_{\mathrm{s}}(y)$",
             0.0                : r"$p_{\mathrm{bkg}}(y)$",
             1.0                : r"$p_{\mathrm{s}}(y)$",
             "n_jets"       : r"$n_{jet}$",
             "n_taus"       : r"$n_{\tau}$",
             "tau1_pt"      : r"$p_T^{\tau_1}$",
             "tau1_phi"     : r"$\phi^{\tau_1}$",
             "tau1_eta"     : r"$\eta^{\tau_1}$",
             "tau1_charge"  : r"$Q^{\tau_1}$",
             "tau2_pt"      : r"$p_T^{\tau_2}$",
             "tau2_phi"     : r"$\phi^{\tau_2}$",
             "tau2_eta"     : r"$\eta^{\tau_2}$",
             "tau2_charge"  : r"$Q^{\tau_2}$",
             "mu1_pt"       : r"$p_T^{\mu}$",
             "mu1_phi"      : r"$\phi^{\mu}$",
             "mu1_eta"      : r"$\eta^{\mu}$",
             "mu1_charge"   : r"$Q^{\mu}$",
             "n_muons"      : r"$n_{\mu}$",
             "n_electrons"  : r"$n_{e}$",
             "MET"          : r"$p_T^{\mathrm{miss}}$",
             "MET_phi"      : r"$\phi^{\mathrm{miss}}$",
             "ht"           : r"$H_T$"
            }
