"""
Global settings for XGBoost
"""

import os

ROOT_DIR = os.path.join(os.getcwd(), "xgboost_model")
MODEL_DIR = os.path.join(ROOT_DIR, "Models")
if not os.path.isdir(MODEL_DIR):
    os.makedirs(MODEL_DIR)

NAME = "xg"
MODELFILE = os.path.join(MODEL_DIR, f"xgboost_model_{NAME}.dat")
PRED_DIR = os.path.join(ROOT_DIR, f"Predictions/{NAME}")
TEMPLATE_DIR = os.path.join(ROOT_DIR, f"Templates/{NAME}")
KDE_DIR = os.path.join(ROOT_DIR, f"KDEs/{NAME}")
OPT_DIR = os.path.join(ROOT_DIR, f"OPT/{NAME}")
