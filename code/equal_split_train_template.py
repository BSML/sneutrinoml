import pickle, sys
import numpy as np
import scipy.optimize
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from xgboost import XGBClassifier, plot_importance
import tensorflow as tf
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
import os

from global_settings import (TRAIN_DIR, TEST_DIR, TEMP_DATA, FEATURES_TO_USE,
                             LABELDICT)
from plots import plot_predictions
from MLE_fit import kdes_from_arrays, nll, make_loglike_curve, sigmas_and_alphas
from utilities import get_dataset_from_path, make_templates, scale



if __name__ == '__main__':

    # Limit CPU usage
    os.environ["OMP_NUM_THREADS"] = "8"
    tf.config.threading.set_inter_op_parallelism_threads(1) 
    tf.config.threading.set_intra_op_parallelism_threads(1)


    do_split = False # If false, loads data from npy files
    do_train = False # If false, loads model from disc
    do_kde = True #False
    do_mle = True #False
    do_plot = False

    if do_split:
        x_data_train, y_data_train, _ = get_dataset_from_path(TRAIN_DIR)
        x_data_templ, y_data_templ, _ = get_dataset_from_path(TEMP_DATA)
        x_data = np.concatenate((x_data_train, x_data_templ))
        y_data = np.concatenate((y_data_train, y_data_templ))

        x_train, x_val, y_train, y_val = train_test_split(
            x_data, y_data, test_size=0.5)

        x_templ = x_val.copy()
        y_templ = y_val.copy()


        np.save('x_train.npy', x_train)
        np.save('y_train.npy', y_train)
        np.save('x_templ.npy', x_templ)
        np.save('y_templ.npy', y_templ)
        np.save('x_val.npy', x_val)
        np.save('y_val.npy', y_val)

        print('x_train.shape:', x_train.shape)
        print('x_templ.shape:', x_templ.shape)
        print('x_val.shape:', x_val.shape)
        print('np.sum(y_templ) =', np.sum(y_templ))

    if do_train:

        x_train, y_train = np.load('x_train.npy'), np.load('y_train.npy')
        x_val, y_val = np.load('x_val.npy'), np.load('y_val.npy')

        # Train xgboost
        model = XGBClassifier(max_depth=10, n_estimators=500, objective="binary:logistic")

        eval_set = [(x_train, y_train), (x_val, y_val)]
        model.fit(x_train, y_train,eval_metric="logloss", eval_set=eval_set,
            verbose=False,early_stopping_rounds=50)

        results = model.evals_result()

        y_pred = model.predict(x_val)

        predictions = [round(_value) for _value in y_pred]

        # --- Evaluate predictions
        accuracy = accuracy_score(y_val, predictions)

        print("Accuracy: %.2f%%" % (accuracy * 100.0))

        # --- Save model
        modelfile = 'steffen-model.xgb'
        pickle.dump(model, open(modelfile, "wb"))
        print("Saved model to ", modelfile)

        # --- Get performance metrics
        epochs = len(results['validation_0']['logloss'])
        x_axis = range(0, epochs)

        # --- Plot log loss
        if do_plot:
            fig, ax = plt.subplots()
            ax.plot(x_axis, results['validation_0']['logloss'], label='Train')
            ax.plot(x_axis, results['validation_1']['logloss'], label='Test')
            ax.legend()
            plt.ylabel('Log Loss')
            plt.title('XGBoost Log Loss')
            plt.show()

    if do_kde:

        tf.config.set_visible_devices([], 'GPU')

        x_templ, y_templ = np.load('data/x_templ.npy'), np.load('data/y_templ.npy')
        # Create templates
        x_templ_signal = x_templ[y_templ == 1]
        x_templ_bgnd = x_templ[y_templ == 0]

        #model, mname = pickle.load(open('xgb-model.xgb', "rb")), "xg"
        model, mname = load_model("neuralnet/Models/keras_model_nn.h5"), "nn"

        if isinstance(model, XGBClassifier):
            pred_signal = model.predict_proba(x_templ_signal)[:, 1]
            pred_bgnd = model.predict_proba(x_templ_bgnd)[:, 1]
        else:
            scalerfile = ("neuralnet/Models/default_scaler_nn.pkl")
            x_templ_signal = scale(x_templ_signal, scalerfile)
            x_templ_bgnd = scale(x_templ_bgnd, scalerfile)
            pred_signal = model.predict(x_templ_signal)
            pred_bgnd = model.predict(x_templ_bgnd)

        kde_signal, kde_bgnd = kdes_from_arrays([pred_signal, pred_bgnd],
                plot=do_plot, labels=['Signal', 'Bgnd'])
        if do_plot:
            plt.show()


    if do_mle:

        for pt in [
            'point00',
            'point12',
            'point13',
            'point14',
            'point15',
            'point16',
            'point20',
            'point30',
            'point40',
            'point50'
            "point15_signal",
        ]:

            print('-----------------', pt)

            # --- Load scaled test data
            x_data_test, y_data_test, _ = get_dataset_from_path('data/test_data_scaled/'+pt)

            num_signal = np.sum(y_data_test)
            true_alpha = num_signal/len(y_data_test)
            #print('true alpha:', true_alpha)

            if isinstance(model, XGBClassifier):
                preds = model.predict_proba(x_data_test)[:, 1]
            else:
                scalerfile = ("neuralnet/Models/default_scaler_nn.pkl")
                preds = scale(x_data_test, scalerfile)
                preds = model.predict(preds)

            plt.hist(preds, bins=80, histtype="step")
            plt.show()

            _nllsdir = os.path.join("nlls", mname)
            fitted_alpha_file = os.path.join(_nllsdir, f"opt_alpha_{pt}.pkl")
            if not os.path.isdir(_nllsdir):
                os.makedirs(_nllsdir)

            if os.path.exists(fitted_alpha_file):
                with open(fitted_alpha_file, 'rb') as _f:
                    FITTED_ALPHA = pickle.load(_f)

            else:
                #print('optimizing...')
                OPT_ALPHA = scipy.optimize.minimize(nll, 0.1, args=([kde_bgnd, kde_signal], preds),
                                                        bounds=((0.0001, 0.999),))

                print("Result from scipy minimization of mixture parameter:")
                print(OPT_ALPHA)

                FITTED_ALPHA = OPT_ALPHA.x[0]
                with open(fitted_alpha_file, 'wb') as _f:
                    pickle.dump(FITTED_ALPHA, _f)

            print('\ntrue alpha:', true_alpha, 'est alpha:', FITTED_ALPHA, '\n')


            _DOWN = (np.logspace(0,np.log10(11),50)-1)/10*FITTED_ALPHA
            _UP =  np.logspace(np.log10(FITTED_ALPHA),np.log10(0.3))
            TEST_ALPHAS = np.append(_DOWN, _UP)
            make_loglike_curve(FITTED_ALPHA, TEST_ALPHAS, [kde_bgnd, kde_signal], preds, pt, mname)
            sigmas_and_alphas()

            if do_plot:
                plt.xlim([-0.07, 0.2])
                plt.ylim([-0.5, 30])
                plt.xlabel(r"Mixture parameter $\alpha$")
                plt.ylabel(r"$-2\mathrm{log}\mathcal{L}$")
                plt.legend(loc="upper left")

                plt.show()

