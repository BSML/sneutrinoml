import sys
import re
import os
import warnings
from argparse import ArgumentParser
import pickle
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from tensorflow.keras.models import load_model

from utilities import scale, get_dataset_from_path
from global_settings_xg import MODELFILE as XGBoost_MODELFILE
from global_settings_nn import MODELFILE as NN_MODELFILE
from global_settings_nn import SCALER
from global_settings import (TRAIN_DIR, TEST_DIR, TEMP_DATA, TUNING_DIR, FEATURES_TO_USE,
                             LABELDICT, ROOT_DIR)

from plots import plot_predictions

def cut_analyis(tune_datadir,
                test_datadir,
                model,
                scalerfile = None,
                plot=False,
                thres=0,
                s_scale=1.):

    x_tune, y_tune,_ = get_dataset_from_path(tune_datadir)
    x_test, y_test,_ = get_dataset_from_path(test_datadir)

    if scalerfile is not None:
        x_tune = scale(x_tune, scalerfile)
        x_test = scale(x_test, scalerfile)

    if PARGS.xgboost:
        y_pred_tune = model.predict_proba(x_tune)[:,1]
        y_pred_test = model.predict_proba(x_test)[:,1]
    else:
        y_pred_tune = model.predict(x_tune).reshape(-1)
        y_pred_test = model.predict(x_test).reshape(-1)

    if thres == 0:
        opt_thres = 0.0
        max_sign = 0
        S_opt = 0
        B_opt = 0


        for i in np.linspace(0.1,1.0,10000):
            ii = i
            threshold_func = np.vectorize(lambda x: 0 if x<ii else 1)
            y_pred_int = threshold_func(y_pred_tune)

            y_tune_1 = y_tune[np.where(y_pred_int==1)]

            S = float(y_tune_1.sum())
            B = float(y_tune_1.size - S)

            #S = S/30

            # --- Verify that number of signal or background events is sufficient
            if B < 50: continue
            if S < 10: continue

            significance = np.sqrt(2*((S+B)*np.log((S+B)/B)-S))

            if significance > max_sign:
                max_sign = significance
                opt_thres = ii
                S_opt = S
                B_opt = B

        if plot:
            plot_predictions(y_tune, y_pred_tune, show=False)


            plt.axvline(x=opt_thres, label="Optimal cutoff", color="red",
                    linestyle='--')

            plt.title("")
            plt.legend()#loc='upper center')
            plt.show()

        print("From tuning process (using tuning dataset):")
        print("\tMax significance (from tuning set): " + str(max_sign))
        print("\tBest Threshold: " + str(opt_thres))
        print("\tS: " + str(S_opt))
        print("\tB: " + str(B_opt))
        print("")
    else:
        print("Using threshold from input: " + str(thres))
        opt_thres = thres 
       

    threshold_func = np.vectorize(lambda x: 0 if x<opt_thres else 1)
    y_pred_int = threshold_func(y_pred_test)

    y_test_1 = y_test[np.where(y_pred_int==1)]
    
    S = float(y_test_1.sum())
    B = float(y_test_1.size - S)

    # Scaling
    if s_scale < 0:
        # Scaling S in term of reference point
        S_before_12 = 521.8 # Expected yield for point 12 with 149fb^-1
        S_before_X = y_test.sum()
        S = S_before_12 * S / S_before_X
    else:
        S = S/s_scale


    significance = np.sqrt(2*((S+B)*np.log((S+B)/B)-S))

    if plot:
        plot_predictions(y_test, y_pred_test, show=False)


        plt.axvline(x=opt_thres, label="Optimal cutoff", color="red",
                linestyle='--')

        plt.title("")
        plt.legend()#loc='upper center')
        plt.show()
    
    

    print("Apply optimized cut on test set:")
    print("\tSignificance: " + str(significance))
    print("\tS: " + str(S))
    print("\tB: " + str(B))

class Range(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
    
    def __eq__(self, other):
        return self.start <= other <= self.end
    
    def __contains__(self, item):
        return self.__eq__(item)

    def __iter__(self):
        yield self

if __name__ == '__main__':

    plt.style.use("sneu.mplstyle")

    PARSER = ArgumentParser(description="Cut and count on classifier output")

    PARSER.add_argument('-plot', "--plot", action="store_true", default=False,
                        help="Plot result.")

    PARSER.add_argument('-cut', '--cut', action="store_true",
                        help='Evaluate network')

    PARSER.add_argument('-xgboost', '--xgboost', action="store_true",
                        help='Evaluate network')

    PARSER.add_argument('-nn', '--nn', action="store_true",
                        help='Evaluate network')

    PARSER.add_argument('-point', '--point', default=00,
                        help="Which point to test on? (Training is always on "\
                                "point00). Input format: 00, 01, ..., 11, .., 19")
    
    PARSER.add_argument('-threshold', '--threshold', default=0., type=float, choices=Range(0.0, 1.0),
                        help="Set the cut threshold, if not specified optimal"\
                             "threshold will be found from the validation set.")
    
    PARSER.add_argument('-scale', '--scale', default=1,
                        help="Scale number of signal events")
    
    PARSER.add_argument('-scalep', '--scalep', default=0,
                        help="Scale number of signal events after a specific"\
                             "points expected yield")

    PARGS = PARSER.parse_args()

    POINT = f"point{PARGS.point}"
    TEST_DIR = os.path.join(TEST_DIR, POINT)

    PLOT = PARGS.plot

    if PARGS.xgboost:
        MODEL = pickle.load(open(XGBoost_MODELFILE, "rb"))
        SCALER = None
    elif PARGS.nn:
        MODEL = load_model(NN_MODELFILE)
    else:
        assert False,"Must define either -xgboost or -nn as arguments to know which modelfile to use"

    OPTIMAL_THRESHOLD = PARGS.threshold

    SCALE = PARGS.scale
    if PARGS.scalep != 0:
        SCALE = -PARGS.scalep

    if PARGS.cut:
        cut_analyis(TUNING_DIR, TEST_DIR, MODEL, SCALER, PLOT, OPTIMAL_THRESHOLD, SCALE)
