import pickle, sys
import numpy as np
from xgboost import XGBClassifier, plot_importance
import tensorflow as tf
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
import os

#from utilities import select_features

from global_settings import FEATURES_TO_USE
from plots import plot_predictions
#from MLE_fit import kdes_from_arrays, nll, make_loglike_curve, sigmas_and_alphas
from utilities import get_dataset_from_files, scale


plt.style.use("sneu.mplstyle")


variations_ew_point12 = {
    'CT14': ['data/variations/point12/ct14.h5'],
    'NNPDF': ['data/variations/point12/nnpdf.h5'],
    'NOM': ['data/variations/point12/nom.h5'],
    'SCALE05': ['data/variations/point12/scale05.h5'],
    'SCALE20': ['data/variations/point12/scale20.h5']
}

variations_chichi_point12 = {
    'CT14': ['data/variations/point12/ct14_chi.h5'],
    'NNPDF': ['data/variations/point12/nnpdf_chi.h5'],
    'NOM': ['data/variations/point12/nom_chi.h5'],
    'SCALE05': ['data/variations/point12/scale05_chi.h5'],
    'SCALE20': ['data/variations/point12/scale20_chi.h5']
}

variations_combined_point12 = {
    'CT14': [
        'data/variations/point12/ct14.h5',
        'data/variations/point12/ct14_chi.h5',
    ],
    'NNPDF': [
        'data/variations/point12/nnpdf.h5',
        'data/variations/point12/nnpdf_chi.h5',
    ],
    'NOM': [
        'data/variations/point12/nom.h5',
        'data/variations/point12/nom_chi.h5',
    ],
    'SCALE05': [
        'data/variations/point12/scale05.h5',
        'data/variations/point12/scale05_chi.h5',
    ],
    'SCALE20': [
        'data/variations/point12/scale20.h5',
        'data/variations/point12/scale20_chi.h5',
    ]
}


variations_ew_point00 = {
    'CT14': ['data/variations/point00/ct14.h5'],
    'NNPDF': ['data/variations/point00/nnpdf.h5'],
    'NOM': ['data/variations/point00/nom.h5'],
    'SCALE05': ['data/variations/point00/scale05.h5'],
    'SCALE20': ['data/variations/point00/scale20.h5']
}

variations_chichi_point00 = {
    'CT14': ['data/variations/point00/ct14_chi.h5'],
    'NNPDF': ['data/variations/point00/nnpdf_chi.h5'],
    'NOM': ['data/variations/point00/nom_chi.h5'],
    'SCALE05': ['data/variations/point00/scale05_chi.h5'],
    'SCALE20': ['data/variations/point00/scale20_chi.h5']
}

variations_combined_point00 = {
    'CT14': [
        'data/variations/point00/ct14.h5',
        'data/variations/point00/ct14_chi.h5',
    ],
    'NNPDF': [
        'data/variations/point00/nnpdf.h5',
        'data/variations/point00/nnpdf_chi.h5',
    ],
    'NOM': [
        'data/variations/point00/nom.h5',
        'data/variations/point00/nom_chi.h5',
    ],
    'SCALE05': [
        'data/variations/point00/scale05.h5',
        'data/variations/point00/scale05_chi.h5',
    ],
    'SCALE20': [
        'data/variations/point00/scale20.h5',
        'data/variations/point00/scale20_chi.h5',
    ]
}





def make_plot(point, signaltype, modeltype):

    os.environ["OMP_NUM_THREADS"] = "8"
    tf.config.threading.set_inter_op_parallelism_threads(1) 
    tf.config.threading.set_intra_op_parallelism_threads(1)

    tf.config.set_visible_devices([], 'GPU')

    out_file_name = 'signal_variation_comparison_' + point + '_' + modeltype + '_' + signaltype

    if modeltype == 'xg':
        model = pickle.load(open('steffen-model.xgb', "rb"))
    elif modeltype == 'nn':
        model = load_model("neuralnet/Models/keras_model_nn.h5")

    scalerfile = ("neuralnet/Models/default_scaler_nn.pkl")

    fig, (ax1, ax2) = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios': [2, 1]})

    if point == 'point12' and signaltype == 'ew':
        variations = variations_ew_point12
    elif point == 'point12' and signaltype == 'chichi':
        variations = variations_chichi_point12
    elif point == 'point12' and signaltype == 'combined':
        variations = variations_combined_point12
    elif point == 'point00' and signaltype == 'ew':
        variations = variations_ew_point00
    elif point == 'point00' and signaltype == 'chichi':
        variations = variations_chichi_point00
    elif point == 'point00' and signaltype == 'combined':
        variations = variations_combined_point00


    counts, bins = {}, {}
    for name, fin in variations.items():

        x_data, y_data, _ = get_dataset_from_files(fin, [0]*len(fin))

        print('x_data.shape:', x_data.shape)

        if isinstance(model, XGBClassifier):
            preds = model.predict_proba(x_data)[:, 1]
        else:
            preds = scale(x_data, scalerfile)
            preds = model.predict(preds)

        histcounts, histbins = np.histogram(preds, bins=60, range=(0,1), density=True)
        counts[name] = histcounts
        bins[name] = histbins

    for name in counts:

        # Main histogram
        ax1.stairs(counts[name], bins[name], label=name)

        # Difference subplot
        diff = (counts[name] - counts['NOM'])/counts[name]
        ax2.stairs(diff, bins[name], label=name)

    plt.subplots_adjust(left=0.175, right=0.97, top=0.99, bottom=0.15)

        
    if modeltype == 'nn':
        plt.xlabel(r"NN output")
    elif modeltype == 'xg':
        plt.xlabel(r"XGBoost output")
    plt.xlim((0,1))
    ax2.set_ylim((-0.15,0.15))
    ax1.set_ylabel(r"Normalised events")
    ax2.set_ylabel(r"Relative difference")

    if point == 'point12':
        ax1.legend(loc="upper left", ncol=2)
    elif point == 'point00':
        ax1.legend(loc="lower center", ncol=2)
        
    plt.savefig(out_file_name + '.pdf')
    plt.savefig(out_file_name + '.png')

    print('Saved', out_file_name, '(.png/.pdf)')


if __name__ == '__main__':

    sigtypes = ['combined'] #, 'ew', 'chichi']
    modtypes = ['nn', 'xg']
    points = ['point12', 'point00']
    
    for pt in points:
        for st in sigtypes:
            for mt in modtypes:
                make_plot(pt, st, mt)

