"""
Functions for file operations on .h5 datafiles.
If run as main file: run with -h for argument options.
"""

import re
import sys
import os
import shutil
import itertools
import numpy as np
import h5py


def extract_samples_from_dataset(datafile, n_samples, target_dir='.'):
    """
    Extract (destructively) events from a datafile and saves the modified one
    as well as the new one.

    Input:
        datafile: Path and file name to .h5 file containing data
        n_events: Integer specifying how many events to extract

    Return:
        Resulting filenames
    """

    assert os.path.isfile(datafile), "File not found: {0}".format(datafile)
    assert isinstance(n_samples, (int, float))

    n_samples = int(n_samples)

    hf = h5py.File(datafile, 'r')

    try:
        data = hf["data"]
    except IOError:
        print("Are you sure that the file exists and that it's not open in "\
                "IPython somewhere?")
        sys.exit()

    # --- Make numpy array from the data and shuffle it
    np_data = np.array(data)
    np.random.shuffle(np_data)

    original_size = np_data.shape[0]
    assert original_size >= n_samples, "Not enough samples in {0} to extract "\
            "{1} samples".format(datafile, n_samples)

    feats = np_data.shape[1]

    name = os.path.basename(datafile)
    name = re.sub('.h5', '', name)

    # --- Write the extracted data to a new .h5 file
    filename_0 = os.path.join(target_dir, "{0}_{1}.h5".format(name, n_samples))
    filename_1 = os.path.join(target_dir, "{0}_{1}.h5".format(
        name, original_size-n_samples))

    for _filename, _data, _size in zip(
            [filename_0, filename_1],
            [np_data[:n_samples, :], np_data[n_samples:original_size, :]],
            [n_samples, original_size-n_samples]):

        _hf = h5py.File(_filename, 'w')
        _dset = _hf.create_dataset("data", shape=(_size, feats),
                                   data=_data)

        try:
            _dset.attrs["columns"] = data.attrs["columns"]
        except KeyError:
            try:
                _dset.attrs["features"] = data.attrs["features"]
            except KeyError:
                print("Sorry, can't find columns or features in your .h5 file.")
                sys.exit()

        _hf.close()

    return filename_0, filename_1

def find_sizes(files, verbose=False):
    """
    Return number of events in .h5 files
    """

    if not isinstance(files, list):
        files = [files]

    if verbose:
        print("File size")

    sizes = []

    for _file in files:

        assert os.path.isfile(_file), "Cannot find file {0}".format(_file)

        _hf = h5py.File(_file, 'r')
        _data = _hf["data"]
        _size = np.array(_data).shape[0]
        sizes.append(_size)

        if verbose:
            print(_file.split('/')[-1].strip('.h5'), _size)

    if len(sizes) == 1:
        return sizes[0]

    return sizes

def merge_h5_files(files, output_filename="merged", output_dir="merged_files",
                   shuffle=True, verbose=True):
    """
    Merge .h5 files by stacking their data and keeping the "features" header.

    Input:
        files: List of filenames with path

    Returns:
        Number of samples in merged file."
    """

    stacked_data = np.array([])
    features = None

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    if not isinstance(files, list):
        files = [files]

    for _file in files:
        # --- Load data from .h5 file
        hf_fin = h5py.File(_file, 'r')
        data = hf_fin.get("data")

        if features is None:
            features = data.attrs["features"]
            new_features = data.attrs["features"]
        else:
            new_features = data.attrs["features"]

        data = np.array(data)

        if any(features != new_features):
            print("Error: Provided datafiles have different features. Exiting")
            sys.exit()

        # --- Add up the data
        if len(stacked_data):
            assert data.shape[1] == stacked_data.shape[1]
            stacked_data = np.vstack((stacked_data, data))
        else:
            stacked_data = data

    # --- Make numpy array from the data and shuffle it
    np_data = np.array(stacked_data)

    if shuffle:
        np.random.shuffle(np_data)

    # --- Write the shuffled data to another .h5 file
    filename = os.path.join(output_dir, "{0}.h5".format(output_filename))
    hf_sub = h5py.File(filename, 'w')
    dset_sub = hf_sub.create_dataset("data",
                                     shape=np_data.shape,
                                     data=np_data)

    dset_sub.attrs["features"] = features

    hf_sub.close()

    if verbose:
        print("Made file ", filename)

    return len(np_data)

def shuffle_and_cut(datafile, size=10000, output_dir="Scaled_files", n_files=1,
                    verbose=True):
    """
    Shuffle the data inside a .h5 file and cut it at [size] events (default
    10k). Save resulting file as .h5 with the same features.
    If n_files is larger than 1, several .h5 files containing different events
    are made.
    """

    assert os.path.isfile(datafile), "Cannot find file {0}".format(datafile)
    assert isinstance(n_files, (int, float))
    assert isinstance(size, (int, float))
    assert n_files > 0, "Can only make a positive number of files."

    n_files = int(n_files)
    size = int(size)

    hf = h5py.File(datafile, 'r')

    try:
        data = hf["data"]
    except IOError:
        print("Are you sure that the file exists and that it's not open in "\
                "IPython somewhere?")
        sys.exit()

    # --- Make numpy array from the data and shuffle it
    np_data = np.array(data)
    np.random.shuffle(np_data)

    assert size*n_files <= np_data.shape[0], "There are not enough events "\
            "in {0} to make {1} files containing {2} events.".format(datafile,
                                                                     n_files, size)

    feats = np_data.shape[1]

    name = os.path.basename(datafile)
    name = name.strip(".h5")

    for _nfile in range(n_files):
        # --- Write the shuffled data (limited by size parameter) to another .h5 file
        if _nfile == 0:
            filename = os.path.join(output_dir, "{0}_{1}.h5".format(name, size))
        else:
            filename = os.path.join(output_dir, "{0}_{1}_{2}.h5".format(name,
                                                                        size, _nfile))

        start = size*(_nfile)
        stop = size*(_nfile+1)

        hf_sub = h5py.File(filename, 'w')
        dset_sub = hf_sub.create_dataset("data",
                                         shape=(size, feats),
                                         data=np_data[start:stop, :])

        try:
            dset_sub.attrs["columns"] = data.attrs["columns"]
        except KeyError:
            try:
                dset_sub.attrs["features"] = data.attrs["features"]
            except KeyError:
                print("Sorry, can't find columns or features in your .h5 file.")
                sys.exit()
        hf_sub.close()
        if verbose:
            print("Made file ", filename)

if __name__ == "__main__":

    from argparse import ArgumentParser

    PARSER = ArgumentParser(description="Operations on .h5 files")

    PARSER.add_argument('-f', "--files", nargs='*', help="Filename")

    PARSER.add_argument('-d', "--directory", help="Directory")

    PARSER.add_argument('-merge', "--merge", nargs='*',
                        help="Merge all these files")

    PARSER.add_argument('-size', "--size",
                        help="Scale file(s) to this many events. If several "\
                        "files are input, these are merged.")

    PARSER.add_argument("-split", "--split", nargs='*',
            help="Split file into two, the first argument being the filename "\
                    "and the second the number of samples in one of the "\
                    "resulting files")

    PARGS = PARSER.parse_args()

    if PARGS.split:
        FILENAME = PARGS.split[0]
        try:
            N_SAMPLES = int(PARGS.split[1])
        except:
            print("Cannot convert {0} to a number".format(PARGS.split[1]))

        assert os.path.isfile(FILENAME), "Cannot find file {0}".format(FILENAME)

        extract_samples_from_dataset(FILENAME, N_SAMPLES)

    if PARGS.merge:

        FILES = PARGS.merge

        merge_h5_files(FILES, output_filename="merged",
                       output_dir='.')

    if PARGS.size:
        if PARGS.directory:
            DIR = PARGS.directory

            FILES = [os.path.join(DIR, _file) for _file in os.listdir(DIR)]

        elif PARGS.files:
            FILES = PARGS.files

        else:
            print("Please provide file(s) or directory of file(s) to scale.")
            sys.exit()

        merge_h5_files(FILES, output_filename="processed",
                       output_dir='.', verbose=False)

        shuffle_and_cut("processed.h5", size=PARGS.size,
                        output_dir='.', n_files=1)

        os.remove("processed.h5")
