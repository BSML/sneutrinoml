"""
Very specific program for generating .npy or .h5 files from .txt datafiles
The resulting files are guaranteed to have
at least two taus and one muon.
"""

import os, sys
import json
from operator import itemgetter
import h5py
import numpy as np


HEADER = ("[[jet],[jet],[jet]]|[[tau],[tau]]|[[muon],[muon]]"
          "|[[electron],[electron]]|met|generator weight|ht|met_phi")

# --- These are the categories of the input data
KEYS = ['jets', 'taus', 'muons', 'electrons', 'MET', 'generatorweight', 'ht', 'MET_phi']

# --- These are the keys for the neural network, i.e. flattened subgroups of KEYS

NN_KEYS = ["n_jets",
           "n_taus",
           "tau1_pt", "tau1_phi", "tau1_eta", "tau1_charge",
           "tau2_pt", "tau2_phi", "tau2_eta", "tau2_charge",
           "mu1_pt", "mu1_phi", "mu1_eta", "mu1_charge",
           "n_muons",
           "n_electrons",
           "MET", "generatorweight", "ht", "MET_phi"]



def make_dataset(filename):
    """
    return np array from datafile on Nikolai's format, no cuts imposed.
    """

    dataset = []
    with open(filename, 'r') as datafile:
        for line in datafile:
            fields = line.split('|')
            response, sub_dataset = make_dataset_subfunc(fields)
            if response:
                dataset.append(sub_dataset)

    return np.array(dataset)


def make_dataset_subfunc(fields):
    """
    For use in make_dataset
    """

    sub_dataset = np.zeros(len(NN_KEYS))
    for _index, _key in enumerate(KEYS):
        key_entry = json.loads(fields[_index])

        if _key in ["jets"]:
            index = NN_KEYS.index("n_{0}".format(_key))
            sub_dataset[index] = len(key_entry)

        if _key in ["muons", "electrons"]:
            if key_entry[0] != [-10., -10., -10., -10.]:

                index = NN_KEYS.index("mu1_pt")
                sub_dataset[index:index + len(key_entry[0])] = key_entry[0]

                index = NN_KEYS.index("n_{0}".format(_key))
                sub_dataset[index] = len(key_entry)

        elif _key == "taus":
            index = NN_KEYS.index("n_{0}".format(_key))
            n_taus = len(key_entry)

            # --- Check that there are two valid taus
            if n_taus < 2:
                return False, []

            if key_entry[0] == [-10., -10., -10., -10.] or key_entry[1] == [-10., -10., -10., -10.]:
                return False, []

            # --- Add n_taus
            sub_dataset[index] = n_taus

            index += 1
            sub_dataset[index:index + len(key_entry[0])] = key_entry[0]
            sub_dataset[index + len(key_entry[0]):
                        index + len(key_entry[0]) + len(key_entry[1])] = key_entry[1]

        # Get all features that is not jets, muons, electron or taus related
        # ex. ht, MET_phi, MET
        elif _key in NN_KEYS:
            sub_dataset[NN_KEYS.index(_key)] = key_entry

    return True, sub_dataset


def np_to_hf(data, features, outname):
    """
    Create a h5py file 'outname' from an array 'data' with 'features' features.
    """

    if not outname.endswith('.h5'):
        outname += '.h5'

    with h5py.File(outname, 'w') as hf:
        dset = hf.create_dataset('data', data.shape, data=data)
        dset.attrs['features'] = np.string_(features)
        print('File: ' + outname + ' created with ' + str(data.shape[0]) + ' events')


if __name__ == "__main__":



    #BKG = True
    BKG = False
    #SIGNAL = False
    SIGNAL = True

    MERGE = False

    import socket

    HOSTNAME = socket.gethostname()

    if HOSTNAME == "":
        DATA_DIR = "/home/"

        SUBDIR = "My_Rawdata/point50/"
        FOLDER = os.path.join(DATA_DIR, SUBDIR)

        BKG_FILES = ["ttZ.txt",
                     "WWW.txt",
                     "ZWW.txt",
                     "ttW.txt",
                     "WWbb.txt",
                     "WZ.txt",
                     "ZZ.txt"]

        SIGNAL_FILES = ["chichi.txt",
                        "qcd.txt",
                        "ew.txt"]

    OUTPUT_DIR = "data/data_{0}".format(SUBDIR)

    if not os.path.isdir(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)

    if BKG:
        print("Converting background data from directory {0}\n".format(FOLDER))
        FILES = BKG_FILES

    elif SIGNAL:
        print("Converting signal data from directory {0}\n".format(FOLDER))
        FILES = SIGNAL_FILES

    if MERGE:
        filenames = [f for f in os.listdir(FOLDER) if os.path.isfile(os.path.join(FOLDER, f))]

        all_files = BKG_FILES + SIGNAL_FILES
        for _filename in all_files:

            abs_filename = os.path.join(FOLDER, _filename)

            _filename_out = _filename.split('.')[0] + '_merged.txt'

            if not os.path.exists(abs_filename):
                print("That file does not exist. Continuing.")
                continue

            with open(os.path.join(FOLDER,_filename_out), 'w') as outfile:


                with open(abs_filename) as infile:
                    for line in infile:
                        outfile.write(line)

                more_files = True
                file_nr = 2
                while more_files:
                    _filename_wo_extension = _filename.split('.')[0]
                    _filename_with_nr = _filename_wo_extension + '_' + str(file_nr) + '.txt'
                    _filename_with_nr = os.path.join(FOLDER, _filename_with_nr)
                    if not os.path.exists(_filename_with_nr):
                        more_files = False
                        continue

                    with open(_filename_with_nr) as infile:
                        for line in infile:
                            outfile.write(line)

                    file_nr = file_nr + 1

    else:
        for _filename in FILES:

            print("Working on {0}...".format(_filename))

            abs_filename = os.path.join(FOLDER, _filename)

            if not os.path.exists(abs_filename):
                print("That file does not exist. Continuing.")
                continue

            data = make_dataset(abs_filename)

            if SIGNAL:
                _filename = "signal_{0}".format(_filename)

            _filename = os.path.join(OUTPUT_DIR, _filename.replace(".txt", ''))

            np_to_hf(data, NN_KEYS, _filename)
