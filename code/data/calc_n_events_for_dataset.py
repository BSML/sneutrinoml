"""
This file contains dictionaries with event production information.
The corresponding files are those in rawdata, rawdata_high... etc

xsectdict convention:
    xsectdict[process] = cross section / n_events_generated

Relation:
    n_events = xsect x Lumi x acceptance x eff

acceptance x efficiency = n_events_in_file / n_events_generated

Per process:
    n_events = n_events_in_file / n_events_generated * Lumi * xsect

When you add info for a new point:
    The point's name must be added in global_settings.py
    The following must be created:
    -   xsect[process][point] = cross section
    -   n_events_in_file[process][point] = nunber of events in raw datafile
    -   n_events_generated = total number of events generated
"""
from global_settings import LUMI, BKG, SIGNAL, POINTS

# pylint: disable=invalid-name


xsect = dict()
n_events_generated = dict()
n_events_in_file = dict()

xsect['qcd'] = {}
xsect['ew'] = {}
xsect['chichi'] = {}

n_events_generated['qcd'] = {}
n_events_generated['ew'] = {}
n_events_generated['chichi'] = {}

n_events_in_file['qcd'] = {}
n_events_in_file['ew'] = {}
n_events_in_file['chichi'] = {}

xsect["ttZ"] = 720.
xsect["ttW"] = 470.
xsect["ZZ"] = 17.5
xsect["WZ"] = 166.
xsect["ZWW"] = 1.
xsect["WWW"] = 9.05
xsect["WWbb"] = 23028.

# --- Benchmark point
xsect["chichi"]["point00"] = 87.5
xsect["qcd"]["point00"] = 0.68
xsect["ew"]["point00"] = 16.8

# --- High points
xsect["chichi"]["point10"] = 280.158
xsect["qcd"]["point10"] = 1.89441
xsect["ew"]["point10"] = 94.7427
xsect["chichi"]["point11"] = 836.921
xsect["qcd"]["point11"] = 1.25513
xsect["ew"]["point11"] = 85.0847
xsect["chichi"]["point12"] = 457.236
xsect["qcd"]["point12"] = 1.35003
xsect["ew"]["point12"] = 100.219
xsect["chichi"]["point13"] = 261.835
xsect["qcd"]["point13"] = 0.974154
xsect["ew"]["point13"] = 92.4518
xsect["chichi"]["point14"] = 598.467
xsect["qcd"]["point14"] = 0.197425
xsect["ew"]["point14"] = 66.767
xsect["chichi"]["point15"] = 571.929
xsect["qcd"]["point15"] = 0.60648
xsect["ew"]["point15"] = 145.357
xsect["chichi"]["point16"] = 577.711
xsect["qcd"]["point16"] = 2.61303
xsect["ew"]["point16"] = 105.082
xsect["chichi"]["point17"] = 505.587
xsect["qcd"]["point17"] = 1.91789
xsect["ew"]["point17"] = 108.077
xsect["chichi"]["point18"] = 250.113
xsect["qcd"]["point18"] = 0.361055
xsect["ew"]["point18"] = 120.39
xsect["chichi"]["point19"] = 552.354
xsect["qcd"]["point19"] = 1.12945
xsect["ew"]["point19"] = 64.33

# --- Around benchmark point
xsect["chichi"]["point01"] = 2.21265
xsect["qcd"]["point01"] = 0.701873
xsect["ew"]["point01"] = 17.1799
xsect["chichi"]["point02"] = 1.59848
xsect["qcd"]["point02"] = 0.652488
xsect["ew"]["point02"] = 16.711
xsect["chichi"]["point03"] = 2.34617
xsect["qcd"]["point03"] = 0.698544
xsect["ew"]["point03"] = 17.5444
xsect["chichi"]["point04"] = 18.1544
xsect["qcd"]["point04"] = 0.672492
xsect["ew"]["point04"] = 16.858
xsect["chichi"]["point05"] = 1.5444
xsect["qcd"]["point05"] = 0.654768
#TODO: Ask Niko for 06 etc

# --- Additional points
xsect["chichi"]["point20"]=156.5
xsect["qcd"]["point20"]=0.46
xsect["ew"]["point20"]= 25.96

xsect["chichi"]["point30"]=128.3
xsect["qcd"]["point30"]=0.11
xsect["ew"]["point30"]= 17.48

xsect["chichi"]["point40"]=825.0
xsect["qcd"]["point40"]=1.54
xsect["ew"]["point40"]= 143.0

xsect["chichi"]["point50"]=1680.
xsect["qcd"]["point50"]=0.64
xsect["ew"]["point50"]= 215.4


n_events_generated["ttZ"] = 65726770.0
n_events_generated["ttW"] = 58659575.0
n_events_generated["ZZ"] = 32652616.0
n_events_generated["WZ"] = 27723222.0
n_events_generated["ZWW"] = 25000000.0
n_events_generated["WWW"] = 36741922.0
n_events_generated["WWbb"] = 133282180.0

# --- Benchmark point
n_events_generated["chichi"]["point00"] = 860000000.0
n_events_generated["qcd"]["point00"] = 25000000.0
n_events_generated["ew"]["point00"] = 275000000.0

# --- High points
n_events_generated["chichi"]["point10"] = 5000000.0
n_events_generated["qcd"]["point10"] = 5000000.0
n_events_generated["ew"]["point10"] = 5000000.
n_events_generated["chichi"]["point11"] = 5000000.0
n_events_generated["qcd"]["point11"] = 5000000.0
n_events_generated["ew"]["point11"] = 5000000.
n_events_generated["chichi"]["point12"] = 105000000.0
n_events_generated["qcd"]["point12"] = 105000000.0
n_events_generated["ew"]["point12"] = 105000000.
n_events_generated["chichi"]["point13"] = 5000000.0
n_events_generated["qcd"]["point13"] = 5000000.0
n_events_generated["ew"]["point13"] = 5000000.
n_events_generated["chichi"]["point14"] = 5000000.0
n_events_generated["qcd"]["point14"] = 5000000.0
n_events_generated["ew"]["point14"] = 5000000.
n_events_generated["chichi"]["point15"] = 5000000.0
n_events_generated["qcd"]["point15"] = 5000000.0
n_events_generated["ew"]["point15"] = 5000000.
n_events_generated["chichi"]["point16"] = 5000000.0
n_events_generated["qcd"]["point16"] = 5000000.0
n_events_generated["ew"]["point16"] = 5000000.
n_events_generated["chichi"]["point17"] = 5000000.0
n_events_generated["qcd"]["point17"] = 5000000.0
n_events_generated["ew"]["point17"] = 5000000.
n_events_generated["chichi"]["point18"] = 5000000.0
n_events_generated["qcd"]["point18"] = 5000000.0
n_events_generated["ew"]["point18"] = 5000000.
n_events_generated["chichi"]["point19"] = 5000000.0
n_events_generated["qcd"]["point19"] = 5000000.0
n_events_generated["ew"]["point19"] = 5000000.

# --- Additional points
n_events_generated["chichi"]["point20"] = 101000000.0
n_events_generated["qcd"]["point20"] =101000000.0
n_events_generated["ew"]["point20"] = 101000000.0

n_events_generated["chichi"]["point30"] = 10000000.0
n_events_generated["qcd"]["point30"] = 10000000.0
n_events_generated["ew"]["point30"] = 10000000.0

n_events_generated["chichi"]["point40"] = 10000000.0
n_events_generated["qcd"]["point40"] = 10000000.0
n_events_generated["ew"]["point40"] = 10000000.0

n_events_generated["chichi"]["point50"] = 10000000.0
n_events_generated["qcd"]["point50"] = 10000000.0
n_events_generated["ew"]["point50"] = 10000000.0

# --- Around benchmark point
n_events_generated["chichi"]["point01"] = 5000000.0
n_events_generated["qcd"]["point01"] = 5000000.0
n_events_generated["ew"]["point01"] = 5000000.
n_events_generated["chichi"]["point02"] = 5000000.0
n_events_generated["qcd"]["point02"] = 5000000.0
n_events_generated["ew"]["point02"] = 5000000.
n_events_generated["chichi"]["point03"] = 5000000.0
n_events_generated["qcd"]["point03"] = 5000000.0
n_events_generated["ew"]["point03"] = 5000000.
n_events_generated["chichi"]["point04"] = 5000000.0
n_events_generated["qcd"]["point04"] = 5000000.0
n_events_generated["ew"]["point04"] = 5000000.
n_events_generated["chichi"]["point05"] = 5000000.0
n_events_generated["qcd"]["point05"] = 5000000.0

n_events_in_file["WWbb"] = 236735
n_events_in_file["ttW"] = 69041
n_events_in_file["ttZ"] = 82002
n_events_in_file["ZWW"] = 1841159
n_events_in_file["ZZ"] = 2371574
n_events_in_file["WZ"] = 497319
n_events_in_file["WWW"] = 308994

n_events_in_file["ew"]["point00"] = 1602624
n_events_in_file["chichi"]["point00"] = 504304
n_events_in_file["qcd"]["point00"] = 351167
#TODO: Fill in from all signal files

# --- Additional points
n_events_in_file["chichi"]["point20"] = 130697
n_events_in_file["qcd"]["point20"] = 1722588
n_events_in_file["ew"]["point20"] = 345813

n_events_in_file["chichi"]["point30"] = 13658
n_events_in_file["qcd"]["point30"] = 155315
n_events_in_file["ew"]["point30"] = 74701

n_events_in_file["chichi"]["point40"] = 38318
n_events_in_file["qcd"]["point40"] = 190790
n_events_in_file["ew"]["point40"] = 205791

n_events_in_file["chichi"]["point50"] = 29024
n_events_in_file["qcd"]["point50"] = 134497
n_events_in_file["ew"]["point50"] = 261572

n_events_in_file["chichi"]["point01"] = 48854
n_events_in_file["qcd"]["point01"] = 41542
n_events_in_file["ew"]["point01"] = 15100

n_events_in_file["chichi"]["point02"] = 48854
n_events_in_file["qcd"]["point02"] = 41542
n_events_in_file["ew"]["point02"] = 15100

n_events_in_file["chichi"]["point03"] = 58021
n_events_in_file["qcd"]["point03"] = 46938
n_events_in_file["ew"]["point03"] = 16435

n_events_in_file["chichi"]["point04"] = 29188
n_events_in_file["qcd"]["point04"] = 57834
n_events_in_file["ew"]["point04"] = 16338

n_events_in_file["chichi"]["point05"] = 41556
n_events_in_file["qcd"]["point05"] = 36644
n_events_in_file["ew"]["point05"] = 14191

n_events_in_file["chichi"]["point06"] = 73809
n_events_in_file["qcd"]["point06"] = 62624
n_events_in_file["ew"]["point06"] = 17728

n_events_in_file["chichi"]["point10"] = 52911
n_events_in_file["qcd"]["point10"] = 103904
n_events_in_file["ew"]["point10"] = 119202

n_events_in_file["chichi"]["point11"] = 9791
n_events_in_file["qcd"]["point11"] = 59047
n_events_in_file["ew"]["point11"] = 133821

n_events_in_file["chichi"]["point12"] = 415417
n_events_in_file["qcd"]["point12"] = 1907578
n_events_in_file["ew"]["point12"] = 1766917

n_events_in_file["chichi"]["point13"] = 44969
n_events_in_file["qcd"]["point13"] = 93956
n_events_in_file["ew"]["point13"] = 102482

n_events_in_file["chichi"]["point14"] = 21632
n_events_in_file["qcd"]["point14"] = 87583
n_events_in_file["ew"]["point14"] = 153771

n_events_in_file["chichi"]["point15"] = 35933
n_events_in_file["qcd"]["point15"] = 96660
n_events_in_file["ew"]["point15"] = 102215

n_events_in_file["chichi"]["point16"] = 51031
n_events_in_file["qcd"]["point16"] = 88419
n_events_in_file["ew"]["point16"] = 134468

n_events_in_file["chichi"]["point17"] = 42207
n_events_in_file["qcd"]["point17"] = 85145
n_events_in_file["ew"]["point17"] = 130241

n_events_in_file["chichi"]["point18"] = 61602
n_events_in_file["qcd"]["point18"] = 106501
n_events_in_file["ew"]["point18"] = 119631


n_events_in_file["chichi"]["point19"] = 29847
n_events_in_file["qcd"]["point19"] = 73666
n_events_in_file["ew"]["point19"] = 178027

def calc_acc_times_eff(process, point=None):
    """
    Acceptance times efficiency from number of events kept divided by number of
    events produced
    """

    if process in BKG:
        return n_events_in_file[process] / n_events_generated[process]

    if process in SIGNAL:

        assert point is not None, "Please provide point number"

        return n_events_in_file[process][point] / n_events_generated[process][point]

    print("Unknown process", process)
    return False

def calc_n_expected(process, point=None):
    """
    Number of expected events from
        N = xsect x acceptance x efficiency x Luminosity
    """

    if process in BKG:
        return xsect[process] * calc_acc_times_eff(process) * LUMI

    if process in SIGNAL:

        assert point is not None, "Please provide point number"

        return xsect[process][point] * calc_acc_times_eff(process, point) * LUMI

    print("Unknown process", process)
    return False


if __name__ == "__main__":
    # --- Tests ---------------------------------------------------------------
    print("Test for ttW:")
    print("Acceptance times efficiency:", calc_acc_times_eff("ttW"))
    print("Expected number of events:", calc_n_expected("ttW"))

    print("Test for ew, point 0:")
    print("Acceptance times efficiency:", calc_acc_times_eff("ew", "point00"))
    print("Expected number of events:", calc_n_expected("ew", "point00"))
    # -------------------------------------------------------------------------

    # --- 1 million background events -----------------------------------------
    N_TOT = 1.0 * 1e6
    print("\nExample:")
    print("I want a background dataset with N_tot = 1 million events\n")
    BKG_N_EVENTS = []

    # --- Calculate expected number of events for each process
    #print "\nExpected n_events from each process:"
    for _process in BKG:
        _n_events = calc_n_expected(_process)
        BKG_N_EVENTS.append(_n_events)
        print("{0}: {1}".format(_process, _n_events))

    # --- Total number of events
    N_EXP = sum(BKG_N_EVENTS)
    print("\nPoint 0, total background: {0}\n".format(N_EXP))

    #print "This corresponds to number of events N_EXP = ", N_EXP

    ## --- Scale to 1 million events
    #SCALE_FACTOR = N_TOT/N_EXP
    #print "\nScale factor for 1 million events:", SCALE_FACTOR

    #BKG_N_TOT = []

    ## --- Calculate number of events from each process to get 1 mill events in total
    #print "\nRequired n_events from each process:"
    #for _process in BKG:
    #    _n_events = SCALE_FACTOR * calc_n_expected(_process)
    #    BKG_N_TOT.append(_n_events)
    #    print "{0}: {1}".format(_process, _n_events)

    #print "\nGet {0} background events in total".format(sum(BKG_N_TOT))
    # -------------------------------------------------------------------------


    SIGNAL_N_EVENTS = []
    for _process in SIGNAL:
        _n_events = calc_n_expected(_process, "point00")
        SIGNAL_N_EVENTS.append(_n_events)
        print("{0}: {1}".format(_process, _n_events))

    # --- Total number of events
    N_EXP_SIGNAL = sum(SIGNAL_N_EVENTS)
    print("\nPoint 0, total signal: {0}\n".format(N_EXP_SIGNAL))

    MIXTURE_PARAMETER = N_EXP_SIGNAL / (N_EXP_SIGNAL + N_EXP)
    print("Mixture parameter (for signal):", MIXTURE_PARAMETER)

    # -------------------------------------------------------------------------
