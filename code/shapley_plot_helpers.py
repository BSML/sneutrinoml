import os
import pickle
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import scipy.linalg
import numpy
import numpy as np
import sys

CF_DICT = { "r2" : r"$R^2$", "dcor" : "Distance correlation"}
COLORS = ["orange", "blue", "green", "purple"]
plt.style.use("sneu.mplstyle")

from global_settings import LABELDICT, COLORS

def F(key):
    return LABELDICT.get(key, key)

def sort_dict(dictionary, reverse=True):
    return {key: value for key, value in sorted(dictionary.items(), key=lambda
        item: item[1], reverse=reverse)}

def ordered_bars_from_dict(dicts, labels=None):
    with open(dicts[0] ,"rb") as f:
        avg_dict = pickle.load(f)
    _sorted = sort_dict(avg_dict, False)
    sorted_features = _sorted.keys()

    if labels is None:
        labels=list(range(len(dicts)))
    hatches = ['.', '', '/']

    ax = plt.gca()
    xs = np.array(range(len(sorted_features)))
    for _n, _dict in enumerate(dicts):
        with open(_dict ,"rb") as f:
            _dict = pickle.load(f)
        _values = [_dict[_key] for _key in sorted_features]
        ax.barh(xs+0.3*_n, _values, color=COLORS[_n], height=0.3,
                label=labels[_n], hatch=hatches[_n], alpha=0.7)

    plt.yticks(ticks=xs+0.2, labels=map(F,sorted_features), rotation=45)
    plt.xlim([-0.001, 0.17])
    plt.legend(loc="lower right")
    plt.show()


def plot_ADXs(filenames, features, kind="ADL", sort=None, color=None,
        marker=None, label=None, save_dict=False):
    if not isinstance(filenames, list):
        filenames = [filenames]

    n_feats = len(features)
    n_files = len(filenames)
    if color is None:
        color = [COLORS[0] if kind == "ADL" else COLORS[1]][0]
    if marker is None:
        marker = ['s' if kind == "ADL" else '*'][0]
    value_dict = {}
    for _f in features:
        value_dict[_f] = []

    for _n, _filename in enumerate(filenames):
        _info = _filename.split("/")[-1].split("_")
        _n_feats = int(_info[1])
        _n_samples = int(_info[3])
        assert _n_feats == n_feats

        _values = np.load(_filename)
        # --- Record values for average
        for _f, _v in zip(features, _values):
            value_dict[_f].append(_v)

    if sort is None:
        feats_in_order = features
    else:
        feats_in_order = sort

    avg_dict = {}
    for _f, _feat in enumerate(feats_in_order):
        _values = value_dict[_feat]
        _avg = np.average(_values)
        avg_dict[_feat] = _avg
        _l = len(_values)
        _x = [_f+(1-2*_n/n_files)*0.2 for _n in range(_l)]
        if _f==0:
            if label is None:
                label = kind
            plt.scatter(_x,_values,c=color,s=40,alpha=0.5,marker=marker,label=label)
        else:
            plt.scatter(_x,_values,c=color,s=40,alpha=0.5,marker=marker)

        #if len(filenames) > 1:
        #    plt.scatter(_f,_avg,c="black",s=40,marker='o')#, label="Average")

    ymin, ymax = -0.05, 0.2
    plt.ylim([ymin, ymax])
    plt.xlim([-0.5, n_feats-0.5])

    ax = plt.gca()
    xticks = range(0, n_feats)
    ax.set_xticks(xticks)
    ax.set_xticklabels(map(F,feats_in_order),rotation="45")
    ax.legend(markerscale=2)

    for _x in xticks:
        plt.vlines(_x-0.5, ymin, ymax, color="gray", alpha=0.2, ls='-', lw=0.3)
    plt.vlines(_x+0.5, ymin, ymax, color="gray", alpha=0.2, ls='-', lw=0.3)

    # --- Save avg dict for later
    if save_dict:
        with open(f"avg_dict_{kind}.pkl" ,"wb") as f:
            pickle.dump(avg_dict,f)

    return avg_dict

