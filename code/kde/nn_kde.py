import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions
from sklearn.model_selection import RepeatedKFold


class NnKde(object):
    '''
    One-dimensional Gaussian kernel density estimator defined on the interval
    [0, 1], useful for classifier outputs. Kernels are renormalised, i.e.
    truncated to the valid interval, to avoid inaccuracies are the boundaries

    Parameters
    ----------
    bandwidth: scalar or string
        The method to use for finding optimal bandwidth. Valid options are
        'silverman' or 'cross-validation' (default). If scalar, this directly
        sets the bandwidth, so not optimisation is performed.
    binned: bool
        Bin the data, to increase evaluation speed on large datasets
        TODO Not implemented yet
    verbose: bool
        Print verbose messages

    Examples
    --------

    See a comparison of optimisation methods:
    $ python nn-kde.py

    Or try the following

    Generate data from a multi-modal distribution:
    >>> X1 = np.random.normal(loc=0.2, scale=0.1, size=60)
    >>> X2 = np.random.normal(loc=0.6, scale=0.3, size=100)
    >>> X = np.concatenate((X1, X2))

    Limit to interval [0, 1]
    >>> X = X[(X > 0) & (X < 1)]

    Create and fit KDE
    >>> kde = NnKde()
    >>> kde.fit(X)

    Plot results
    >>> import matplotlib.pyplot as plt
    >>> xvals = np.linspace(0, 1, 100)
    >>> xvals = xvals.astype(np.float32)
    >>> plt.hist(X, bins=30, range=(0,1), density=True, label='Data')
    >>> plt.plot(xvals, kde.evaluate(xvals), label='KDE')
    >>> plt.legend()
    >>> plt.show()

    See also test functions below for more inspo
    '''

    _bw_methods = ['fixed', 'silverman', 'cross-validation']


    def __init__(self, bandwidth='cross-validation', binned=False, verbose=False):

        # Output level
        self._verbose = verbose

        # The final pdf, constructed by fit()
        self.pdf = None

        # Precision
        self.dtype = np.float32

        # Nuumber of bins, if binning data
        self._nbins = 300

        # Bounds 
        self._lower = 0.0
        self._upper = 1.0

        # Tensorflow session (for tensorflow v1)
        ##self._sess = tf.Session()

        # Set bandwidth if specified explicitly, otherwise set selection method
        try:
            self._bandwidth = self.dtype(bandwidth)
            self._bandwidth_selector = 'fixed'
        except ValueError:
            self._bandwidth = None
            self._bandwidth_selector = bandwidth
        
        if self._bandwidth_selector not in self._bw_methods:
            raise Exception('Not a valid bandwidth selection method: {}'.format(bandwidth))

        if self._verbose:
            print('Bandwidth selection method: {}'.format(self._bandwidth_selector))
            

        # Binned or unbinned data
        self._binned = binned
    


    def fit(self, x, weights=None, **kwargs):
        '''
        Optimise bandwidth (unless fixed by construction) and initialize
        probability distributions. 

        Parameters
        ----------
        x:  Array-like, shape (n_data_points, )
            Input data
        wgts: Array-like, shape (n_data_points, )
            Weigths for each input data point
        kwargs: dict
            Optional parameters specific to the chosen fit method, see
            :function:_crossvalidation
        
        Returns
        -------
        self: object
        '''

        # Internal copy of data
        _x = np.array(x, dtype=self.dtype)
        _weights = np.array(weights, dtype=self.dtype) if weights is not None else None


        # Flatten if necessary
        if len(_x.shape) > 1:
            assert _x.shape[1] == 1, "Data must be one-dimensional"
            _x = _x.ravel()

        if self._verbose:    
            print('fit(): Input data shape: {}'.format(_x.shape))

        if _weights is not None:
            if len(_weights.shape) > 1:
                assert _weights.shape[1] == 1, "Weights must be one-dimensional"
                _weights = _weights.ravel()
            assert _x.shape[0] == _weights.shape[0], 'Weights must be same length as data'



        # Find bandwidth
        if self._bandwidth_selector == 'fixed':
            pass
        
        elif self._bandwidth_selector == 'silverman':
            self._bandwidth = self._silverman_rule(x)   # NOTE use unbinned data
            if _weights is not None:
                raise UserWarning('Weights ignored when using Silverman rule')
        
        elif self._bandwidth_selector == 'cross-validation':
            self._bandwidth = self._crossvalidation(_x, _weights, **kwargs)

        # Construct pdf
        self.pdf = self._create_pdf(self._bandwidth, _x, _weights)

        return self



    
    def evaluate(self, x, log=False):
        '''
        Return probability under the KDE for given set of points
        
        Parameters
        ----------
        x:  Array-like, shape (n_data_points, )
            Input data
        log: bool
            Return log-probability rather than just probability
        
        Returns
        -------
        prob: ndarray, shape (n_data_points, )
            (Log-) Probability of points under estimated distribution
        '''

        _x = np.array(x, dtype=self.dtype)

        # Flatten if necessary
        if len(_x.shape) > 1:
            assert _x.shape[1] == 1, "Data must be one-dimensional"
            _x = _x.ravel()

        if log:
            ##prob = self.pdf.log_prob(_x).eval(session=self._sess)
            prob = self.pdf.log_prob(_x)
        else:
            ##prob = self.pdf.prob(_x).eval(session=self._sess)
            prob = self.pdf.prob(_x)

        return prob



    def _kernel(self, x, bw):
        '''
        Truncated Gaussian kernel
        '''
        return tfd.Independent(
            tfd.TruncatedNormal(
                loc=self.dtype(x),
                scale=self.dtype(bw),
                low=self.dtype(self._lower),
                high=self.dtype(self._upper)
                )
            )


    def _create_pdf(self, bw, x, wgts=None):
        '''
        Create mixture of kernel functions
        If self.binned is set, only self.nbins number of kernels are used.

        Parameters
        ----------
        bw: float
            Kernel bandwidth, i.e. Gaussian width/sigma
        x:  Array-like, shape (n_data_points, )
            Input data
        wgts: Array-like, shape (n_data_points, )
            Weigths for each input data point
        
        Returns
        -------
        pdf: tfd.MixtureSameFamily
            Tensorflow distribution
        '''

        # Bin data
        if self._binned:
            x, wgts = self._bin_data(x, wgts)

        nkernels = x.shape[0]

        if wgts is not None:
            # Adjust for number of kernels
            wgts /= nkernels
        else:
            # Weights are just 1 / number of kernels
            wgts = [1.0/float(nkernels)] * nkernels
            wgts = np.array(wgts, dtype=self.dtype)
        
        assert wgts.shape[0] == nkernels

        # Log offset trick
        wgts += 1.0e-12

        pdf = tfd.MixtureSameFamily(
            mixture_distribution=tfd.Categorical(logits=np.log(wgts)),
            components_distribution=self._kernel(x, bw)
        )

        return pdf



    def _bin_data(self, x, wgts):
        '''
        Return binned copy of input data
        '''

        histrange = (self._lower, self._upper)
        bincounts, binedges = np.histogram(x,
            bins=self._nbins, range=histrange, weights=wgts, density=True)
            
        bincenters = binedges[:-1] + 0.5*np.diff(binedges)
        assert bincenters.shape == (self._nbins,)

        return np.array(bincenters, dtype=self.dtype), np.array(bincounts, dtype=self.dtype)    # x, weights





    def _silverman_rule(self, x):
        '''
        Choose bandwidth according to Silverman's rule of thumb. Useful only
        for unimodal data, otherwise bandwidth is typically overestimated

        Parameters
        ----------
        x:  Array-like, shape (n_data_points, )
            Input data

        Returns
        -------
        float
        '''
        
        return self.dtype(1.06 * np.std(x) * len(x)**(-1./5.))


    def _crossvalidation(self, x, wgts=None, method='grid', spacing='linear',
                         n_points=15, n_splits=4, n_repeats=3,
                         bw_min=1.e-5, bw_max=1.0):
        '''
        Choose bandwith based on the highest likelihood obtained from
        cross-validation.

        Parameters
        ----------
        x:  Array-like, shape (n_data_points, )
            Input data.
        wgts: Array-like, shape (n_data_points, )
            Weights corresponding to each data point
        method: string
            Method for sampling bandwidth test values, valid options are
            'grid' or 'random'
        spacing: string
            Spacing between test values, valid options are 'linear' or 'log'
        n_points: int
            Number of bandwidth values to test
        n_splits: int
            Number of splits in k-fold cross validation, see 
            documantation for sklearn.model_selection.RepeatedKFold
        n_repeats: int
            Number of times cross validation is repeated for one test value
        bw_min: float
            Lower limit for bandwidth values to consider
        bw_max: float
            Upper limit for bandwidth values to consider

        Returns
        -------
        best_bw: float
            Optimal bandwidth based on log-likelihood values of points in
            unseen test sets

        '''

        assert bw_max > bw_min, 'Upper bandwidth limit smaller than lower limit'


        # Sampling function to obtain bandwidths to test 
        sampling_function = None

        # Linearly spaced grid
        if method == 'grid' and spacing == 'linear':
            sampling_function = np.linspace
        # Log spaced grid
        elif method == 'grid' and spacing == 'log':
            sampling_function = np.logspace
        # Uniformly sampled random values 
        elif method == 'random' and spacing == 'linear':
            sampling_function = np.random.uniform
        # Log-uniformly sampled random values 
        elif method == 'random' and spacing == 'log':
            def loguniform(low, high, size):
                return np.exp(np.random.uniform(low, high, size))
            sampling_function = loguniform
        else:
            raise Exception('Cross-validation: Not a valid combination: '
                'method = {}, spacing = {}'.format(method, spacing))

        # Transform upper/lower limits to log space
        if spacing == 'log':
            bw_min = np.log10(bw_min)
            bw_max = np.log10(bw_max)


        # Draw points
        testpoints = sampling_function(bw_min, bw_max, n_points)        
        testpoints = testpoints.astype(self.dtype)

        rkf = RepeatedKFold(n_splits=n_splits, n_repeats=n_repeats)

        # Loop over grid points, compute average scores for unseen test sets
        scores = {}
        for point in testpoints:

            if self._verbose:
                print('CV: testing point: {:.3}'.format(point))
            scores_for_point = []
            
            for itrain, itest in rkf.split(x):
                bandwidth = point

                train = x[itrain]
                test = x[itest]

                pdf = self._create_pdf(bandwidth, train)
                ##loglike = np.sum(pdf.log_prob(test).eval(session=self._sess))
                loglike = np.sum(pdf.log_prob(test))

                #if self._verbose:
                #    print('bw = {} - score = {}'.format(point, loglike))

                scores_for_point.append(loglike)
                del pdf

            avg_score = np.mean(scores_for_point)
            scores[point] = avg_score

            if self._verbose:
                print('Mean score for point: {:.4}'.format(avg_score))

        best_bw = max(scores, key=scores.get)

        return best_bw







if __name__ == '__main__':

    import matplotlib.pyplot as plt

    # Test data 
    X1 = np.random.normal(loc=0.2, scale=0.1, size=300)
    X2 = np.random.normal(loc=0.6, scale=0.3, size=450)
    X = np.concatenate((X1, X2))
    X = X[(X > 0.0) & (X < 1.0)]
    

    def test_case1(data):
        ''' Testing fixed bandwidth '''
        
        kde = NnKde(bandwidth=0.05)
        kde.fit(data)

        return kde
    

    def test_case2(data):
        ''' Testing Silverman method '''
        
        kde = NnKde(bandwidth='silverman')
        kde.fit(data)

        return kde

    def test_case3(data):
        ''' Testing cross-validation method '''
        
        kde = NnKde(bandwidth='cross-validation', verbose=True)
        kde.fit(data, method='grid', spacing='linear', n_points=10)

        return kde



    kde1 = test_case1(X)
    kde2 = test_case2(X)
    kde3 = test_case3(X)

    # Plot results 
    fig = plt.figure()

    xvals = np.linspace(0, 1, 100)

    plt.hist(X, bins=30, range=(0,1), density=True, label='Data')
    plt.plot(xvals, kde1.evaluate(xvals), label='Fixed bw ({:.3})'.format(kde1._bandwidth))
    plt.plot(xvals, kde2.evaluate(xvals), label='Silverman ({:.3})'.format(kde2._bandwidth))
    plt.plot(xvals, kde3.evaluate(xvals), label='CV ({:.3})'.format(kde3._bandwidth))

    plt.legend()
    plt.show()

