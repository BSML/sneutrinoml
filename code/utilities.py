# pylint: disable=C0303

"""
Various functions needed by the other files
"""

import sys
import os
import warnings
import pickle
from glob import glob
import h5py
import numpy as np
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc, accuracy_score
from tensorflow.keras import utils
from xgboost import XGBClassifier
import matplotlib.pyplot as plt

from global_settings import FEATURES_TO_USE

def format_alpha(alpha):
    """
    Format a number (here: mixture parameter alpha) to have an underscore
    instead of a dot, and two decimals. Example: 0.05324783 returns 0_05.
    """

    assert isinstance(alpha, (int, float)), "{0} is not a number".format(alpha)


    assert 0 <= alpha <= 1, "Only mixture paramenters between 0 and 1, please."
    if alpha == 1:
        return "1_0"

    if alpha < 0.01 or alpha > 0.99:
        return "0_{0}".format(str(round(alpha, 3)).split('.')[-1])

    return "0_{0}".format(str(round(alpha, 2)).split('.')[-1])

def predict(model, x_data, scalerfile=None):
    """
    Use model to predict on x_data
    """

    if scalerfile is not None:
        x_data = scale(x_data, scalerfile)

    if x_data is None:
        return None, None

    y_pred = model.predict(x_data)

    return y_pred

def make_templates(model, templ_data, scalerfile=None):
    """
    Make templates for each class.
    """

    #x_data, y_data, _ = get_dataset_from_path(templ_data)
    x_data, y_data = np.load(templ_data[0]), np.load(templ_data[1])

    if scalerfile is not None:
        x_data = scale(x_data, scalerfile)

        if x_data is None:
            return False

    x_datas, y_datas = split_class(x_data, y_data)

    if isinstance(model, XGBClassifier):
        templates = [model.predict_proba(_x_data)[:, 1] for _x_data in x_datas]
    else:
        templates = [model.predict(_x_data) for _x_data in x_datas]
    targets = [np.mean(_y_data) for _y_data in y_datas]

    return templates, targets

def split_class(x_data, y_data):
    """
    Split x_data based on the target value in y_data and return an x_data array
    per class.
    """

    x_datas = []
    y_datas = []

    for _y in set(y_data):
        new_x_data = np.array([x_data[n] for n, i in enumerate(y_data) if i == _y])
        x_datas.append(new_x_data)

        new_y_data = np.ones(new_x_data.shape[0])*_y
        y_datas.append(new_y_data)

    return x_datas, y_datas


def get_dataset_from_path(path):
    """
    Open h5 files in path, return numpy arrays
    """

    if not path.endswith('/'):
        path += '/'

    filelist = glob(path + '*.h5')
    #print('filelist:')
    #print(filelist)
    targets = [1 if "signal" in filename else 0 for filename in filelist]

    if len(filelist) < 1:
        print("Error: no files found at", path)
        exit(-1)

    return get_dataset_from_files(filelist, targets)

def get_dataset_from_files(filelist, targetlist):
    """
    Open npy files as listed in filelist,
    return numpy array X with data from files, and corresponding numpy array Y with
    targets as specified by 'targetlist'.
    """

    if not isinstance(filelist, list):
        filelist = [filelist]

    x_data = np.array([])
    y_data = np.array([])
    features = None

    print("Opening files:")

    for index, fin in enumerate(filelist):
        # --- Get name of process
        process = fin.split('.')[0].split('/')[-1]

        # --- Assert h5py datafiles
        assert fin.endswith(".h5"), "Please make sure the files are in .h5 format."

        # --- Load data from .h5 file
        hf_fin = h5py.File(fin, 'r')
        data = hf_fin.get('data')

        #print('fin:', fin, 'shape:', data.shape)    

        if features is None:
            features = np.char.decode(data.attrs['features'])
            new_features = np.char.decode(data.attrs['features'])
        else:
            new_features = np.char.decode(data.attrs['features'])

        data = np.array(data)

        if any(features != new_features):
            print("Error: Provided datafiles have different features. Exiting")
            sys.exit()

        target = np.ones(data.shape[0])*targetlist[index]

        # --- Add up the data
        if x_data.size > 0:
            assert data.shape[1] == x_data.shape[1]
            x_data = np.vstack((x_data, data))
            y_data = np.append(y_data, target)
        else:
            x_data = data
            y_data = target

        assert x_data.shape[0] == y_data.shape[0]

        print(("Got data from {0}, containing {1} events with target {2}.")
               .format(fin, data.shape[0], targetlist[index]))

    if np.isnan(x_data).any():
        print("\nx_data contains nan. Do something.\n")

    x_data, features = select_features(x_data, features, FEATURES_TO_USE)

    assert x_data.shape[1] == len(FEATURES_TO_USE)

    print("Using features ", FEATURES_TO_USE)

    return x_data, y_data, features

def scale(x_data, scalerfile):
    """
    Scale the data given the saved scaler from scalerfile.
    """

    try:
        with open(scalerfile, 'rb') as hfile:
            scaler = pickle.load(hfile)
    except IOError:
        print("Couldn't find scaler file {0}. Aborting".format(scalerfile))
        return None

    x_data = scaler.transform(x_data)

    return x_data


def select_features(data, feature_names_in_data, features_to_use):
    """
    Select columns from data. Returns new numpy array

        feature_names_in_data: List of the names of the columns in 'data'
        features_to_use: List of features to keep in data
    """

    features_to_use = np.array(features_to_use)
    feature_names_in_data = np.array(feature_names_in_data)

    cols = []
    for name in features_to_use:
        if name in feature_names_in_data:
            cols.append(np.where(feature_names_in_data == name)[0][0])

    data = data[:, cols]

    return data, features_to_use
