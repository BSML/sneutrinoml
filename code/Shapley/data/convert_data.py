import pandas as pd
import numpy as np
import xgboost
import sys
import pickle
from sklearn import metrics

from utilities import get_dataset_from_path, predict
from global_settings import FEATURES_TO_USE

xgb = True
nn = True

for _p in ["00", "12", "13", "14", "15", "16", "20", "30", "40", "50"]:
    folder = f"point{_p}"
    x, y, features = get_dataset_from_path(folder)
    assert all(FEATURES_TO_USE == features)
    assert np.mean(y)-0.5 < 0.01, "Mean target value deviates from 0.5"

    xframe = pd.DataFrame(x, columns=FEATURES_TO_USE)
    tframe = pd.DataFrame(y, columns=["target"])

    # ----------------------------------------------------
    # --- XGBOOST:
    if xgb:
        from global_settings_xg import MODELFILE
        model = pickle.load(open(MODELFILE, "rb"))
        y_pred = model.predict(x)
        print('Accuracy Score: {:4.2f}% '.format(100*metrics.accuracy_score(y, np.round(y_pred))))
        filename = f"xgb_data_and_preds_{_p}.csv"

        pframe = pd.DataFrame(y_pred, columns=["prediction"])
        dataframe = pd.concat([xframe, tframe, pframe], axis=1, join="inner")
        dataframe.to_csv(filename)
        print(f"Saved data to {filename}")


    if nn:
        from global_settings_nn import MODELFILE, SCALER
        from tensorflow.keras.models import load_model
        model = load_model(MODELFILE)
        y_pred = predict(model, x, scalerfile=SCALER)
        print('Accuracy Score: {:4.2f}% '.format(100*metrics.accuracy_score(y, np.round(y_pred))))
        filename = f"nn_data_and_preds_{_p}.csv"


        pframe = pd.DataFrame(y_pred, columns=["prediction"])
        dataframe = pd.concat([xframe, tframe, pframe], axis=1, join="inner")
        dataframe.to_csv(filename)
        print(f"Saved data to {filename}")
# ----------------------------------------------------

    if not xgb and not nn:
        filename = f"data_{_p}.csv"
        dataframe = pd.concat([xframe, tframe], axis=1, join="inner")
        dataframe.to_csv(filename)
        print(f"Saved data to {filename}")
