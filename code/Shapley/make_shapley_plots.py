import os
import pickle
import matplotlib.pyplot as plt
import scipy.linalg
import numpy
import numpy as np
import sys

from global_settings import FEATURES_TO_USE, COLORS, SHAPLEY_DIR, SHAPLEY_VALUES
from shapley_plot_helpers import plot_ADXs, F, ordered_bars_from_dict

plt.style.use("sneu.mplstyle")

# --- Figure 8
FILENAMES =[
        [os.path.join(SHAPLEY_VALUES, "point12/ADL", _f) for _f in
            os.listdir(f"{SHAPLEY_VALUES}/point12/ADL")],
        [os.path.join(SHAPLEY_VALUES, "point12/ADP/xgb", _f) for _f in
            os.listdir(f"{SHAPLEY_VALUES}/point12/ADP/xgb")],
        [os.path.join(SHAPLEY_VALUES, "point12/ADP/nn", _f) for _f in
            os.listdir(f"{SHAPLEY_VALUES}/point12/ADP/nn")]]

MARKERS = ['o', 's', '*']
LABELS = ["ADL", "ADP (XGB)", "ADP (DNN)"]
for filenames, _color, _marker, _label in zip(FILENAMES, COLORS, MARKERS, LABELS):
    plot_ADXs(filenames,FEATURES_TO_USE, color=_color, marker=_marker, label=_label)

plt.ylabel("Shapley decomposition")
plt.legend(loc="upper right")
plt.show()


# --- Figure 9
AVG_DICTS = [os.path.join(SHAPLEY_DIR, _d) for _d in ["avg_dict_ADL_nn_12.pkl",
    "avg_dict_ADP_xgb_12.pkl", "avg_dict_ADP_nn_12.pkl"]]
LABELS = ["ADL", "ADP (XGB)", "ADP (DNN)"]
ordered_bars_from_dict(AVG_DICTS, labels=LABELS)
