"""
Shapley values with various model-agnostic measures of dependence as
utility functions.
"""

import sys
import numpy as np
import numpy
import os
from itertools import combinations
import pandas as pd
import scipy
import tensorflow as tf
import warnings
warnings.filterwarnings("ignore")
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.FATAL)

def get_coalitions(n_players):

    players = range(n_players)

    coals = list()
    for n in range(1, n_players + 1):
        coals_size_n = list(combinations(players, n))
        if n == 1:
            coals_size_n = [[c[0]] for c in coals_size_n]
        coals += coals_size_n

    return coals

@tf.function
def compute_Akl_tf(x):
    akl = tf.norm(x[:, None, :] - x[None, :, :], axis=-1)

    akbar = tf.math.reduce_mean(akl, axis=1)
    akbar = tf.reshape(akbar, (-1, 1))
    albar = tf.math.reduce_mean(akl, axis=0)
    abar = tf.math.reduce_mean(akl, axis=None)

    _Akl = (akl - akbar - albar + abar)
    return _Akl

@tf.function
def dist_cov(dx, dy):

    sumkl = tf.math.reduce_sum(
        tf.math.multiply(dx, dy),
        axis=None
    )
    n = dx.shape[0]
    sumkl /= n*n

    return sumkl

@tf.function
def dist_cor(x, y):

    Akl = compute_Akl_tf(x)
    Bkl = compute_Akl_tf(y)

    numerator = tf.math.sqrt(
        tf.math.multiply(
            dist_cov(Akl, Akl),
            dist_cov(Bkl, Bkl)
        )
    )

    dcor_sqr = tf.math.divide(
        dist_cov(Akl, Bkl),
        numerator
    )

    return tf.math.sqrt(dcor_sqr)

#@tf.function
def compute_cf_list(x, y, coalitions):

    n_coals = coalitions.shape[0]
    cf_list = tf.TensorArray(tf.float32, size=n_coals)

    for i in tf.range(n_coals):

        coal = tf.gather(coalitions, i)
        x_c = tf.gather(x, coal, axis=1)
        dc = dist_cor(x_c, y)
        # In case dcor is zero or nan for some reason:
        dc = tf.where(tf.math.is_nan(dc), tf.zeros_like(dc), dc)
        cf_list.write(i, dc)

    return cf_list.stack()

def make_cf_dict_tf(x, y, n_players):
    """
    Input: n_players
    Make coalitions from the players, calculate the cfs values,
    return dict with keys=coalition and values=cf.
    """

    cf_dict = {}
    cf_dict[()] = 0
    c = get_coalitions(n_players)
    coalitions = tf.ragged.constant(c)
    cfs = compute_cf_list(x, y, coalitions)

    for _coal, _cf in zip(coalitions, cfs):
        cf_dict[tuple(_coal.numpy())] = _cf.numpy()

    return cf_dict

def calc_shapley_value(player_index, all_players, cf_dict):
    """
    Calculate the Shapley value for player index
    Input:
        all_players, list of player indices
        cf_dict, dictionary containing characteristic function values for all players
    """
    players = all_players.copy()

    if player_index in players:
        players.remove(player_index)

    num_players = len(players)
    coalition_sizes = list(range(num_players+1))
    value = 0
    player_tuple = (player_index,)

    for _size in coalition_sizes:
        coalition_value = 0
        coalitions_of_size_s = list(combinations(players, _size))
        for _coalition in coalitions_of_size_s:
            value_in_coalition = (cf_dict[tuple(sorted(_coalition + player_tuple))] - cf_dict[_coalition])
            coalition_value += value_in_coalition

        average_coalition_value = coalition_value/len(coalitions_of_size_s)
        value += average_coalition_value
    average_value = value/len(coalition_sizes)

    return average_value

def calc_shapley_values(x, y):
    """
    Returns the shapley values for features x and labels y using CF=dcor
    """
    n_players = x.shape[1]
    players = list(range(n_players))
    shapley_values = []
    cf_dict = make_cf_dict_tf(x, y, n_players)
    for _player in players:
        shapley_values.append(calc_shapley_value(_player, players, cf_dict))
    return shapley_values

def calc_split_shapley_values_from_file(datafile, output_dir,
        features_to_use=None, n_iter=10, n_samples=2000, ADL=True, ADP=True):
    """
    Calculate n_iter sets of ADL and ADP Shapley values for the feaures_to_use
    columns in the datafile. ADL uses column "target" and ADP uses column
    "prediciton".
    datafile = csv file containing X, targets, preds
    output_dir = output directory
    n_iter = number of data splits
    """
    data = pd.read_csv(datafile)
    try:
        data = data.drop("Unnamed: 0", axis=1)
    except:
        pass
    assert all(_feat in list(data.columns) for _feat in features_to_use)

    n_feats = len(features_to_use)

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    data_features = data.columns
    target_features = []
    if "target" in data_features:
        target_features.extend(["target"])
    if "prediction" in data_features:
        target_features.extend(["prediction"])

    if features_to_use is None:
        _data = data.copy()
    else:
        _features = features_to_use.copy()
        _features.extend(target_features)
        _data = data[_features]

    shapleys = []
    for _i in range(n_iter):
        print(f"Iteration: {_i}/{n_iter}")
        print("Samples: ", n_samples)
        sample_data = _data.sample(n_samples)
        print("Data array shape: ", _data.shape)
        _x = np.array(sample_data.drop(target_features, axis=1), dtype=np.float32)

        # --- Shapley values on targets
        if "target" in target_features and ADL:
            assertdir(os.path.join(output_dir, "ADL"))
            _y_true = np.array(sample_data.target, dtype=np.float32)
            _y_true = np.reshape(np.array(_y_true), (_y_true.shape[0], 1))
            _shapley_target = calc_shapley_values(_x, _y_true)
            shapleys.append(_shapley_target)
            np.save(f"{output_dir}/ADL/ADL_{n_feats}_feats_{n_samples}_samples_{_i}.npy",
                    np.array(_shapley_target))

        # --- Shapley values on predictions
        if "prediction" in target_features and ADP:
            assertdir(os.path.join(output_dir, "ADP"))
            _y_pred = np.array(sample_data.prediction, dtype=np.float32)
            _y_pred = np.reshape(np.array(_y_pred), (_y_pred.shape[0], 1))
            _shapley_pred = calc_shapley_values(_x, _y_pred)
            shapleys.append(_shapley_pred)
            np.save(f"{output_dir}/ADP/ADP_{n_feats}_feats_{n_samples}_samples_{_i}.npy",
                np.array(_shapley_pred))

        print("Continuing")
    print("Done")


def assertdir(_dir):
    if not os.path.isdir(_dir):
        os.makedirs(_dir)

if __name__ == "__main__":

    from global_settings import FEATURES_TO_USE, SHAPLEY_VALUES, SHAPLEY_DATA

    n_iter = 20
    n_samples = 2000
    assertdir(SHAPLEY_VALUES)
    POINT_DIR = os.path.join(SHAPLEY_VALUES, f"point12")
    assertdir(POINT_DIR)

    # --- ADLs
    OUTPUT_DIR = os.path.join(POINT_DIR, "targets")
    assertdir(OUTPUT_DIR)
    INPUT_FILE = os.path.join(SHAPLEY_DATA, f"/data_12.csv")

    calc_split_shapley_values_from_file(INPUT_FILE,
            OUTPUT_DIR,
            features_to_use=FEATURES_TO_USE,
            n_iter=n_iter,
            n_samples=n_samples,
            ADL=True, ADP=False)

    # --- ADPs
    for model in ["nn", "xgb"]:
        OUTPUT_DIR = os.path.join(POINT_DIR, model)
        assertdir(OUTPUT_DIR)
        INPUT_FILE = os.path.join(SHAPLEY_DATA, f"{model}_data_and_preds_12.csv")

        calc_split_shapley_values_from_file(INPUT_FILE,
                OUTPUT_DIR,
                features_to_use=FEATURES_TO_USE,
                n_iter=n_iter,
                n_samples=n_samples,
                ADL=False, ADP=True)

