"""
Grid scan for sneutrino ML project. Parallellised.
"""

import sys
import multiprocessing
import os
import itertools
import shutil

import sneu_components.spectrum_high as spectrum
from sneu_components import gb

# === Settings
CORES = 5
GENERATOR = "spheno"
RUN_NAME = "grid_scan"

GOOD_POINTS_DIR = "slha_grid_correct_mh"
WORKING_DIR = os.path.abspath("running_scan_folder_{0}".format(RUN_NAME))

if not os.path.isdir(GOOD_POINTS_DIR):
    os.mkdir(GOOD_POINTS_DIR)

if not os.path.isdir(WORKING_DIR):
    os.makedirs(WORKING_DIR)

# === Parameters and ranges
TANB = 10
QIN = 1000 #1466.80439
SIGN_MUE = 1

POINTS = [0,5,10,12,20,30,40,50]

M2_RANGE = [951.3, 5.12817807E+02, 3.41385579E+02, 2.67526629E+02, 1.00000000E+03, 6.00000000E+02, 8.53390396E+02, 2.00000000E+02] # [951, 1100, 2000]
M1_RANGE = [521.6, 9.60186838E+02, 2.99469719E+02, 7.53983237E+02, 5.99917075E+02, 4.46844958E+02, 2.50000000E+02, 4.99269426E+02] # [_i for _i in range(521, 900, 100*RANGE_FACTOR)]
M3_RANGE = [2554.5, 2.57815392E+03, 2.94323635E+03, 2.46683271E+03, 2.72486245E+03, 3.52994947E+03, 2.46683271E+03, 2.51440430E+03] # [_i for _i in range(2554, 6800, 100*RANGE_FACTOR)]

AT_RANGE = [_i for _i in range(0, -14000, -10)]

AB_RANGE = [-5544.2, -5.53500815E+03, -5.99406117E+03, -4.67366978E+03, -6.74606426E+03, -5.61148005E+03, -4.72247267E+03, -4.26697028E+03] # [_i for _i in range(-5544, -4000, 500*RANGE_FACTOR)]
ATAU_RANGE = [-3636.7, -3.62664826E+03, -2.75646753E+03, -2.93319885E+03, -3.74454684E+03, -2.61894649E+03, -3.03146946E+03, -3.01789773E+03] # [_i for _i in range(-3636, -1500, 500*RANGE_FACTOR)]

MHD_RANGE = [19642213.0, 1.71896368E+07, 2.14146673E+07, 2.15540298E+07, 2.23128245E+07, 2.01507460E+07, 2.49551788E+07, 2.40310204E+07] #[_i*10e+03 for _i in range(19, 23, 1*RANGE_FACTOR)]
MHU_RANGE = [-3657014.82, -4.50559377E+06, -5.71802067E+06, -2.05314552E+06, -3.54133895E+06, -3.84077539E+06, -3.42009443E+06, -3.09645307E+06] #[_i*10e+03 for _i in range(-36, -30, 2*RANGE_FACTOR)]


get_scan_range = lambda m1,m2,m3,at,ab,atau,mdh,mug: tuple(itertools.product(M1_RANGE, M2_RANGE, M3_RANGE,
                                                         AT_RANGE, AB_RANGE, ATAU_RANGE,
                                                         MHD_RANGE, MHU_RANGE))

#SCAN_RANGES = {p: get_scan_range(M1_RANGE[i], M2_RANGE[i], M3_RANGE[i], AT_RANGE, AB_RANGE[i], ATAU_RANGE[i], MHD_RANGE[i], MHU_RANGE[i]) for i, p in enumerate(POINTS)}
SCAN_RANGES = {p: tuple(itertools.product([M1_RANGE[i]], [M2_RANGE[i]], [M3_RANGE[i]],
                                                         AT_RANGE, [AB_RANGE[i]], [ATAU_RANGE[i]],
                                                         [MHD_RANGE[i]], [MHU_RANGE[i]])) for i, p in enumerate(POINTS)}

def spheno_worker((i,(m1, m2, m3, at, ab, atau, mhd, mhu))):
    """
    TODO
    """

    #iteration = gb.iteration
    #gb.iteration += 1
    iteration =  i

    #slha_input = "_temp_slha_in_proc_{0}".format(iteration)
    #slha_output = "_temp_slha_out_proc_{0}".format(iteration)
    slha_input = "i{0}".format(iteration)
    slha_output = "u{0}".format(iteration)
    slha_input_file = os.path.join(WORKING_DIR, slha_input)
    slha_output_file = os.path.join(WORKING_DIR, slha_output)

    param = {
        'M1' : m1,
        'M2' : m2,
        'M3' : m3,
        'At' : at,
        'Ab': ab,
        'Atau': atau,
        'm2Hd' : mhd,
        'm2Hu' : mhu,
        'slha_input_file' : slha_input_file,
        'slha_output_file' : slha_output_file,
        "valid" : False,
        }

    param.update(CONSTANTS)

    param = spectrum.main(param)                    # adds
                                                    # 'slha', m_higgs
                                                    # can set
                                                    # 'valid' -> False if:
                                                    # not 10000016 LSP
                                                    # not valid decay tables
                                                    # not correct sneu decay

    if not param["valid"]:

        os.remove(param["slha_input_file"])

        try:
            os.remove(param['slha_output_file'])
        except OSError:
            pass

        return False

    # --- if point has valid spectrum:
    # --- FeynHiggs correct Higgs mass
    param = spectrum.feynhiggs_correction(param)    # adds m_higgs_fh


    if (param['m_higgs_fh'] > 123 and param['m_higgs_fh'] < 127):

        # --- Store slha input and output files
        shutil.copy(param["slha_input_file"], GOOD_POINTS_DIR)
        shutil.copy(param["slha_output_file"], GOOD_POINTS_DIR)
    
    return "{0}\t{1}\t{2}\t{3}\t{4}\t{5}".format(iteration,
                                                 param['m_higgs_fh'],
                                                 POINT, at, m2, m3)
                                                     #param["br"], m1, m2, m3)

    #return "Wrong Higgs mass"#False


def handler(n_cores, scan_range, logfile):
    """
    TODO
    """

    print "Starting grid scan with {} iterations".format(len(scan_range))
    process = multiprocessing.Pool(n_cores)

    with open(logfile, "w") as _file:
        for result in process.imap(spheno_worker, list(enumerate(scan_range))):
            if result:
                _file.write(result+'\n')

def test_handler():
    """
    Run spheno_worker on one point
    """

    m1 = 521.6
    m2 = 951.3
    m3 = 2554.5

    at = -0.002
    ab= -5544.2
    atau= -3636.7

    mhd = 19642213.0
    mhu= -3657014.82

    print spheno_worker((m1, m2, m3, at, ab, atau, mhd, mhu))

if __name__ == '__main__':
    for point, scan_range in SCAN_RANGES.iteritems():
    
        if point == 0:
            # Point00
            ML11 = 314.7
            ML22 = 314.2
            ML33 = 240.2
            ME11 = 1094.0
            ME22 = 1094.0
            ME33 = 2485.4
            MQ11 = 2331.5
            MQ22 = 2331.5
            MQ33 = 7704
            MU11 = 2044.0
            MU22 = 2044.0
            MU33 = 7999.9
            MD11 = 2265.8
            MD22 = 2265.8
            MD33 = 7999.9       

        if point == 5:
            # Point 5
            ML11 = 3.14586870E+02
            ML22 = 3.15437272E+02
            ML33 = 1.07718636E+02
            ME11 = 1.08791720E+03
            ME22 = 1.09069224E+03
            ME33 = 1.01234774E+03
            MQ11 = 2.32567456E+03
            MQ22 = 2.31023298E+03
            MQ33 = 3.96514554E+03
            MU11 = 2.03768794E+03
            MU22 = 2.04903154E+03
            MU33 = 2.10973319E+03
            MD11 = 2.26328377E+03
            MD22 = 2.27281536E+03
            MD33 = 3.21811872E+03       

        if point == 10:
            # Point 10
            ML11 = 3.12245316E+02
            ML22 = 2.14750923E+02
            ML33 = 1.07375461E+02
            ME11 = 1.79850311E+03
            ME22 = 1.44354379E+03
            ME33 = 1.80229185E+03
            MQ11 = 2.45229198E+03
            MQ22 = 2.15574247E+03
            MQ33 = 4.71107137E+03
            MU11 = 1.34180582E+03
            MU22 = 1.14400968E+03
            MU33 = 1.37441650E+03
            MD11 = 1.46808640E+03
            MD22 = 1.24753765E+03
            MD33 = 3.34701778E+03     

        if point == 12:
            # Point12
            ML11 = 2.41774566E+02  
            ML22 = 2.64206470E+02  
            ML33 = 1.32103235E+02  
            ME11 = 1.18886540E+03  
            ME22 = 1.38036609E+03  
            ME33 = 1.92083150E+03  
            MQ11 = 2.41401189E+03  
            MQ22 = 2.99771701E+03  
            MQ33 = 4.83810867E+03  
            MU11 = 1.67200057E+03  
            MU22 = 2.06464345E+03  
            MU33 = 1.91244783E+03  
            MD11 = 1.77176749E+03  
            MD22 = 2.22296618E+03  
            MD33 = 3.52805749E+03   

        if point == 20:
            # Point 20
            ML11 = 3.03404435E+02 
            ML22 = 2.72299021E+02 
            ML33 = 1.36149510E+02 
            ME11 = 1.76774198E+03 
            ME22 = 1.70233543E+03 
            ME33 = 1.13352275E+03 
            MQ11 = 2.72208670E+03 
            MQ22 = 2.69618981E+03 
            MQ33 = 5.44970312E+03 
            MU11 = 1.80980213E+03 
            MU22 = 2.12698157E+03 
            MU33 = 6.55242135E+03 
            MD11 = 1.63201514E+03 
            MD22 = 1.60570337E+03 
            MD33 = 7.61276481E+03       

        if point == 30:
            # Point 30
            ML11 = 3.84281738E+02
            ML22 = 3.06396985E+02
            ML33 = 1.53198492E+02
            ME11 = 1.81997919E+03
            ME22 = 1.42382842E+03
            ME33 = 1.97185228E+03
            MQ11 = 2.95081415E+03
            MQ22 = 2.84584951E+03
            MQ33 = 6.01697476E+03
            MU11 = 2.70755252E+03
            MU22 = 2.19102359E+03
            MU33 = 6.28373770E+03
            MD11 = 1.84459875E+03
            MD22 = 2.49370377E+03
            MD33 = 6.98177791E+03  

        if point == 40:
            # Point 40
            ML11 = 2.18282831E+02
            ML22 = 2.22690105E+02
            ML33 = 1.11345053E+02
            ME11 = 1.11569852E+03
            ME22 = 1.30047894E+03
            ME33 = 1.55363786E+03
            MQ11 = 2.49725658E+03
            MQ22 = 2.49390453E+03
            MQ33 = 6.00551540E+03
            MU11 = 1.32745934E+03
            MU22 = 1.07222462E+03
            MU33 = 5.56714934E+03
            MD11 = 1.52600169E+03
            MD22 = 1.03722239E+03
            MD33 = 6.74957657E+03

        if point == 50:
            # Point 50
            ML11 = 2.10258532E+02 
            ML22 = 2.15496385E+02 
            ML33 = 1.07748193E+02 
            ME11 = 1.86114275E+03 
            ME22 = 1.31518245E+03 
            ME33 = 1.55648178E+03 
            MQ11 = 2.47490996E+03 
            MQ22 = 2.97323841E+03 
            MQ33 = 5.78566402E+03 
            MU11 = 2.32885265E+03 
            MU22 = 1.35522699E+03 
            MU33 = 7.30307859E+03 
            MD11 = 2.90195608E+03 
            MD22 = 2.91721940E+03 
            MD33 = 9.25655222E+03       
        
        CONSTANTS = {
            'ML11' : ML11,
            'ML22' : ML22,
            'ML33' : ML33,
            'ME11' : ME11,
            'ME22' : ME22,
            'ME33' : ME33,
            'MQ11' : MQ11,
            'MQ22' : MQ22,
            'MQ33' : MQ33,
            'MU11' : MU11,
            'MU22' : MU22,
            'MU33' : MU33,
            'MD11' : MD11,
            'MD22' : MD22,
            'MD33' : MD33,
            # ...
            'Qin' : QIN,
            'tanb_ext' : TANB,
            'signmu' : SIGN_MUE,
            'generator' : GENERATOR,
            'modsel'    : 0,
            # ...
            }   

        POINT = point
        # === Log output file
        LOGFILE = "grid_scan_{}.csv".format(point)
        with open(LOGFILE, "w") as _file:
            _file.write("Iteration\t m_Higgs\t point \t At \t M2 \t M3")
            #_file.write("Iteration\t m_Higgs\t br \t M1 \t M2 \t M3")

        try:
            handler(CORES, scan_range, LOGFILE)
        except KeyboardInterrupt:
            sys.exit()

    #test_handler()
