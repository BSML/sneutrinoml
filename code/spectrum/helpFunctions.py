#from plotLSP import PlotLSP
def get_lsp(pyslha_masses):
   '''Get the ID of the LSP in this parameter point'''

   # A start index and mass
   lsp_id = -1
   lsp_m = 100000

   # Loop through all the masses
   for key,value in pyslha_masses.items():
      
      # Check if the particle is a supersymmetric particle
      if int(key)<1000000:
         continue # if not a susy particle
      
      # Check if the mass of the susy particle is lighter then the previous
      if lsp_m > abs(int(value)):       
         lsp_id = int(key)
         lsp_m = int(value)
        
      # Makes an error if no LSP particles was found.
      assert not lsp_id == -1

   return lsp_id

def is_lsp(id, pyslha_masses):
   '''Check if the id is the LSP/NLSP'''
   #self.plotLSP.append_point(self.x,self.y,lsp_id)
   lsp_id = get_lsp(pyslha_masses)
   return id == lsp_id

def correctHiggsMass(m_higgs):
   '''Check if the point have a correct higgs mass'''
   return  123 < m_higgs and m_higgs < 127



































class ParameterPoint:
   # Particle number indexes
   higgs_index = 25
   smuon = 1000013
   sneu_muon = 1000014
   stau = 1000015
   sneu_tau = 1000016

   tau = 15
   neu_tau = 16
   muon = 13
   neu_muon = 14


   def __init__(self,point):
      self.decays = point.decays
      self.masses = point.blocks['MASS']
      self.m_higgs = self.masses[self.higgs_index]
      self.m_sneu = self.masses[self.sneu_tau]
      self.m_stau = self.masses[self.stau]
      
   def isAccepted(self):
      '''Check if the point is accepted'''
      return self.checkIfBrExists() #self.correctHiggsMass()# and self.is_lsp(self.sneu_tau)

   def correctHiggsMass(self):
      '''Check if the point have a correct higgs mass'''
      return  123 < self.m_higgs and self.m_higgs < 127

   def is_lsp(self,id):
      '''Check if the id is the LSP/NLSP'''
      #self.plotLSP.append_point(self.x,self.y,lsp_id)
      lsp_id = self.get_lsp()
      return id == lsp_id
   
   def get_lsp(self):
      '''Get the ID of the LSP in this parameter point'''
      
      # A start index and mass
      lsp_id = -1
      lsp_m = 100000

      # Loop through all the masses
      for key,value in self.masses.items():
        # Check if the particle is a supersymmetric particle
        if int(key)<1000000:
          continue # if not a susy particle
       
        # Check if the mass of the susy particle is lighter then the previous
        if lsp_m > abs(int(value)):       
          lsp_id = int(key)
          lsp_m = int(value)
        
        # Makes an error if no LSP particles was found.
        assert not lsp_id == -1

      return lsp_id


   def printable_string(self):
      '''A printable string for some importante featues of the parameter point'''
      return str(self.m_higgs)+','\
            +str(self.m_sneu)+','\
            +str(self.m_sneu-self.m_stau)+','\
            +str(self.get_lsp())
            

   def getBranchingRatios(self):
      '''Returns the branching ratios of the smuon and the sneu_smuon'''
      smuon_brs = self.decays[self.smuon].decays
      sneu_muon_brs = self.decays[self.sneu_muon].decays

      return smuon_brs, sneu_muon_brs

   def checkIfBrExists(self):
     '''Checks if the brancking ratios corresponding to the 
        EW production signal is present
        
        return: True if both the 1000013 --> 1000016 -15 -14 
                and 1000014 --> 1000016 13 -15 exits
                False if one of them is missing
      '''

     # Gets the branching ratio for the smuon and sneu_smuon
     smuon_brs, sneu_muon_brs = self.getBranchingRatios() 
     
     # Gets the index of the branching ratio, return -1 if it dont exist
     inxSmuon_br = self.indexBranchingRatio(smuon_brs, [-1000016, 15, 14])
     inxSneu_br = self.indexBranchingRatio(sneu_muon_brs, [1000016, 13, -15])
      
     # Returns only if both branching ratios is present
     return not (inxSmuon_br == -1 or inxSneu_br == -1)


   def indexBranchingRatio(self, branchingRatios, brToCompare):
      '''Gets the index of the brancing ratio
         
         input: 
            - BranchingRatios: Array of branching raito objects
            - brToCompare: The branching ratio to find the index of
         return:
            -1: if branching ratio not found
            index: index of the branching ratio in the given array
      '''

      # Start at index 0 and try all branching ratios untill it is found
      i = 0
      for br in branchingRatios:
         if br.ids == brToCompare:
            return i
         i += 1

      # return -1 if not found
      return -1



      
      






