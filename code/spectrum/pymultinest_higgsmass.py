from argparse import ArgumentParser
import os
import shutil
import math
import pymultinest
#import scipy as sp

import sneu_components.spectrum_high as spectrum
import sneu_components.cross_section as cross_section

from sneu_components import gb

from sneu_components.likelihoods import loglike_normal
from sneu_components.likelihoods import loglike_lin
from sneu_components.likelihoods import like_lin
from sneu_components.likelihoods import LL_MIN

# ================================================================================

# === Prior functions ============================================================

def heavisideprior(value, cutoff=0.5):
    """
    Input:
        value: input value, int or float
        cutoff: cutoff value, int or float (default 0.5)
    Returns:
        0 if value is below 1 if value is above cutoff.
    """
    assert isinstance(value, (int, float))
    assert isinstance(cutoff, (int, float))

    if value <= cutoff:
        return 0.0

    return 1.0

def logpriorflat(lower, upper, randvar):
    """
    Log(flat prior) function
    """
    outvar = lower*math.exp(randvar*math.log(upper/lower))
    return outvar

# ================================================================================

# ================================================================================
# === PyMultinest functions

def myprior(cube, ndim, nparams):
    """
    Cube entries varied by PyMultinest.
    The prior function scales the unit cube entries for the loglike function.
    """
    # --- Scan ranges ------------------------------------------------------------

    # --- map [0,1] to required ranges:
    var_M1 = 0  + (1000)*cube[PARAMETERS.index('M1')]           #521.7
    #var_M2 = 250  + 750*heavisideprior(cube[PARAMETERS.index('M2')]) #250, 1000
    var_M2 = CONSTANTS["M2"]  + (200)*cube[PARAMETERS.index('M2')] # Around M2
    var_M3 = 2000  + (4000-2000)*cube[PARAMETERS.index('M3')]           #2554.5

    var_At = -0.003 + (0.003-0.001)*cube[PARAMETERS.index('At')]        #-0.002
    #var_At = -6000 + (6000)*cube[PARAMETERS.index('At')]
    var_Ab = -7000 + (7000-4000)*cube[PARAMETERS.index('Ab')]        #-5544.2
    var_Atau = -4000 + (4000-2000)*cube[PARAMETERS.index('Atau')]    #-3636.7

    var_mhd = 2.0e7 + (2e7-1.5e7)*cube[PARAMETERS.index('m2Hd')]           #19642213.0
    var_mhu = -4e6 + (4e6-3e6)*cube[PARAMETERS.index('m2Hu')]             #-3657014.82
    #var_mhd = 20 + (20-15)*cube[PARAMETERS.index('m2Hd')]           #19.64
    #var_mhu = -10 + (0+20)*cube[PARAMETERS.index('m2Hu')]        #-3.64

    var_ML11 = 200 + (400-200)*cube[PARAMETERS.index('ML11')]     #314.7
    var_ML22 = 200 + (400-200)*cube[PARAMETERS.index('ML22')]     #314.2
    var_ML33 = 100 + (200-100)*cube[PARAMETERS.index('ML22')]     #107.4
    var_ME11 = 1000 + (2000-1000)*cube[PARAMETERS.index('ME11')]     #1094.0
    var_ME22 = 1000 + (2000-1000)*cube[PARAMETERS.index('ME22')]     #1094.0
    var_ME33 = 1000 + (2000-1000)*cube[PARAMETERS.index('ME33')]     #1010.0
    var_MQ11 = 2000 + (3000-2000)*cube[PARAMETERS.index('MQ11')]     #2331.5
    var_MQ22 = 2000 + (3000-2000)*cube[PARAMETERS.index('MQ22')]     #2331.5
    var_MQ33 = 4000 + (7000-4000)*cube[PARAMETERS.index('MQ33')]     #3969.7
    var_MU11 = 1000 + (3000-1000)*cube[PARAMETERS.index('MU11')]     #2044.0
    var_MU22 = 1000 + (3000-1000)*cube[PARAMETERS.index('MU22')]     #2044.0
    var_MU33 = 1000 + (8000-1000)*cube[PARAMETERS.index('MU33')]     #2108.2?
    var_MD11 = 1000 + (3000-1000)*cube[PARAMETERS.index('MD11')]     #2265.8
    var_MD22 = 1000 + (3000-1000)*cube[PARAMETERS.index('MD22')]     #2265.8
    var_MD33 = 6000 + (10000-6000)*cube[PARAMETERS.index('MD33')]     #3212.7?
    #-----------------------------------------------------------------------------

    # --- Fill the cube ----------------------------------------------------------
    cube[PARAMETERS.index('M1')] = var_M1
    cube[PARAMETERS.index('M2')] = var_M2
    cube[PARAMETERS.index('M3')] = var_M3

    cube[PARAMETERS.index('At')] = var_At
    cube[PARAMETERS.index('Ab')] = var_Ab
    cube[PARAMETERS.index('Atau')] = var_Atau

    cube[PARAMETERS.index('m2Hu')] = var_mhu
    cube[PARAMETERS.index('m2Hd')] = var_mhd

    cube[PARAMETERS.index('ML11')] = var_ML11
    cube[PARAMETERS.index('ML22')] = var_ML22
    cube[PARAMETERS.index('ML33')] = var_ML33
    cube[PARAMETERS.index('ME11')] = var_ME11
    cube[PARAMETERS.index('ME22')] = var_ME22
    cube[PARAMETERS.index('ME33')] = var_ME33
    cube[PARAMETERS.index('MQ11')] = var_MQ11
    cube[PARAMETERS.index('MQ22')] = var_MQ22
    cube[PARAMETERS.index('MQ33')] = var_MQ33
    cube[PARAMETERS.index('MU11')] = var_MU11
    cube[PARAMETERS.index('MU22')] = var_MU22
    cube[PARAMETERS.index('MU33')] = var_MU33
    cube[PARAMETERS.index('MD11')] = var_MD11
    cube[PARAMETERS.index('MD22')] = var_MD22
    cube[PARAMETERS.index('MD33')] = var_MD33

def myloglike(cube, ndim, nparams):

    # --- Keep track of scan iteration ----------------------------------------
    iteration = gb.iteration
    cube[PARAMETERS.index("iteration")] = iteration
    gb.iteration += 1

    if gb.iteration % 100 == 0:
        print "Iteration {0}".format(gb.iteration)

    # --- Read values from cube -----------------------------------------------
    var_M1 = cube[PARAMETERS.index('M1')]
    var_M2 = cube[PARAMETERS.index('M2')]
    var_M3 = cube[PARAMETERS.index('M3')]
    var_At = cube[PARAMETERS.index('At')]
    var_Ab = cube[PARAMETERS.index('Ab')]
    var_Atau = cube[PARAMETERS.index('Atau')]
    var_mhd = cube[PARAMETERS.index('m2Hd')]
    var_mhu = cube[PARAMETERS.index('m2Hu')]
    var_ML11 = cube[PARAMETERS.index('ML11')]
    var_ML22 = cube[PARAMETERS.index('ML22')]
    var_ML33 = cube[PARAMETERS.index('ML33')]
    var_ME11 = cube[PARAMETERS.index('ME11')]
    var_ME22 = cube[PARAMETERS.index('ME22')]
    var_ME33 = cube[PARAMETERS.index('ME33')]
    var_MQ11 = cube[PARAMETERS.index('MQ11')]
    var_MQ22 = cube[PARAMETERS.index('MQ22')]
    var_MQ33 = cube[PARAMETERS.index('MQ33')]
    var_MU11 = cube[PARAMETERS.index('MU11')]
    var_MU22 = cube[PARAMETERS.index('MU22')]
    var_MU33 = cube[PARAMETERS.index('MU33')]
    var_MD11 = cube[PARAMETERS.index('MD11')]
    var_MD22 = cube[PARAMETERS.index('MD22')]
    var_MD33 = cube[PARAMETERS.index('MD33')]

    # --- Fill dictionary ---------------------------------------------------------

    # --- Associate SLHA output and input files name with iteration number
    slha_input = "slha_input_file_{0}".format(iteration)
    slha_output = "slha_output_file_{0}".format(iteration)

    slha_input_file = os.path.abspath(os.path.join(WORKING_DIR, slha_input))
    slha_output_file = os.path.abspath(os.path.join(WORKING_DIR, slha_output))

    param = {
        #... Varied during run:
        'M1' : var_M1,
        'M2' : var_M2,
        'M3' : var_M3,
        'At' : var_At,
        'Ab': var_Ab,
        'Atau': var_Atau,
        'm2Hd' : var_mhd,
        'm2Hu' : var_mhu,
        'ML11' : var_ML11,
        'ML22' : var_ML22,
        'ML33' : var_ML33,
        'ME11' : var_ME11,
        'ME22' : var_ME22,
        'ME33' : var_ME33,
        'MQ11' : var_MQ11,
        'MQ22' : var_MQ22,
        'MQ33' : var_MQ33,
        'MU11' : var_MU11,
        'MU22' : var_MU22,
        'MU33' : var_MU33,
        'MD11' : var_MD11,
        'MD22' : var_MD22,
        'MD33' : var_MD33,
        # ...
        # ... Constants not recorded ...
        'slha_input_file' : slha_input_file,
        'slha_output_file' : slha_output_file,
        'valid' : False,                            # Failing modules can set this to False
        'warning' : None,                           # Modules can return warning message
        # ...
        }

    param.update(CONSTANTS)

    #------------------------------------------------------------------------------
    # ---  Modules section --------------------------------------------------------

    # --- Calculate spectrum ------------------------------------------------------
    param = spectrum.main(param)                    # adds
                                                    # 'slha', m_higgs
                                                    # can set
                                                    # 'valid' -> False if:
                                                    # not 10000016 LSP
                                                    # not valid decay tables
                                                    # not correct sneu decay
    if not param['valid']:
        ll_scan = LL_MIN
        cube[PARAMETERS.index('ll_scan')] = ll_scan

        os.remove(param["slha_input_file"])

        try:
            os.remove(param['slha_output_file'])
        except OSError:
            pass

        return ll_scan
    #------------------------------------------------------------------------------


    # --- if point has valid spectrum:
    # --- FeynHiggs correct Higgs mass
    param = spectrum.feynhiggs_correction(param)    # adds m_higgs_fh

    #------------------------------------------------------------------------------

    if True:
    #try:
        # --- Higgs mass likelihood
        ll_higgs = loglike_normal(param['m_higgs_fh'], 125, SIGMA_HIGGS)
                                                        # Mean=125GeV
                                                        # max = -0.23, at mh=125

        # --- If it is a potential point:
        # --- store SLHA file and write logfile

        if (param['m_higgs_fh'] > 123 and param['m_higgs_fh'] < 127):
            print "Good Higgs mass"

        #if param["br"] > OK_BR_VALUE:
        #    print "OK BR"

        if (param['m_higgs_fh'] > 123 and param['m_higgs_fh'] < 127 and
                param["br"] > OK_BR_VALUE):
            # --- Store slha input and output file
            shutil.copy(param["slha_input_file"], GOOD_POINTS_DIR)
            shutil.copy(param["slha_output_file"], GOOD_POINTS_DIR)

            print("OK combined")

            # --- Loglikelihoods
            ll_br = loglike_lin(param['br'], coeff=-50.0)
                                                    # max=0 (br=1),

            # --- Record in logfile
            with open(LOGFILE, "a") as _file:
                _file.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}".format(iteration,
                                                                  param['m_higgs_fh'],
                                                                  param["br"],
                                                                  param["M1"],
                                                                  param["M2"],
                                                                  param["M3"]))
        else:
            ll_br = -100

        # --- Record calculated parameters if the point is not discarded ----------
        cube[PARAMETERS.index('lsp')] = param['lsp']
        cube[PARAMETERS.index('mlsp')] = param['mlsp']
        cube[PARAMETERS.index('m_higgs')] = param['m_higgs']
        cube[PARAMETERS.index('m_higgs_fh')] = param['m_higgs_fh']
        cube[PARAMETERS.index('br')] = param['br']

        # --- end of modules and calculations --------------------------------------
        # --------------------------------------------------------------------------

        # ==========================================================================
        # === LIKELIHOODS ==========================================================

        # --- The likelihood that guides the scan ----------------------------------
        ll_scan = ll_br + ll_higgs

        # --- Add likelihoods to the cube ------------------------------------------
        cube[PARAMETERS.index('ll_higgs')] = ll_higgs
        cube[PARAMETERS.index('ll_br')] = ll_br
        cube[PARAMETERS.index('ll_scan')] = ll_scan


        # === END LIKELIHOODS ======================================================
        # ==========================================================================


    # ------------------------------------------------------------------------------
    else:
    #except Exception as Ex: #TypeError:
        print "FATAL ERROR IN POINT {0}".format(param)
        print "Caught Exception ", Ex

        ll_scan = LL_MIN
        cube[PARAMETERS.index('ll_scan')] = ll_scan

    # ------------------------------------------------------------------------------

    # -- Clean up and return -------------------------------------------------------
    try:
        os.remove(param['slha_input_file'])
        os.remove(param['slha_output_file'])
    except OSError:
        pass

    return ll_scan


# ==================================================================================
# === MAIN

if __name__ == '__main__':

    GENERATOR = 'spheno'

    M2 = 200
    TANB = 10
    QIN = 1000
    SIGMA_HIGGS = 0.5

    OK_BR_VALUE = 3.5e-02

    CONSTANTS = {
        'generator' : GENERATOR,
        'modsel'    : 0,
        'Qin' : QIN,
        'tanb_ext' : TANB,
        "signmu" : 1,
        "M2" : M2,
        }

    # ------------------------------------------------------------------------------
    # --- All (in- and output) parameters
    # ------------------------------------------------------------------------------

    PARAMETERS = [
        # --- Input parameters
        'M1',
        'M2',
        'M3',
        'At',
        'Ab',
        'Atau',
        'm2Hd',
        'm2Hu',
        'ML11',
        'ML22',
        'ML33',
        'ME11',
        'ME22',
        'ME33',
        'MQ11',
        'MQ22',
        'MQ33',
        'MU11',
        'MU22',
        'MU33',
        'MD11',
        'MD22',
        'MD33',

        # --- Output parameters:
        "lsp", "mlsp",
        "m_higgs",
        "m_higgs_fh",
        "br",

        # --- Likelihoods:
        "ll_higgs",
        "ll_br",
        "ll_scan",
        # ---
        "iteration",
        ]

    N_PARAMS = len(PARAMETERS)

    # === Log output file
    LOGFILE = "gridscan.log"
    with open(LOGFILE, "w") as _file:
        _file.write("Iteration\t m_Higgs\t br \t M1 \t M2 \t M3")


    # ------------------------------------------------------------------------------
    # --- Collect arguments
    # ------------------------------------------------------------------------------

    PARSER = ArgumentParser(description='Plot scan result')
    PARSER.add_argument('-dir', '--directory', help='Save scan result to directory')
    PARSER.add_argument('-n', '--name', help="Run name",
                        default="low_At_mHscan_M2_{0}".format(M2))
    PARSER.add_argument('-v', '--verbose', help='Verbose PyMultinest run', action='store_true')
    PARSER.add_argument('-t', '--testrun', action='store_true',
                        help='Run for testing modules, few points checked.')

    PARGS = PARSER.parse_args()

    if PARGS.directory:
        DIR = PARGS.directory
    else:
        DIR = 'chains_sc'

    if not os.path.isdir(DIR):
        os.mkdir(DIR)

    RUN_NAME = PARGS.name
    CHAINS = os.path.join(DIR, RUN_NAME)

    print 'Result written to {0}'.format(CHAINS)

    print 'Running multinest...'

    VERBOSE = PARGS.verbose

    WORKING_DIR = os.path.abspath("running_scan_folder_{0}".format(RUN_NAME))
    GOOD_POINTS_DIR = os.path.abspath("correct_mh_{0}".format(RUN_NAME))

    # ------------------------------------------------------------------------------
    # --- Directory for SLHA files
    # ------------------------------------------------------------------------------

    if not os.path.isdir(GOOD_POINTS_DIR):
        os.mkdir(GOOD_POINTS_DIR)

    if not os.path.isdir(WORKING_DIR):
        os.makedirs(WORKING_DIR)

    # ------------------------------------------------------------------------------
    # --- Run PyMultinest
    # ------------------------------------------------------------------------------

    if PARGS.testrun:
        pymultinest.run(
            myloglike, myprior, N_PARAMS,
            verbose=VERBOSE,
            importance_nested_sampling=False,
            resume=False,
            sampling_efficiency=0.8,
            n_live_points=300,
            evidence_tolerance=0.5,
            log_zero=-1.0e99,
            n_iter_before_update=50,
            outputfiles_basename=CHAINS
            )
    else:
        pymultinest.run(
            myloglike, myprior, N_PARAMS,
            verbose=VERBOSE,
            importance_nested_sampling=False,
            resume=False,
            sampling_efficiency=0.8, #0.01,
            n_live_points=500, # 5000,
            evidence_tolerance=0.1, #0.01,
            log_zero=-1.0e99,
            n_iter_before_update=500,
            outputfiles_basename=CHAINS
            )

    # ------------------------------------------------------------------------------
    # --- Write relevant info to file after PyMultinest finishes
    # ------------------------------------------------------------------------------

    HEADER = ("#                     weight                        -2ll "
              +(" ").join(str(x).rjust(27) for x in PARAMETERS)+"\n")
    open(CHAINS+"data.dat", "w").write(HEADER+open(CHAINS+".txt").read())

    # ------------------------------------------------------------------------------
    # --- Clean up
    # ------------------------------------------------------------------------------

    for fname in os.listdir(WORKING_DIR):
        if fname.startswith("_temp_slha_input_file_proc_") or fname == "Messages.out":
            os.remove(os.path.join(WORKING_DIR, fname))

# ==================================================================================
