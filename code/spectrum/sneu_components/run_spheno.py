"""
Module for running SPheno on existing SLHA file.
"""

import os
import subprocess
import socket
import random
import sys

HOSTNAME = socket.gethostname()

CURRENT_PATH = os.getcwd()

def main(param):
    """
    Run SPheno on existing SLHA file from param
    """

    if not os.path.isfile(param['slha_input_file']):
        print "==================================================================="
        print "Input file missing"
        print "==================================================================="
        return False

    try:
        os.remove(param['slha_output_file'])
    except OSError:
        pass

    if HOSTNAME == '':
        sphenopath = 'SPheno-4.0.3/bin'
    
    # -- In case of large filenames. Only a problem spheno.
    #temp_inname = "_spheno_slha_in_{0}".format(random.randint(1, 1000))
    #temp_outname = "_spheno_slha_out_{0}".format(random.randint(1, 1000))

    #os.rename(param['slha_input_file'], temp_inname)
    temp_inname = param['slha_input_file']
    temp_outname = param['slha_output_file']

    process = subprocess.Popen(
        [os.path.join(sphenopath, 'SPheno'), temp_inname, temp_outname],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        )

    process.wait()

    std_out_value, std_err_value = process.communicate()

    if std_err_value.strip() is not "" or std_out_value.strip() is not "":
        # --- SPheno only writes to command line when there's something wrong
        return False

    #os.rename(temp_inname, param['slha_input_file'])

    #try:
    #    os.rename(temp_outname, param['slha_output_file'])
    #except OSError:
    #    return False

    return True

def next_feynhiggs(param):
    """
    Run FeynHiggs on SPheno produced spectrum
    """

    if HOSTNAME == '':
        fhpath = 'FeynHiggs-2.14.2/x86_64-Linux/bin'

    fh_infile = param['slha_output_file']
    devnull = open(os.devnull, 'w')

    process = subprocess.Popen(
        [os.path.join(fhpath, 'FeynHiggs'), fh_infile],
        stdin=devnull,
        stdout=devnull,
        stderr=devnull,
        )

    process.wait()

    param['fh_corrected_slha_file'] = str(param['slha_output_file'])+".fh-001"

    return param

