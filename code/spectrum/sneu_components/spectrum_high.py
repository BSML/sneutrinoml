"""
Module for spectrum calculation
Fills dictionary with lifetimes and masses
"""
import shutil
import sys
import os
import pyslha

import modules as mod
import susymisc as misc
import run_spheno
#import sneu_components.scan_slhafile_maker as scan_slhafile_maker
from filemisc import LH_from_dict
from slhamisc import slha_input_file_from_dict


def main(param):
    """
    Calculates spectrum from points in param.
    Returns param with additional entries:
    key 'lsp' tuple (PDG code, mass)
    key 'm_higgs' float lightest Higgs mass
    """

    generator = param['generator']

    #LH_from_dict(param, filename=param['slha_input_file'], generator=generator)

    slha_input_file_from_dict(param, filename=param['slha_input_file'], generator=generator)

    #response = mod.make_spectrum(param, generator)#,input_filename='temp_slha')

    if generator.lower() in 'spheno':
        response = run_spheno.main(param)#,input_filename='temp_slha')

    else:
        print "Cannot calculate spectrum using ", generator
        print "Only SPheno is implemented"
        return param

    if response:
        try:
            slha_file = pyslha.readSLHAFile(param['slha_output_file'])
            mass_block = slha_file.blocks['MASS']
            param['m_higgs'] = mass_block[25]
            param['lsp'], param['mlsp'] = misc.lsp(mass_block) # function returns tuple

            if param['lsp'] == 1000016:
                try:
                    sneu_mu_width = slha_file.decays[1000014].totalwidth
                    if sneu_mu_width == 0:
                        print "Decay table problem"
                        return param

                    sneu_mu_decays = slha_file.decays[1000014].decays

                    for index, _ in enumerate(sneu_mu_decays):
                        if sneu_mu_decays[index].ids == [1000016, 13, -15]:
                            param['br'] = sneu_mu_decays[index].br
                            param['valid'] = True

                except Exception as ex:
                    print "Exception caught while reading decay table"
                    print ex

        except (pyslha.ParseError, pyslha.AccessError):
            print "Pyslha can't read SLHA file from SPheno."

    return param

def feynhiggs_correction(param):
    """
    Call Feynhiggs on SLHA file stored in param, and get the corrected Higgs mass
    """

    if not param['valid']:
        return param

    param = run_spheno.next_feynhiggs(param)

    try:
        slha_file = pyslha.readSLHAFile(param['fh_corrected_slha_file'])
        mass_block = slha_file.blocks['MASS']
        param['m_higgs_fh'] = mass_block[25]

    except (pyslha.ParseError, pyslha.AccessError):
        print "Pyslha can't read SLHA file from FeynHiggs."
        param['valid'] = False

    return param


# -----------------------------------------------------------------------------------------
# --- For testing -------------------------------------------------------------------------

if __name__ == '__main__':
    PARAM = {'valid' : True,
            'generator' : 'spheno',
             "modsel" : 0,
             'Qin' : 1.46680439E+03,
             "signmu" : 1.00000000E+00,

             "tanb_ext" : 10,

             "M1" : 5.21671045E+02,
             "M2" : 9.51264575E+02,
             "M3" : 2.55452888E+03,
             'At': -2.04227288E-03,
             "Ab" : -5.54420118E+03,
             "Atau" : -3.63669853E+03,

             #"m2Hd" : 19.6422130,
             #"m2Hu" : -3.65701482,

             "m2Hd" : 1.96422130E+07,
             "m2Hu" : -3.65701482E+06,


             "ML11" : 3.14692692E+02,
             "ML22" : 3.14187063E+02,
             "ML33" : 2.40191846E+02,

             "ME11" : 1.09408608E+03,
             "ME22" : 1.09379345E+03,
             "ME33" : 2.48542999E+03,

             "MQ11" : 2.33149338E+03,
             "MQ22" : 2.33147596E+03,
             'MQ33' : 7.70402060E+03,

             "MU11" : 2.04402141E+03,
             "MU22" : 2.04400251E+03,
             "MU33" : 7.99995862E+03,

             "MD11" : 2.26589270E+03,
             "MD22" : 2.26587352E+03,
             "MD33" : 7.99995862E+03,

             'dir_name' : 'slha_dir',
             'slha_input_file' : 'slha_input_for_test',
             'slha_output_file' : 'slha_output_for_test',
            }
    PARAM = main(PARAM)

    if PARAM['valid']:
        print "LSP: {0}".format(PARAM['lsp'])
        print "LSP mass: {0}".format(PARAM['mlsp'])
        print "Higgs mass: {0} GeV".format(PARAM['m_higgs'])
        print "BR: {0} GeV".format(PARAM['br'])
        print "\nNext: Running FeynHiggs.\n..."
        PARAM = feynhiggs_correction(PARAM)
        print "FeynHiggs corrected Higgs mass: {0} GeV".format(PARAM['m_higgs_fh'])
    else:
        print "Invalid point. Probably couldn't calculate decay tables."

