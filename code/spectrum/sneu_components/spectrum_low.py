"""
Module for spectrum calculation
Fills dictionary with lifetimes and masses
"""
import sys
import os
import pyslha

import common.modules as mod
import common.susymisc as misc
import run_spheno
#import sneu_components.scan_slhafile_maker as scan_slhafile_maker
import scan_slhafile_maker


def main(param):
    """
    Calculates spectrum from points in param.
    Returns param with additional entries:
    key 'lsp' tuple (PDG code, mass)
    key 'm_higgs' float lightest Higgs mass
    """
    generator = param['generator']
    scan_slhafile_maker.main(param)
    #response = mod.make_spectrum(param, generator)#,input_filename='temp_slha')
    if generator.lower() in 'spheno':
        response = run_spheno.main(param)#,input_filename='temp_slha')
    else:
        print "No other spectrum generators than spheno are currently possible."
        sys.exit()

    # -----------------------------------------------------------------------------

    param['valid'] = False

    if not response:
        return param
    try:
        slha_file = pyslha.readSLHAFile(param['slha_output_file'])
        mass_block = slha_file.blocks['MASS']
        param['m_higgs'] = mass_block[25]

        param['lsp'], param['mlsp'] = misc.lsp(mass_block) # function returns tuple

        if param['lsp'] == 1000016:
            try:
                sneu_mu_width = slha_file.decays[1000014].totalwidth
                if sneu_mu_width == 0:
                    print "Decay table problem"
                    return param

                sneu_mu_decays = slha_file.decays[1000014].decays
                for index, _ in enumerate(sneu_mu_decays):
                    if sneu_mu_decays[index].ids == [1000016, 13, -15]:
                        param['br'] = sneu_mu_decays[index].br
                        param['valid'] = True

            except Exception as ex:
                print "Exception caught while reading decay table"
                print ex

    except (pyslha.ParseError, pyslha.AccessError):
        print "Pyslha can't read SLHA file from SPheno."

    return param

def xs_calc(param):
    """calculates the cross section for the ew and qcd signal
      return the param dictionary with the keys xs_ew and xs_qcd
    """
    from xs_calculation import *
    try:
        # For testing
        create_herwig_input_file('EW', slha_output)
        create_herwig_input_file('QCD', slha_output)

        run_herwig('EW')
        run_herwig('QCD')

        # Get the XS from the output file
        param['xs_ew'] = get_xs_from_herwig('EW')
        param['xs_qcd'] = get_xs_from_herwig('QCD')

    except Exception as ex:
        print ex

    return param


def feynhiggs_correction(param):
    """
    Call Feynhiggs on SLHA file stored in param, and get the corrected Higgs mass
    """

    if not param['valid']:
        return param

    param = run_spheno.next_feynhiggs(param)
    try:
        slha_file = pyslha.readSLHAFile(param['fh_corrected_slha_file'])
        mass_block = slha_file.blocks['MASS']
        param['m_higgs_fh'] = mass_block[25]

    except (pyslha.ParseError, pyslha.AccessError):
        print "Pyslha can't read SLHA file from FeynHiggs."
        param['valid'] = False

    return param


# --------------------------------------------------------------------------------------------------
# --- For testing ----------------------------------------------------------------------------------

if __name__ == '__main__':
    PARAM = {'valid' : True,
            'generator' : 'spheno',
             'Qin' : 1000,
             'A_T': -6.14744759E+03,
             'MQ33': 3.96967795E+03,
             'MU33' : 2.10820609E+03,
             'MD33' : 3.21273064E+03,
             'MD33' : 3.21273064E+03,
             'ML33' : 1.07398620E+02,
             'ME33' : 1.01036326E+03,
             'dir_name' : 'slha_dir',
             'slha_input_file' : 'slha_input_for_test',
             'slha_output_file' : 'slha_output_for_test',
            }
    PARAM = main(PARAM)

    if PARAM['valid']:
        print "LSP: {0}".format(PARAM['lsp'])
        print "LSP mass: {0}".format(PARAM['mlsp'])
        print "Higgs mass: {0} GeV".format(PARAM['m_higgs'])
        print "BR: {0} GeV".format(PARAM['br'])
        print "\nNext: Running FeynHiggs.\n..."
        PARAM = feynhiggs_correction(PARAM)
        print "FeynHiggs corrected Higgs mass: {0} GeV".format(PARAM['m_higgs_fh'])
    else:
        print "Invalid point. Probably couldn't calculate decay tables."

