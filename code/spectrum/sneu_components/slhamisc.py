"""
Functions for reading and writing SLHA files
"""

import os
import pyslha

import SM_parameters as SM_parameters

SUSY_KEYS = [1000001, 2000001, 1000002, 2000002,
             1000003, 2000003, 1000004, 2000004,
             1000005, 2000005, 1000006, 2000006,
             1000011, 2000011, 1000012, 1000013,
             2000013, 1000014, 1000015, 2000015,
             1000016, 1000021, 1000023, 1000025,
             1000035, 1000024, 1000037]

# --- Complete dictionary
DEFAULT_DICT = {
    "signmu": 1,
    "modsel": 1,
}

MINPAR_INDICES = {"m0" : 1, "m12" : 2, "tanb" : 3, "signmu" : 4, "A0" : 5}
MINPAR_KEYS = MINPAR_INDICES.keys()

EXTPAR_INDICES = {"M_input" : 0, "Qin" : 0, "m_input" : 0,
                  "M1" : 1, "M2" : 2, "M3" : 3,
                  "At" : 11, "Ab" : 12, "Atau" : 13,
                  "m2H1" : 21, "m2Hd" : 21, "m2H2" : 22, "m2Hu" : 22,
                  "mu" : 23, "mA2" : 24,
                  "tanb_ext" : 25,
                  "mA0" : 26,
                  "mH+" : 27,
                  "mH" : 27,
                  "m_eL" : 31,
                  "m_muL" : 32,
                  "m_tauL" : 33,
                  "m_eR" : 34,
                  "m_muR" : 35,
                  "m_tauR" : 36,
                  "m_q1L" : 41,
                  "m_q2L" : 42,
                  "m_q3L" : 43,
                  "m_uR" : 44,
                  "m_cR" : 45,
                  "m_tR" : 46,
                  "m_dR" : 47,
                  "m_sR" : 48,
                  "m_bR" : 49,
                  "MSL(1)" : 31,
                  "MSL(2)" : 32,
                  "MSL(3)" : 33,
                  "MSE(1)" : 34,
                  "MSE(2)" : 35,
                  "MSE(3)" : 36,
                  "MSQ(1)" : 41,
                  "MSQ(2)" : 42,
                  "MSQ(3)" : 43,
                  "MSU(1)" : 44,
                  "MSU(2)" : 45,
                  "MSU(3)" : 46,
                  "MSD(1)" : 47,
                  "MSD(2)" : 48,
                  "MSD(3)" : 49,
                  "ML11" : 31,
                  "ML22" : 32,
                  "ML33" : 33,
                  "ME11" : 34,
                  "ME22" : 35,
                  "ME33" : 36,
                  "MQ11" : 41,
                  "MQ22" : 42,
                  'MQ33' : 43,
                  "MU11" : 44,
                  "MU22" : 45,
                  "MU33" : 46,
                  "MD11" : 47,
                  "MD22" : 48,
                  "MD33" : 49,
                 }
EXTPAR_KEYS = EXTPAR_INDICES.keys()

SCALAR_KEYS = ("m_eL", "m_muL", "m_tauL",
               "m_eR", "m_muR", "m_tauR",
               "m_q1L", "m_q2L", "m_q3L",
               "m_uR", "m_cR", "m_tR",
               "m_dR", "m_sR", "m_bR")


DESCRIPTION = {
    "m0" : " m0",
    "m12" : " m12",
    "tanb" : " tan beta at m_Z",
    "signmu" : " sign(mu)",
    "A0" : " A0",
    "ML11" : "M_L11",
    "ML22" : "M_L22",
    "ML33" : "M_L33",
    "ME11" : "M_E11",
    "ME22" : "M_E22",
    "ME33" : "M_E33",
    "MQ11" : "M_Q11",
    "MQ22" : "M_Q22",
    'MQ33' : "M_Q33",
    "MU11" : "M_U11",
    "MU22" : "M_U22",
    "MU33" : "M_U33",
    "MD11" : "M_D11",
    "MD22" : "M_D22",
    "MD33" : "M_D33",
    "m_eL" : " m_e L  Left 1 st gen. scalar lepton mass",
    "m_muL" : " m_mu L  Left 2 nd gen. scalar lepton mass",
    "m_tauL" : " m_tau L  Left 3 rd gen. scalar lepton mass",
    "m_eR" : " m_e R  Right scalar electron mass",
    "m_muR" : " m_mu R  Right scalar muon mass",
    "m_tauR" : " m_tau R  Right scalar tau mass",
    "m_q1L" : " m_q 1L. Left 1 st gen. scalar quark mass",
    "m_q2L" : " m_q 2L. Left 2 nd gen. scalar quark mass",
    "m_q3L" : " m_q 3L. Left 3 rd gen. scalar quark mass",
    "m_uR" : " m_u R  Right scalar up mass",
    "m_cR" : " m_c R  Right scalar up mass",
    "m_tR" : " m_t R  Right scalar up mass",
    "m_dR" : " m_d R  Right scalar down mass",
    "m_sR" : " m_s R  Right scalar strange mass",
    "m_bR" : " m_b R  Right scalar bottom mass",
    "Qin" : " M_input",
    "M_input" : " M_input",
    "m_input" : " M_input",
    "M1" : " M1 (Bino Mass)",
    "M2" : " M2 (Wino Mass)",
    "M3" : " M3 (Gluino Mass)",
    "m2H1" : " (m^2_H1)^2 Soft Higgs mass squared",
    "m2H2" : " (m^2_H2))^2 Soft Higgs mass squared",
    "m2Hd" : " (m^2_H1)^2 Soft Higgs mass squared",
    "m2Hu" : " (m^2_H2))^2 Soft Higgs mass squared",
    "mu" : " Soft mu parameter",
    "mA2" : " Tree-level pseudoscalar Higgs parameter squared",
    "tanb_ext" : " tan beta at Q_input",
    "mA0" : " Pseudoscalar Higgs pole mass; instead of mA",
    "A_b" : "A_b",
    "A_t" : "A_t",
    "A_tau" : "A_tau",
    "Ab" : "Ab",
    "At" : "At",
    "Atau" : "Atau",
    }


# =========================================================================================
# === FAST BUT HANDWRITTEN


def slha_input_file_from_dict(values_dict,
                              filename="LesHouches.in",
                              dir_name='.',
                              generator=None):

    """
    Returns minimal SLHA2 format file.
    Arguments:
    Dictionary with entries for SLHA file.
    filename (default LesHouches.in),  dir_name (default .),
    generator (default None) intended to calculate spectrum.
    """

    slha_string = slha_input_string_from_dict(values_dict, generator=generator)

    filename = os.path.join(dir_name, filename)

    with open(filename, 'w') as open_file:
        open_file.write(slha_string)

def slha_input_string_from_dict(values_dict,
                                generator=None):

    """
    Returns minimal SLHA2 format string.
    Arguments:
    Dictionary with entries for SLHA file.
    generator (default None) intended to calculate spectrum.
    """

    # --- MODSEL Block (default=1)
    if values_dict.get("modsel") is None:
        values_dict["modsel"] = 1

    slha_string = ("Block MODSEL         # Select model\n"
                   " 1 {0}\n".format(values_dict["modsel"]))

    slha_string += SM_parameters.get_string()


    # --- MINPAR Block
    slha_string += "Block MINPAR\n"

    for input_key in MINPAR_KEYS:
        if values_dict.get(input_key) is not None:
            slha_string += (" {0} {1} # {2}\n".format(MINPAR_INDICES[input_key],
                                                      values_dict[input_key],
                                                      DESCRIPTION.get(input_key)))

    # --- Check if common scalar masses should be filled
    if "m_scalars" in values_dict.keys():
        values_dict = common_scalar_masses(values_dict)

    # --- EXTPAR Block
    slha_string += "Block EXTPAR\n"

    for input_key in EXTPAR_KEYS:
        if values_dict.get(input_key) is not None:
            slha_string += (" {0} {1} # {2}\n".format(EXTPAR_INDICES[input_key],
                                                      values_dict[input_key],
                                                      DESCRIPTION.get(input_key)))
    # --- Generator specific block
    if generator is not None:
        slha_string += get_generator_string(values_dict, generator)

    return slha_string

def get_generator_string(param, generator, decay_tables=True):
    """
    Write generator specific block, formatted as string for
    writing to SLHA file
    """

    # --- SOFTSUSY
    if generator.lower() in "softsusy" or "softsusy" in generator.lower():
        generator_string = "Block SOFTSUSY     # SOFTSUSY specific inputs\n"
        if decay_tables:
            generator_string += " 0   1       # Calculate decays in output\n"

        generator_string += (" 1   1.000000000e-04  # tolerance\n"
                             " 2   2                # up-quark mixing (=1) or down (=2)\n"
                             " 5   1                # 2-loop running\n"
                             " 3   0                # printout\n"
                             " 15  0                # NMSSMTools compatible output (default: 0)\n"
                             " 18  0                # use soft Higgs masses as EWSB output\n"
                             " 19  1                # Include 3-loop SUSY RGEs\n"
                             " 24  1.0e-06     # If decay BR is below this number, don't output\n"
                             " 25  1           # If 0, don't calculate 3-body decays (1=default)\n")

    # --- SPHENO
    elif generator.lower() in "spheno" or "spheno" in generator.lower():
        generator_string = ("Block SphenoInput     # SPheno specific input"+'\n'
                            " 11 1                 # Calculate branching ratios"+'\n'
                            " 12 1.00000000e-04    # Write only branching ratios larger than\n")

    # --- SUSPECT
    elif generator.lower() in "suspect" or "suspect" in generator.lower():
        generator_string = ("BLOCK SUSPECT_CONFIG\n"
                            " 0 2   # 21: 2-loop RGE, 11: 1-loop\n"
                            " 1 1   # 1: g_1(gut)=g_2(gut), 0: High scale input via index 2\n"
                            " 2 2.50000000e+16  # unification scale (if 1.=1)\n"
                            " 3 2   # RGE accuracy: 1: moderate, 2: accurate\n")
        if param.get("mu") is not None:
            generator_string += " 4 2   # 2: EWSB imposed with mA pole mu\n"
        if param.get("m2H1") is not None or param.get("m2Hd") is not None:
            generator_string += " 4 2   # 1: EWSB imposed with Hu Hd\n"

        generator_string += (
            " 5 2.00000000e+00     # Sparticles masses rad. corr. excluding Higgs): 2 -> all\n"
            " 6 1.00000000e+00     # 1: EWSB scale=(mt_L*mt_R)^(1/2)\n"
            "                      # 0: give EWSB as index 0 in Block EXTPAR\n"
            " 7 2.00000000e+00     # Final spectrum accuracy: 1 -> 1% acc.; 2 -> 0.01 % acc.\n"
            " 8 2.00000000e+00     # Higgs boson masses rad. corr. calculation options:\n"
            "#             A simple (but very good) approximation (advantage=fast)  : 0\n"
            "#             Full one-loop calculation                                : 1\n"
            "#             One-loop  + dominant DSVZ 2-loop (default,recommended)   : 2\n")

    # --- UNKNOWN GENERATOR: returns empty string
    else:
        generator_string = ""

    return generator_string

# =========================================================================================
# =========================================================================================
# === PYSLHA - pretty but slow

def common_scalar_masses(values_dict):
    """
    Check if values_dict contains they key "m_scalars".
    If yes, it is assumed that common scalar masses are desired,
    and the entries for all the supersymmetric scalars in values_dict
    are assigned the value of m_scalars.
    """

    if "m_scalars" in values_dict.keys():
        for _scalar in SCALAR_KEYS:
            values_dict[_scalar] = values_dict["m_scalars"]

    return values_dict

def pyslha_make_slha_input(values_dict,
                           slha_file_name="slha_input",
                           modsel_value=1,
                           generator_input=None):
    """
    Creates slha file from the provided
    values_dict (dictionary) and the optional
    slha file name (default slha_input),
    modsel value (default 1),
    generator input (pyslha block).
    Note that any pre-existing file with the slha_file_name will be
    overwritten.
    This function uses pyslha - so it's safe but quite slow.
    """

    # --- Get SM input parameters
    sminputs = SM_parameters.get_block()

    # --- Ensure input types
    if not all((isinstance(sminputs, pyslha.Block),
                isinstance(values_dict, dict))):
        raise TypeError("SMINPUTS must be type pyslha.Block and the input "
                        "values must be in a dictionary.")

    # --- Pyslha dictionaries are just OrderedDicts (from Collections),
    # --- so you can use that if it makes you more comfortable
    all_blocks = pyslha._dict()

    # -- Add the sminputs to the pyslha dictionary containing all the blocks
    all_blocks["SMINPUTS"] = sminputs

    # --- Make the MODSEL block from the modsel_value, which must be a number
    assert isinstance(modsel_value*1.0, float), "The MODSEL value must be a number."

    modsel = pyslha.Block("MODSEL")
    modsel[1] = modsel_value

    # --- and add the block to the pyslha dictionary
    all_blocks["MODSEL"] = modsel

    # --- Make the MINPAR and EXTPAR blocks and fill them
    minpar = pyslha.Block("MINPAR")
    extpar = pyslha.Block("EXTPAR")

    # --- Check if common scalar masses should be filled
    if "m_scalars" in values_dict.keys():
        values_dict = common_scalar_masses(values_dict)

    # --- Fill the MINPAR block with the correct keys (indices)

    for key in values_dict:
        if key in MINPAR_KEYS:
            minpar[MINPAR_INDICES[key]] = values_dict[key]
        elif key in EXTPAR_KEYS:
            extpar[EXTPAR_INDICES[key]] = values_dict[key]

    # --- and add the block to the pyslha dictionary
    all_blocks["MINPAR"] = minpar
    all_blocks["EXTPAR"] = extpar

    # --- Add the generator input block if provided
    if  generator_input is not None:

        # --- Make sure generator input is a pyslha.Block
        if not isinstance(generator_input, pyslha.Block):
            raise TypeError("If generator_input is provided, it must "
                            "be an pyslha.Block.")

        all_blocks[generator_input] = generator_input


    # --- Pyslha Docs are filled with Blocks, Decays and Xsections.
    # --- For an SLHA input file, only the Blocks part is relevant.

    # --- Make the pyslha doc
    slha_doc = pyslha.Doc(all_blocks)

    # --- and write the SLHAfile
    pyslha.writeSLHAFile(slha_file_name, slha_doc)

# =========================================================================================



# =========================================================================================
# === TESTING

if __name__ == '__main__':
    M_SCALAR = 15000
    PARAM = {'valid' : True,
             'Qin' : 1000,
             "signmu" : 1,
             'mG': 1e2,
             'TR': 1e10,
             'm0' : 500,
             'tanb' : 10,
             'm12' : 500,
             'M1': 1000,#200,
             'M2': 1000, #200,
             'M3': 850, #3000,
             'mu' : M_SCALAR,
             'mA0' : M_SCALAR,
             'm_scalars' : M_SCALAR,
             'use_sdecay' : False,
            }

    #slha_file_from_dict(PARAM, filename='ugly_test_slha')
    slha_input_file_from_dict(PARAM, filename='pretty_test_slha')


    #for i in range(1000000):
        #slha_file_from_dict(PARAM)
        #draft_new_slha_from_dict(PARAM)
