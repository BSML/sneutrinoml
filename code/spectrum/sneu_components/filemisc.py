import os
import SM_parameters

SUSY_KEYS = [1000001, 2000001, 1000002, 2000002,
             1000003, 2000003, 1000004, 2000004,
             1000005, 2000005, 1000006, 2000006,
             1000011, 2000011, 1000012, 1000013,
             2000013, 1000014, 1000015, 2000015,
             1000016, 1000021, 1000023, 1000025,
             1000035, 1000024, 1000037]

# Complete dict:
DEFAULT_DICT = {
    'm0': None,
    'm12': None,
    'tanb': None,
    'tanb_ext': None,
    'signmu': 1,
    'modsel': 1,
    'Qin': None,
    'A0': None,
    'm_scalars': None,
    'M1': None,
    'M2': None,
    'M3': None,
    'm2H1': None,
    'm2H2': None,
    'mu': None,
    'mA2': None,
    'mA0': None
}
def LH_from_dict(param, filename='LesHouches.in', dir_name='.', generator=None):
    """
    Returns minimal SLHA2 format file.
    Arguments:
    param dictionary with entries for SLHA file.
    optional: filename='LesHouches.in',  dir_name='.'
    """
    # Fill missing values in input dicttionary
    for key in DEFAULT_DICT:
        param.setdefault(key, DEFAULT_DICT[key])

    filename = os.path.join(dir_name, filename)
    f = open(filename, "w")

    f.write("Block MODSEL         # Select model\n")
    f.write(" 1 {0}                     # MSSM:0, mSUGRA:1, mGMSB=2, mAMSB=3\n".format(param['modsel']))
    f.write("                     # MSSM bottom up:-1, HighScaleMSSM: 100,\n")
    f.write("                     # HighScaleMSSM with Top Down: 101, External:\n")
    f.write("Block SMINPUTS       # Standard Model inputs\n")
    f.write(" 1 1.279180000e+02   # alpha^(-1) SM MSbar(MZ)\n")
    f.write(" 2 1.166379000e-05   # G_Fermi\n")
    f.write(" 3 1.181000000e-01   # alpha_s(MZ) SM MSbar\n")
    f.write(" 4 9.118760000e+01   # MZ(pole)\n")
    f.write(" 5 4.180000000e+00   # mb(mb) SM MSbar\n")
    f.write(" 6 1.732000000e+02   # mtop(pole)\n")
    f.write(" 7 1.776860000e+00   # mtau(pole)\n")
    f.write("Block MINPAR                   # Model input parameters\n")
    if param['m0'] is not None:
        f.write(" 1  {0}         # m0\n".format(param['m0']))
    if param['m12'] is not None:
        f.write(" 2  {0}         # m12\n".format(param['m12']))
 #   if param['tanb'] is not None and param['tanb_ext'] is None:
 #       f.write(" 3  {0}         # tan beta at m_Z\n".format(param['tanb']))
    if param['signmu'] is not None:
        f.write(" 4  {0}         # sign(mu)\n".format(param['signmu']))
    if param['A0'] is not None:
        f.write(" 5  {0}         # A0\n".format(param['A0']))
    f.write("Block EXTPAR   # Specific model input parameters\n")
    if param['Qin'] is not None:
        f.write("0   {0}      # M_input\n".format(param['Qin']))
    if param['M1'] is not None:
        f.write(" 1  {0}      # M1 (Bino Mass)\n".format(param['M1']))
    if param['M2'] is not None:
        f.write(" 2  {0}      # M2 (Wino Mass)\n".format(param['M2']))
    if param['M3'] is not None:
        f.write(" 3  {0}      # M3 (Gluino Mass)\n".format(param['M3']))
    if param['At'] is not None:
        f.write(" 11 {0}     # A_t\n".format(param['At']))
    if param['Ab'] is not None:
        f.write(" 12 {0}     # A_b\n".format(param['Ab']))
    if param['Atau'] is not None:
        f.write(" 13 {0}     # A_tau\n".format(param['Atau']))
    if param['m2H1'] is not None:
        f.write(" 21 {0}e+06  # (m^2_H1)^2 Soft Higgs mass squared\n".format(param['m2H1']))
    if param['m2H2'] is not None:
        f.write(" 22 {0}e+06  # (m^2_H2))^2 Soft Higgs mass squared\n".format(param['m2H2']))
    if param['m2Hd'] is not None:
        f.write(" 21 {0}e+06     # (m^2_H1)^2 Soft Higgs mass squared\n".format(param['m2Hd']))
    if param['m2Hu'] is not None:
        f.write(" 22 {0}e+06     # (m^2_H2))^2 Soft Higgs mass squared\n".format(param['m2Hu']))
    if param['mu'] is not None:
        f.write(" 23 {0}     # Soft mu parameter\n".format(param['mu']))
    if param['mA2'] is not None:
        f.write(" 24 {0}     # Tree-level pseudoscalar Higgs parameter squared\n".format(param['mA2']))
    if param['tanb'] is not None:
        f.write(" 25  {0}         # tan beta at Q_input\n".format(param['tanb']))
    if param['mA0'] is not None:
        f.write(" 26 {0}     # Pseudoscalar Higgs pole mass. May be given instead of mA\n".format(param['mA0']))
    if param['ML11'] is not None:
        f.write(" 31 {0}     # M_(L,11)\n".format(param['ML11']))
    if param['ML22'] is not None:
        f.write(" 32 {0}     # M_(L,22)\n".format(param['ML22']))
    if param['ML33'] is not None:
        f.write(" 33 {0}     # M_(L,33)\n".format(param['ML33']))
    if param['ME11'] is not None:
        f.write(" 34 {0}     # M_(E,11)\n".format(param['ME11']))
    if param['ME22'] is not None:
        f.write(" 35 {0}     # M_(E,22)\n".format(param['ME22']))
    if param['ME33'] is not None:
        f.write(" 36 {0}     # M_(E,33)\n".format(param['ME33']))
    if param['MQ11'] is not None:
        f.write(" 41 {0}     # M_(Q,11)\n".format(param['MQ11']))
    if param['MQ22'] is not None:
        f.write(" 42 {0}     # M_(Q,22)\n".format(param['MQ22']))
    if param['MQ33'] is not None:
        f.write(" 43 {0}     # M_(Q,33)\n".format(param['MQ33']))
    if param['MU11'] is not None:
        f.write(" 44 {0}     # M_(U,11)\n".format(param['MU11']))
    if param['MU22'] is not None:
        f.write(" 45 {0}     # M_(U,22)\n".format(param['MU22']))
    if param['MU33'] is not None:
        f.write(" 46 {0}     # M_(U,33)\n".format(param['MU33']))
    if param['MD11'] is not None:
        f.write(" 47 {0}     # M_(D,11)\n".format(param['MD11']))
    if param['MD22'] is not None:
        f.write(" 48 {0}     # M_(D,22)\n".format(param['MD22']))
    if param['MD33'] is not None:
        f.write(" 49 {0}     # M_(D,33)\n".format(param['MD33']))

    if param['m_scalars'] is not None:
        #TODO: non-universal scalars
        f.write(" 31 {0}   # m_e L  Left 1 st gen. scalar lepton mass\n".format(param['m_scalars']))
        f.write(" 32 {0}   # m_mu L  Left 2 nd gen. scalar lepton mass\n".format(param['m_scalars']))
        f.write(" 33 {0}   # m_tau L  Left 3 rd gen. scalar lepton mass\n".format(param['m_scalars']))
        f.write(" 34 {0}   # m_e R  Right scalar electron mass.\n".format(param['m_scalars']))
        f.write(" 35 {0}   # m_mu R  Right scalar muon mass.\n".format(param['m_scalars']))
        f.write(" 36 {0}   # m_tau R  Right scalar tau mass.\n".format(param['m_scalars']))
        f.write(" 41 {0}   # m_q 1L. Left 1 st gen. scalar quark mass.\n".format(param['m_scalars']))
        f.write(" 42 {0}   # m_q 2L. Left 2 nd gen. scalar quark mass.\n".format(param['m_scalars']))
        f.write(" 43 {0}   # m_q 3L. Left 3 rd gen. scalar quark mass.\n".format(param['m_scalars']))
        f.write(" 44 {0}   # m_u R  Right scalar up mass.\n".format(param['m_scalars']))
        f.write(" 45 {0}   # m_c R  Right scalar charm mass.\n".format(param['m_scalars']))
        f.write(" 46 {0}   # m_t R  Right scalar top mass.\n".format(param['m_scalars']))
        f.write(" 47 {0}   # m_d R  Right scalar down mass.\n".format(param['m_scalars']))
        f.write(" 48 {0}   # m_s R  Right scalar strange mass.\n".format(param['m_scalars']))
        f.write(" 49 {0}   # m_b R  Right scalar bottom mass.\n".format(param['m_scalars']))
    if generator is not None and generator.lower() in 'softsusy':
        f.write("Block SOFTSUSY               # SOFTSUSY specific inputs\n")
        if True: #TODO: build in decay tables switch
            f.write(" 0   1.000000000e+00      # Calculate decays in output (only for RPC (N)MSSM)\n")
        f.write(" 1   1.000000000e-04      # tolerance\n")
        f.write(" 2   2.000000000e+00      # up-quark mixing (=1) or down (=2)\n")
        f.write(" 5   1.000000000E+00      # 2-loop running\n")
        f.write(" 3   0.000000000E+00      # printout\n")
        f.write(" 15   0.000000000E+00      # NMSSMTools compatible output (default: 0)\n")
        f.write(" 18   0.000000000E+00      # use soft Higgs masses as EWSB output\n")
        f.write(" 19   1.000000000e+00      # Include 3-loop SUSY RGEs\n")
        f.write(" 24   1.000000000e-06      # If decay BR is below this number, don't output\n")
        f.write(" 25   1.000000000e+00      # If set to 0, don't calculate 3-body decays (1=default)\n")
    if  generator is not None and generator.lower() in 'spheno':
        f.write("Block SphenoInput      # SPheno specific input"+'\n')
#            f.write(" 2 1                  # SPA conventions"+'\n')
        f.write(" 11 1                                  # Calculate branching ratios"+'\n')
        f.write(" 12 1.00000000e-04     # Write only branching ratios larger than this value"+'\n')
    f.close()


def SPhenoLH_all_params(m0=None, m12=None, tanb=None, signmu=None, A0=None, modsel=1,
                        Qin=None, m_scalars=None,
                        M1=None, M2=None, M3=None,
                        m2H1=None, m2H2=None,
                        mu=None, mA2=None, mA0=None, tanb_ext=None,
                        base_filename='LesHouches.in', dir_name='.'):
    """
    m0, m12, tanb, signmu, A0, m_scalars=None, M1=None, M2=None, M3=None, m2H1=None, m2H2=None,
    mu=None, mA=None, mA0=None, tanb_ext=None, base_filename='LesHouches.in', dir_name='.'
    """

    filename = os.path.join(dir_name, base_filename)
    f = open(filename,    "w")
    f.write("Block MODSEL         # Select model"'\n')
    f.write(" 1 {0}                     # MSSM:0, mSUGRA:1, mGMSB=2, mAMSB=3\n".format(modsel))
    f.write("Block SMINPUTS           # Standard Model inputs"+'\n')
    f.write(" 1 1.279180000e+02         # alpha^(-1) SM MSbar(MZ)"+'\n')
    f.write(" 2 1.166379000e-05   # G_Fermi"+'\n')
    f.write(" 3 1.181000000e-01   # alpha_s(MZ) SM MSbar"+'\n')
    f.write(" 4 9.118760000e+01   # MZ(pole)"+'\n')
    f.write(" 5 4.180000000e+00   # mb(mb) SM MSbar"+'\n')
    f.write(" 6 1.732000000e+02   # mtop(pole)"+'\n')
    f.write(" 7 1.776860000e+00   # mtau(pole)"+'\n')
    f.write("Block MINPAR                   # Model input parameters\n")
    if m0 is not None:# or (m0 is None and m_scalars is None):
        f.write(" 1  {0}         # m0\n".format(m0))
    if m12 is not None:# or (m12 is None and M1 is None):
        f.write(" 2  {0}         # m12\n".format(m12))
    if tanb is not None and tanb_ext is None:
        f.write(" 3  {0}         # tan beta at m_Z\n".format(tanb))
        f.write(" 3  {0}         # tan beta\n".format(tanb))
    if signmu is not None:
        f.write(" 4  {0}         # sign(mu)\n".format(signmu))
    if A0 is not None:
        f.write(" 5  {0}         # A0\n".format(A0))
    f.write("Block EXTPAR   # Specific model input parameters\n")
    if Qin is not None:
        f.write("0   {0}      # M_input\n".format(Qin))
    if M1 is not None:
        f.write(" 1  {0}      # M1 (Bino Mass)\n".format(M1))
    if M2 is not None:
        f.write(" 2  {0}      # M2 (Wino Mass)\n".format(M2))
    if M3 is not None:
        f.write(" 3  {0}      # M3 (Gluino Mass)\n".format(M3))
    if m2H1 is not None:
        f.write(" 21 {0}     # (m^2_H1)^2 Soft Higgs mass squared\n".format(m2H1))
    if m2H2 is not None:
        f.write(" 22 {0}     # (m^2_H2))^2 Soft Higgs mass squared\n".format(m2H2))
    if mu is not None:
        f.write(" 23 {0}     # Soft mu parameter\n".format(mu))
    if mA2 is not None:
        f.write(" 24 {0}     # Tree-level pseudoscalar Higgs parameter squared\n".format(mA2))
    if tanb_ext is not None:
        f.write(" 25  {0}         # tan beta at Q_input\n".format(tanb_ext))
    if mA0 is not None:
        f.write(" 26 {0}     # Pseudoscalar Higgs pole mass. May be given instead of mA\n".format(mA0))
    if m_scalars is not None:
        f.write(" 31 {0}   # m_e L  Left 1 st gen. scalar lepton mass\n".format(m_scalars[0]))
        f.write(" 32 {0}   # m_mu L  Left 2 nd gen. scalar lepton mass\n".format(m_scalars[1]))
        f.write(" 33 {0}   # m_tau L  Left 3 rd gen. scalar lepton mass\n".format(m_scalars[2]))
        f.write(" 34 {0}   # m_e R  Right scalar electron mass.\n".format(m_scalars[3]))
        f.write(" 35 {0}   # m_mu R  Right scalar muon mass.\n".format(m_scalars[4]))
        f.write(" 36 {0}   # m_tau R  Right scalar tau mass.\n".format(m_scalars[5]))
        f.write(" 41 {0}   # m_q 1L. Left 1 st gen. scalar quark mass.\n".format(m_scalars[6]))
        f.write(" 42 {0}   # m_q 2L. Left 2 nd gen. scalar quark mass.\n".format(m_scalars[7]))
        f.write(" 43 {0}   # m_q 3L. Left 3 rd gen. scalar quark mass.\n".format(m_scalars[8]))
        f.write(" 44 {0}   # m_u R  Right scalar up mass.\n".format(m_scalars[9]))
        f.write(" 45 {0}   # m_c R  Right scalar charm mass.\n".format(m_scalars[10]))
        f.write(" 46 {0}   # m_t R  Right scalar top mass.\n".format(m_scalars[11]))
        f.write(" 47 {0}   # m_d R  Right scalar down mass.\n".format(m_scalars[12]))
        f.write(" 48 {0}   # m_s R  Right scalar strange mass.\n".format(m_scalars[13]))
        f.write(" 49 {0}   # m_b R  Right scalar bottom mass.\n".format(m_scalars[14]))
    f.write("Block SphenoInput      # SPheno specific input"+'\n')
#        f.write(" 2 1                  # SPA conventions"+'\n')
    f.write(" 11 1                                  # Calculate branching ratios"+'\n')
    f.write(" 12 1.00000000e-04     # Write only branching ratios larger than this value"+'\n')
    f.close()

def SPhenoLH_simple(m0, m12, tanb, signmu, A0, modsel=1,
                    base_filename='LesHouches.in', dir_name='.'):
    """
    Make SPheno SLHA input file with only MINPAR parameters
    """

    filename = os.path.join(dir_name, base_filename)
    f = open(filename,    "w")
    f.write("Block MODSEL         # Select model"'\n')
    f.write(" 1 {0}                     # MSSM:0, mSUGRA:1, mGMSB=2, mAMSB=3\n".format(modsel))
    f.write("Block SMINPUTS           # Standard Model inputs"+'\n')
    f.write(" 1 1.279180000e+02         # alpha^(-1) SM MSbar(MZ)"+'\n')
    f.write(" 2 1.166379000e-05   # G_Fermi"+'\n')
    f.write(" 3 1.181000000e-01   # alpha_s(MZ) SM MSbar"+'\n')
    f.write(" 4 9.118760000e+01   # MZ(pole)"+'\n')
    f.write(" 5 4.180000000e+00   # mb(mb) SM MSbar"+'\n')
    f.write(" 6 1.732000000e+02   # mtop(pole)"+'\n')
    f.write(" 7 1.776860000e+00   # mtau(pole)"+'\n')
    f.write("Block MINPAR                   # Model input parameters\n")
    f.write(" 1  {0}         # m0\n".format(m0))
    f.write(" 2  {0}         # m12\n".format(m12))
    f.write(" 3  {0}         # tan beta\n".format(tanb))
    f.write(" 4  {0}         # sign(mu)\n".format(signmu))
    f.write(" 5  {0}         # A0\n".format(A0))
    f.close()

def SPhenoLH(m0=None, m12=None, tanb=None, signmu=None, A0=None, modsel=1,
             Qin=None, m_scalars=None, M1=None, M2=None, M3=None, m2H1=None, m2H2=None,
             mu=None, mA2=None, mA0=None, tanb_ext=None,
             base_filename='LesHouches.in', dir_name='.'):
    """
    m0, m12, tanb, signmu, A0, m_scalars=None, M1=None, M2=None, M3=None, m2H1=None, m2H2=None,
    mu=None, mA=None, mA0=None, tanb_ext=None, base_filename='LesHouches.in', dir_name='.'
    """

    filename = os.path.join(dir_name, base_filename)
    f = open(filename,    "w")
    f.write("Block MODSEL         # Select model"'\n')
    f.write(" 1 {0}                     # MSSM:0, mSUGRA:1, mGMSB=2, mAMSB=3\n".format(modsel))
    f.write("Block SMINPUTS           # Standard Model inputs"+'\n')
    f.write(" 1 1.279180000e+02         # alpha^(-1) SM MSbar(MZ)"+'\n')
    f.write(" 2 1.166379000e-05   # G_Fermi"+'\n')
    f.write(" 3 1.181000000e-01   # alpha_s(MZ) SM MSbar"+'\n')
    f.write(" 4 9.118760000e+01   # MZ(pole)"+'\n')
    f.write(" 5 4.180000000e+00   # mb(mb) SM MSbar"+'\n')
    f.write(" 6 1.732000000e+02   # mtop(pole)"+'\n')
    f.write(" 7 1.776860000e+00   # mtau(pole)"+'\n')
    f.write("Block MINPAR                   # Model input parameters\n")
    if m0 is not None:# or (m0 is None and m_scalars is None):
        f.write(" 1  {0}         # m0\n".format(m0))
    if m12 is not None:# or (m12 is None and M1 is None):
        f.write(" 2  {0}         # m12\n".format(m12))
    if tanb is not None and tanb_ext is None:
        f.write(" 3  {0}         # tan beta\n".format(tanb))
    if signmu is not None:
        f.write(" 4  {0}         # sign(mu)\n".format(signmu))
    if A0 is not None:
        f.write(" 5  {0}         # A0\n".format(A0))
    f.write("Block EXTPAR   # Specific model input parameters\n")
    if Qin is not None:
        f.write("0   {0}      # M_input\n".format(Qin))
    if M1 is not None:
        f.write(" 1  {0}      # M1 (Bino Mass)\n".format(M1))
    if M2 is not None:
        f.write(" 2  {0}      # M2 (Wino Mass)\n".format(M2))
    if M3 is not None:
        f.write(" 3  {0}      # M3 (Gluino Mass)\n".format(M3))
    if m2H1 is not None:
        f.write(" 21 {0}     # (m^2_H1)^2 Soft Higgs mass squared\n".format(m2H1))
    if m2H2 is not None:
        f.write(" 22 {0}     # (m^2_H2))^2 Soft Higgs mass squared\n".format(m2H2))
    if mu is not None:
        f.write(" 23 {0}     # Soft mu parameter\n".format(mu))
    if mA2 is not None:
        f.write(" 24 {0}     # Tree-level pseudoscalar Higgs parameter squared\n".format(mA2))
    if tanb_ext is not None:
        f.write(" 25  {0}         # tan beta at Q_input\n".format(tanb_ext))
    if mA0 is not None:
        f.write(" 26 {0}     # Pseudoscalar Higgs pole mass. May be given instead of mA\n".format(mA0))
    if m_scalars is not None:
        f.write(" 31 {0}   # m_e L  Left 1 st gen. scalar lepton mass\n".format(m_scalars))
        f.write(" 32 {0}   # m_mu L  Left 2 nd gen. scalar lepton mass\n".format(m_scalars))
        f.write(" 33 {0}   # m_tau L  Left 3 rd gen. scalar lepton mass\n".format(m_scalars))
        f.write(" 34 {0}   # m_e R  Right scalar electron mass.\n".format(m_scalars))
        f.write(" 35 {0}   # m_mu R  Right scalar muon mass.\n".format(m_scalars))
        f.write(" 36 {0}   # m_tau R  Right scalar tau mass.\n".format(m_scalars))
        f.write(" 41 {0}   # m_q 1L. Left 1 st gen. scalar quark mass.\n".format(m_scalars))
        f.write(" 42 {0}   # m_q 2L. Left 2 nd gen. scalar quark mass.\n".format(m_scalars))
        f.write(" 43 {0}   # m_q 3L. Left 3 rd gen. scalar quark mass.\n".format(m_scalars))
        f.write(" 44 {0}   # m_u R  Right scalar up mass.\n".format(m_scalars))
        f.write(" 45 {0}   # m_c R  Right scalar charm mass.\n".format(m_scalars))
        f.write(" 46 {0}   # m_t R  Right scalar top mass.\n".format(m_scalars))
        f.write(" 47 {0}   # m_d R  Right scalar down mass.\n".format(m_scalars))
        f.write(" 48 {0}   # m_s R  Right scalar strange mass.\n".format(m_scalars))
        f.write(" 49 {0}   # m_b R  Right scalar bottom mass.\n".format(m_scalars))
    f.write("Block SphenoInput      # SPheno specific input"+'\n')
#        f.write(" 2 1                  # SPA conventions"+'\n')
    f.write(" 11 1                                  # Calculate branching ratios"+'\n')
    f.write(" 12 1.00000000e-04     # Write only branching ratios larger than this value"+'\n')
    f.close()


def slha_string_from_dict(param, generator=None):
    """
    Returns minimal SLHA2 format string.
    Arguments:
    Dictionary with entries for SLHA file.
    generator, for writing generator block (default None).
    This function is explicit and ugly, but much faster than a pyslha version.
    """

    # --- Fill missing values in input dictionary
    for key in DEFAULT_DICT:
        param.setdefault(key, DEFAULT_DICT[key])

    # --- Block MODSEL
    slha_string = ("Block MODSEL         # Select model\n"
                   " 1 {0}     # MSSM:0, mSUGRA:1, mGMSB=2,mAMSB=3\n"
                   .format(param.get("modsel")))

    # --- Block SMINPUTS
    slha_string += SM_parameters.get_string()

    # --- Block MINPAR
    slha_string += get_minpar_string(param)

    # --- Block EXTPAR
    slha_string += get_extpar_string(param)

    # --- Generator specific block
    if generator is not None:
        slha_string += get_generator_string(param, generator)

    return slha_string

def slha_file_from_dict(param, filename="LesHouches.in", dir_name='.', generator=None):
    """
    Returns minimal SLHA2 format file.
    Arguments:
    Dictionary with entries for SLHA file.
    filename (default LesHouches.in)
    dir_name (default .)
    generator, for writing generator block (default None).
    This function is explicit and ugly, but much faster than a pyslha version.
    """

    filename = os.path.join(dir_name, filename)

    slha_string = slha_string_from_dict(param, generator=generator)

    with open(filename, "w") as open_file:
        open_file.write(slha_string)

def get_generator_string(param, generator, decay_tables=True):
    """
    Write generator specific block, formatted as string for
    writing to SLHA file
    """

    # --- SOFTSUSY
    if generator.lower() in "softsusy" or "softsusy" in generator.lower():
        generator_string = "Block SOFTSUSY     # SOFTSUSY specific inputs\n"
        if decay_tables:
            generator_string += " 0   1       # Calculate decays in output\n"

        generator_string += (" 1   1.000000000e-04  # tolerance\n"
                             " 2   2                # up-quark mixing (=1) or down (=2)\n"
                             " 5   1                # 2-loop running\n"
                             " 3   0                # printout\n"
                             " 15  0                # NMSSMTools compatible output (default: 0)\n"
                             " 18  0                # use soft Higgs masses as EWSB output\n"
                             " 19  1                # Include 3-loop SUSY RGEs\n"
                             " 24  1.0e-06     # If decay BR is below this number, don't output\n"
                             " 25  1           # If 0, don't calculate 3-body decays (1=default)\n")

    # --- SPHENO
    elif generator.lower() in "spheno" or "spheno" in generator.lower():
        generator_string = ("Block SphenoInput     # SPheno specific input"+'\n'
                            " 11 1                 # Calculate branching ratios"+'\n'
                            " 12 1.00000000e-04    # Write only branching ratios larger than\n")

    # --- SUSPECT
    elif generator.lower() in "suspect" or "suspect" in generator.lower():
        generator_string = ("BLOCK SUSPECT_CONFIG\n"
                            " 0 2   # 21: 2-loop RGE, 11: 1-loop\n"
                            " 1 1   # 1: g_1(gut)=g_2(gut), 0: High scale input via index 2\n"
                            " 2 2.50000000e+16  # unification scale (if 1.=1)\n"
                            " 3 2   # RGE accuracy: 1: moderate, 2: accurate\n")
        if param.get("mu") is not None:
            generator_string += " 4 2   # 2: EWSB imposed with mA pole mu\n"
        if param.get("m2H1") is not None or param.get("m2Hd") is not None:
            generator_string += " 4 2   # 1: EWSB imposed with Hu Hd\n"

        generator_string += (
            " 5 2.00000000e+00     # Sparticles masses rad. corr. excluding Higgs): 2 -> all\n"
            " 6 1.00000000e+00     # 1: EWSB scale=(mt_L*mt_R)^(1/2)\n"
            "                      # 0: give EWSB as index 0 in Block EXTPAR\n"
            " 7 2.00000000e+00     # Final spectrum accuracy: 1 -> 1% acc.; 2 -> 0.01 % acc.\n"
            " 8 2.00000000e+00     # Higgs boson masses rad. corr. calculation options:\n"
            "#             A simple (but very good) approximation (advantage=fast)  : 0\n"
            "#             Full one-loop calculation                                : 1\n"
            "#             One-loop  + dominant DSVZ 2-loop (default,recommended)   : 2\n")

    # --- UNKNOWN GENERATOR: returns empty string
    else:
        generator_string = ""

    return generator_string

def get_minpar_string(param):
    """
    Write minimal parameters block, formatted as string for
    writing to SLHA file
    """
    minpar = "Block MINPAR         # Model input parameters\n"
    if param.get('m0') is not None:
        minpar += (" 1 {0}         # m0\n".format(param.get('m0')))
    if param.get('m12') is not None:
        minpar += (" 2 {0}         # m12\n".format(param.get('m12')))
    if param.get('tanb') is not None and param.get('tanb_ext') is None:
        minpar += (" 3 {0}          # tan beta at m_Z\n".format(param.get('tanb')))
    if param.get('signmu') is not None: #OBS
        minpar += (" 4 {0}           # sign(mu)\n".format(param.get('signmu')))
    if param.get('A0') is not None:
        minpar += (" 5 {0}        # A0\n".format(param.get('A0')))

    return minpar

def get_extpar_string(param):
    """
    Write external parameters block, formatted as string for
    writing to SLHA file
    """

    extpar = "Block EXTPAR   # Specific model input parameters\n"
    if param.get("Qin") is not None:
        extpar += ("0   {0}      # M_input\n".format(param.get('Qin')))
    if param.get("M1") is not None:
        extpar += (" 1  {0}      # M1 (Bino Mass)\n".format(param.get('M1')))
    if param.get("M2") is not None:
        extpar += (" 2  {0}      # M2 (Wino Mass)\n".format(param.get('M2')))
    if param.get("M3") is not None:
        extpar += (" 3  {0}      # M3 (Gluino Mass)\n".format(param.get('M3')))
    if param.get("m2H1") is not None:
        extpar += (" 21 {0}e+06  # (m^2_H1)^2 Soft Higgs mass squared\n".format(param.get('m2H1')))
    if param.get("m2H2") is not None:
        extpar += (" 22 {0}e+06  # (m^2_H2))^2 Soft Higgs mass squared\n".format(param.get('m2H2')))
    if param.get("m2Hd") is not None:
        extpar += (" 21 {0}e+06     # (m^2_H1)^2 Soft Higgs mass squared\n".format(param.get('m2Hd')))
    if param.get("m2Hu") is not None:
        extpar += (" 22 {0}e+06     # (m^2_H2))^2 Soft Higgs mass squared\n".format(param.get('m2Hu')))
    if param.get("mu") is not None:
        extpar += (" 23 {0}     # Soft mu parameter\n".format(param.get('mu')))
    if param.get("mA2") is not None:
        extpar += (" 24 {0}     # Tree-level pseudoscalar Higgs parameter squared\n"
                   .format(param.get('mA2')))
    if param.get("tanb_ext") is not None:
        extpar += (" 25  {0}         # tan beta at Q_input\n".format(param.get('tanb_ext')))
    if param.get("mA0") is not None:
        extpar += (" 26 {0}     # Pseudoscalar Higgs pole mass; instead of mA\n"
                   .format(param.get('mA0')))

    extpar += get_scalar_part_of_extpar(param)

    return extpar

def get_scalar_part_of_extpar(param):
    """
    Get the part in extpar concerning the scalars
    """

    scalar_part = ""

    if param.get("m_scalars") is not None:
        scalar_part += (
            " 31 {0}   # m_e L  Left 1 st gen. scalar lepton mass\n"
            " 32 {0}   # m_mu L  Left 2 nd gen. scalar lepton mass\n"
            " 33 {0}   # m_tau L  Left 3 rd gen. scalar lepton mass\n"
            " 34 {0}   # m_e R  Right scalar electron mass.\n"
            " 35 {0}   # m_mu R  Right scalar muon mass.\n"
            " 36 {0}   # m_tau R  Right scalar tau mass.\n"
            " 41 {0}   # m_q 1L. Left 1 st gen. scalar quark mass.\n"
            " 42 {0}   # m_q 2L. Left 2 nd gen. scalar quark mass.\n"
            " 43 {0}   # m_q 3L. Left 3 rd gen. scalar quark mass.\n"
            " 44 {0}   # m_u R  Right scalar up mass.\n"
            " 45 {0}   # m_c R  Right scalar charm mass.\n"
            " 46 {0}   # m_t R  Right scalar top mass.\n"
            " 47 {0}   # m_d R  Right scalar down mass.\n"
            " 48 {0}   # m_s R  Right scalar strange mass.\n"
            " 49 {0}   # m_b R  Right scalar bottom mass.\n".format(param.get('m_scalars')))

    if param.get("m_eL") is not None:
        scalar_part += (" 31 {0}   # m_e L  Left 1 st gen. scalar lepton mass\n"
                        .format(param.get("m_eL")))
    if param.get("m_muL") is not None:
        scalar_part += (" 32 {0}   # m_mu L  Left 2 nd gen. scalar lepton mass\n"
                        .format(param.get("m_muL")))
    if param.get("m_tauL") is not None:
        scalar_part += (" 33 {0}   # m_tau L  Left 3 rd gen. scalar lepton mass\n"
                        .format(param.get("m_tauL")))
    if param.get("m_eR") is not None:
        scalar_part += (" 34 {0}   # m_e R  Right scalar electron mass.\n"
                        .format(param.get("m_eR")))
    if param.get("m_muR") is not None:
        scalar_part += (" 35 {0}   # m_mu R  Right scalar muon mass.\n"
                        .format(param.get("m_muR")))
    if param.get("m_tauR") is not None:
        scalar_part += (" 36 {0}   # m_tau R  Right scalar tau mass.\n"
                        .format(param.get("m_tauR")))
    if param.get("m_q1L") is not None:
        scalar_part += (" 41 {0}   # m_q 1L. Left 1 st gen. scalar quark mass.\n"
                        .format(param.get("m_q1L")))
    if param.get("m_q2L") is not None:
        scalar_part += (" 42 {0}   # m_q 2L. Left 2 nd gen. scalar quark mass.\n"
                        .format(param.get("m_q2L")))
    if param.get("m_q3L") is not None:
        scalar_part += (" 43 {0}   # m_q 3L. Left 3 rd gen. scalar quark mass.\n"
                        .format(param.get("m_q3L")))
    if param.get("m_uR") is not None:
        scalar_part += (" 44 {0}   # m_u R  Right scalar up mass.\n"
                        .format(param.get("m_uR")))
    if param.get("m_cR") is not None:
        scalar_part += (" 45 {0}   # m_c R  Right scalar up mass.\n"
                        .format(param.get("m_cR")))
    if param.get("m_tR") is not None:
        scalar_part += (" 46 {0}   # m_t R  Right scalar up mass.\n"
                        .format(param.get("m_tR")))
    if param.get("m_dR") is not None:
        scalar_part += (" 47 {0}   # m_d R  Right scalar down mass.\n"
                        .format(param.get("m_dR")))
    if param.get("m_sR") is not None:
        scalar_part += (" 48 {0}   # m_s R  Right scalar strange mass.\n"
                        .format(param.get("m_sR")))
    if param.get("m_bR") is not None:
        scalar_part += (" 49 {0}   # m_b R  Right scalar bottom mass.\n"
                        .format(param.get("m_bR")))

    return scalar_part


# =========================================================================================
if __name__ == '__main__':
    SPhenoLH(m12=500, tanb=10, A0=-500, Qin=1000, m_scalars=10, M1=1000, M2=1000, M3=2000)
