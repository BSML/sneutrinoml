"""This file have functions calculating the xs for EW and QCD signals
for the tau tau mu signature (This can be changed by changing the analysis
used in the Herwig template file)"""

import os
import subprocess

SIMULATIONS_DIR = 'Simulations/'
ROOT_DIR = 'code/spectrum'
SNEU_COMPONENTS = os.path.join(ROOT_DIR,'sneu_components')
DEVNULL = open(os.devnull,'wb')


def create_herwig_input_file(process, slha_file):
    """DOCSTRING::::
       input: process can be EW or QCD"""
    # Change the Herwig input file
    if process == 'EW':
        template_file = os.path.join(SNEU_COMPONENTS,'Herwig_ew_template.in')

        herwig_run_directory = os.path.join(SIMULATIONS_DIR,'HerwigTest') 
    else:
        template_file = os.path.join(SNEU_COMPONENTS,'Herwig_qcd_template.in')
        herwig_run_directory =  os.path.join(SIMULATIONS_DIR,'HerwigTest2') 

    with open(template_file) as f:
        newText=f.read( ).replace('setup MSSM/Model', 'setup MSSM/Model ' +  slha_file)

    with open(os.path.join(herwig_run_directory,'LHC-MSSM.in'), "w") as f:
        f.write(newText)


def run_herwig(process):
    # Run Herwig
    if process == 'EW':
        p = subprocess.Popen([os.path.join(SNEU_COMPONENTS,'HerwigEW_run_script.sh')],
                             stdout=DEVNULL,
                             stderr=DEVNULL)
    else:
        p = subprocess.Popen([os.path.join(SNEU_COMPONENTS,'./HerwigQCD_run_script.sh')],
                             stdout=DEVNULL,
                             stderr=DEVNULL)
    p.wait()
   
def get_xs_from_herwig(output=None, process=None):
    
    
    if process == 'EW':
      output = os.path.join(SIMULATIONS_DIR,'HerwigTest/crossSections_mrK.txt') 
    elif process == 'QCD':
      output =  os.path.join(SIMULATIONS_DIR,'HerwigTest2/crossSections_mrK.txt')
    

    with open(output) as f:
        for line in f:
            if line == "": continue

            # Get the xs from the line with detector/basic cuts
            l = line.split(' ')
            if l[0] != 'CROSS': continue
            if l[3] == 'Sherpa':
                xs =eval(l[-1])
                return xs


def main(param):
    """calculates the cross section for the ew and qcd signal
       return the param dictionary with the keys xs_ew and xs_qcd
    """
    from xs_calculations import create_herwig_input_file, run_herwig, get_xs_from_herwig
    try:
    #if True:
        create_herwig_input_file('EW', param['slha_output_file'])
        run_herwig('EW')
        param['xs_ew'] = get_xs_from_herwig(process ='EW')

        create_herwig_input_file('QCD', param['slha_output_file'])
        run_herwig('QCD')
        param['xs_qcd'] = get_xs_from_herwig(process = 'QCD')

    except Exception as ex:
        print "Cross section module threw exception:"
        print ex

    return param


if __name__ == '__main__':
    
    slha_output = os.path.join(SIMULATIONS_DIR,'/HerwigTest/out_0_1200_10_-3000_20.0_0') 

    # For testing
    create_herwig_input_file('EW', slha_output)
    create_herwig_input_file('QCD', slha_output)

    run_herwig('EW')
    run_herwig('QCD')
    
    # Get the XS from the output file
    ew_output_xs_file = os.path.join(SIMULATIONS_DIR,'HerwigTest/crossSections_mrK.txt') 
    qcd_output_xs_file =  os.path.join(SIMULATIONS_DIR,'HerwigTest2/crossSections_mrK.txt')
    
    xs_ew = get_xs_from_herwig(ew_output_xs_file)
    xs_qcd = get_xs_from_herwig(qcd_output_xs_file)
    
    print(xs_ew)
    print(xs_qcd)

    

