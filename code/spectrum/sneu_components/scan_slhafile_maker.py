"""
Make very spedific SLHA file from param. Not for general use.
"""

import os

def main(param):
    """
    Make very spedific SLHA file from param. Not for general use.
    """
    filename = param['slha_input_file']
    open_file = open(filename, 'w')
    open_file.write("Block MODSEL                 # Select model\n")
    open_file.write(" 1    0                      # mSugra\n")
    open_file.write("Block SMINPUTS               # Standard Model inputs\n")
    open_file.write(" 2   1.166379E-05       # G_F, Fermi constant\n")
    open_file.write(" 3   1.184000E-01       # alpha_s(MZ) SM MSbar\n")
    open_file.write(" 4   9.118760E+01       # Z-boson pole mass\n")
    open_file.write(" 5   4.180000E+00       # m_b(mb) SM MSbar\n")
    open_file.write(" 6   1.731000E+02       # m_top(pole)\n")
    open_file.write(" 7   1.776820E+00       # m_tau(pole)\n")
    open_file.write("Block MINPAR                 # Input parameters\n")
    open_file.write(" 4   1                  # sign(mu)\n")
    open_file.write("Block EXTPAR                 # Input parameters\n")
    open_file.write("0    1.46680439E+03 \n")
    open_file.write("25   1.000000E+01       # tanb\n")
    open_file.write(" 1    5.21671045E+02  # M_1\n")
    open_file.write(" 2    9.51264575E+02  # M_2\n")
    open_file.write(" 3    2.55452888E+03  # M_3\n")
    open_file.write("\n")
    open_file.write("11    {0} # A_t\n".format(param['A_T']))
    open_file.write("\n")
    open_file.write("12    -5.54420118E+03 # A_b\n")
    open_file.write("13    -3.63669853E+03 # A_tau\n")
    open_file.write("21    1.96422130E+07  # M^2_(H,d)\n")
    open_file.write("22   -3.65701482E+06  # M^2_(H,u)\n")
    open_file.write("31    3.14692692E+02  # M_(L,11)\n")
    open_file.write("32    3.14187063E+02  # M_(L,22)\n")

    open_file.write("33    {0}  # M_(L,33)\n".format(param['ML33']))
    #open_file.write("33    1.07398620E+02  # M_(L,33)\n")

    open_file.write("34    1.09408608E+03  # M_(E,11)\n")
    open_file.write("35    1.09379345E+03  # M_(E,22)\n")

    open_file.write("36    {0}  # M_(E,33)\n".format(param['ME33']))
    #open_file.write("36    1.01036326E+03  # M_(E,33)\n")

    open_file.write("41    2.33149338E+03  # M_(Q,11)\n")
    open_file.write("42    2.33147596E+03  # M_(Q,22)\n")
    open_file.write("\n")
    open_file.write("43    {0}  # M_(Q,33)\n".format(param['MQ33']))
    open_file.write("\n")
    open_file.write("44    2.04402141E+03  # M_(U,11)\n")
    open_file.write("45    2.04400251E+03  # M_(U,22)\n")
    open_file.write("\n")
    open_file.write("46    {0} # M_(U,33)\n".format(param['MU33']))
    open_file.write("\n")
    open_file.write("47    2.26589270E+03  # M_(D,11)\n")
    open_file.write("48    2.26587352E+03  # M_(D,22)\n")
    open_file.write("\n")
    open_file.write("49    {0} # M_(D,33)\n".format(param['MU33']))
