"""
Loglikelihood function for low statistics searches
"""

# Loglikelihood: The larger, the better.
# -2 loglikelihood follows chi-squared distribution with corresponding d.o.f.

import os
import math
import numpy as np
import scipy as sp
#from scipy.interpolate import griddata
from scipy.special import gammaincinv
from scipy.stats import poisson

LL_MIN = -1e99

def loglike_normal(value, mean, sigma):
    """
    Returns the natural logarithm of the normal likelihood,
    based on input value, mean and sigma,
    including the prefactor Sqrt(1/(2pi*sigma^2)).
    The pymc.normal_like is not used, see emails.
    """
    if value is None:
        return LL_MIN
    else:
        return -0.5*sp.log(2*math.pi*sigma**2) - (value-mean)**2/(2*sigma**2)

def loglike_lin(value, coeff=1.0, add=0.0, scale=1.0):
    """
    Lnear loglike function, returns the logarithm of the input value.
    (i.e. it favours larger values). Can be scaled by the optional argument
    coeff (default = 1.0), and be decreased/increased via the optional argument
    add (defalt = 0.0).
    """

    if value == 0:
        return 0 + add
    return coeff*sp.log(scale*value) + add

def like_lin(value, coeff=1.0, add=0.0):
    """
    Linear likelifunction, favouring large values
    """
    return coeff*value + add

if __name__ == '__main__':
    import matplotlib.pyplot as plt

    # --- What typically goes into loglike_normal (Higgs)
    HIGGS_MEAN = 125
    HIGGS_SIGMA = 0.5
    X_RANGE_HIGGS_MASS = np.arange(110, 130, 0.1)
    LL_HIGGS = [loglike_normal(x, HIGGS_MEAN, HIGGS_SIGMA) for x in
                X_RANGE_HIGGS_MASS]

    print "Max Higgs loglike: ", max(LL_HIGGS)

    plt.scatter(X_RANGE_HIGGS_MASS, LL_HIGGS,
                label=r"Gaussian $m_h$ loglike",
                #;$\mu=125$GeV and $\sigma=2$GeV",
                s=2)

    # --- What typically goes into loglike_lin (BR)
    X_RANGE_BR = np.logspace(0, 1, 100)
    LL_BR = [loglike_lin(x, coeff=-50.0) for x in X_RANGE_BR]

    print "Max BR loglike: ", max(LL_BR)

    plt.scatter(X_RANGE_BR, LL_BR, label=r"Linear $\mathcal{BR}$ loglike",
                s=2)

    # --- What typically goes into loglike_lin (xsect)
    X_RANGE_XSECT = np.linspace(1, 200, 100)
    LL_XSECT = [loglike_lin(x, coeff=50, add=116, scale=0.001) for x in X_RANGE_XSECT]
    print loglike_lin(100, coeff=50, add=116, scale=0.001)

    print "Max xsect loglike: ", max(LL_XSECT)

    plt.scatter(X_RANGE_XSECT, LL_XSECT,
                label=r"Linear $\sigma$ loglike",
                s=2)

    plt.title("Loglikelihood functions")
    plt.xlabel("Value")
    plt.ylabel("Loglike(value)")
    plt.legend()
    #plt.ylim([-20, 20])
    plt.show()
