# pylint: disable=C0303, C0103

import numpy as np
import re
import os
import pyslha

PDG = {
      '1000001'  :'~d_L'    ,
      '1000002'  :'~u_L'    ,
      '1000003'  :'~s_L'    ,
      '1000004'  :'~c_L'    ,
      '1000005'  :'~b_1'    ,
      '1000006'  :'~t_1'    ,
      '1000011'  :'~e_L'    ,
      '1000012'  :'~nu_eL'  ,
      '1000013'  :'~mu_L'   ,
      '1000014'  :'~nu_muL' ,
      '1000015'  :'~tau_1'  ,
      '1000016'  :'~nu_tauL',
      '2000001'  :'~d_R'    ,
      '2000002'  :'~u_R'    ,
      '2000003'  :'~s_R'    ,
      '2000004'  :'~c_R'    ,
      '2000005'  :'~b_2'    ,
      '2000006'  :'~t_2'    ,
      '2000011'  :'~e_R'    ,
      '2000013'  :'~mu_R'   ,
      '2000015'  :'~tau_2'  ,
      '1000021'  :'~g'      ,
      '1000022'  :'~chi_10' ,
      '1000023'  :'~chi_20' ,
      '1000024'  :'~chi_1+' ,
      '1000025'  :'~chi_30' ,
      '1000035'  :'~chi_40' ,
      '1000037'  :'~chi_2+' ,
      '1000039'  :'~G'
      }

PDG_INV = {
     '~d_L'     :'1000001' ,
     '~u_L'     :'1000002' ,
     '~s_L'     :'1000003' ,
     '~c_L'     :'1000004' ,
     '~b_1'     :'1000005' ,
     '~t_1'     :'1000006' ,
     '~e_L'     :'1000011' ,
     '~nu_eL'   :'1000012' ,
     '~mu_L'    :'1000013' ,
     '~nu_muL'  :'1000014' ,
     '~tau_1'   :'1000015' ,
     '~nu_tauL' :'1000016' ,
     '~d_R'     :'2000001' ,
     '~u_R'     :'2000002' ,
     '~s_R'     :'2000003' ,
     '~c_R'     :'2000004' ,
     '~b_2'     :'2000005' ,
     '~t_2'     :'2000006' ,
     '~e_R'     :'2000011' ,
     '~mu_R'    :'2000013' ,
     '~tau_2'   :'2000015' ,
     '~g'       :'1000021' ,
     '~chi_10'  :'1000022' ,
     '~chi_20'  :'1000023' ,
     '~chi_1+'  :'1000024' ,
     '~chi_30'  :'1000025' ,
     '~chi_40'  :'1000035' ,
     '~chi_2+'  :'1000037' ,
     '~G'       :'1000039'
      }

#def is_lsp(massdict, pid):
#  """ Checks if particle with given pid is the lsp """
#  is_ = True
#  for k in susy_keys:
#    if abs(massdict[k]) > abs(massdict[pid]):
#       is_ = False
#  return is_

def alpha(g, power=1):
    """
    Returns alpha from coupling g.
    Optional parameter power = exponent of alpha (default=1)
    value -1 makes the function return alpha inverse.
    """

    mW = 80.0
    mZ = 91.0
    cw = mW/mZ
    sw = np.sqrt(1-cw**2)

    return (g**2*sw**2/(4*np.pi))**power

def all_subdirs_of(root_dir='.'):
    """
    List all sub-directories of the root dir (default is current dir)
    """
    result = []
    for subdir in os.listdir(root_dir):
        subdir_path = os.path.join(root_dir, subdir)
        if os.path.isdir(subdir_path):
            result.append(subdir_path)
    return result

def lsp(massdict):
    """
    Input: Mass dict of type pyslha.Block
    Returns a tuple with (pid, mass) of the LSP.
    """
    if not isinstance(massdict, pyslha.Block):
        raise TypeError("Expected input of type pyslha.Block." "Got: ",
                        type(massdict))

    # --- Brute force way of getting only the supersymmetric particles from the
    # --- mass dict (these have PDG-codes consisting of seven characters)
    sparticles_dict = [ptuple for ptuple in massdict.items() if
                       len(str(abs(ptuple[0]))) == 7]

    # --- Return the entry with the smalles mass
    return min(sparticles_dict, key=lambda x: abs(x[1]))

def num(l, index=None):
    """
    Finds number in line l (or list of index index in list of lines l)
    The number can be in scientific format
    """
    if index is None:
        number = re.findall("[+-]?\d+\.\d*[Ee]?[+-]?\d*", l)
        if len(number) == 1:
            return number[0]
        return number

    else:
        number = re.findall("[+-]?\d+\.\d*[Ee]?[+-]?\d*", l[index])
        if len(number) == 1:
            return number[0]
        return number

def num_after(string, line):
    """
    Finds numbers (can be in scientific format) after a string.
    Input: string to start looking from, line to check.
    """
    # --- Find all matches
    matches = re.findall("(?<={0})[:,=]?\s*[+-]?\d+\.\d*[Ee]?[+-]?\d*"
                         .format(re.escape(string)), line)

    # --- Makes sure the matches contain only numbers (incl scientific format)
    num_matches = map(num, matches)

    return num_matches

def num_with_pm(string1, string2, line):
    """
    Returns the two numbers separated by +-, betweeen string1 and string2.
    This still needs testing. Assumes the number to be on the form
    (digits) +- (digits)
    """
    # --- Find all matches
    match_group = re.search("\s*{0}\s*:\s+(.*?)\s+[+-]+\s+(.*?)\s+{1}"
                            .format(string1, string2), line)


    # --- Makes sure the matches contain only numbers (incl scientific format)
    num_matches = map(num, match_group) #TODO: test if this works

    return match_group(1), match_group(2)

def extract(pattern1, pattern2, output):
    """ Returns written between 'pattern1' and 'pattern2' in output.
    Returns False if pattern isn't found.
    """
    try:
        #found = float(re.search(pattern1+'(.+?)'+pattern2, output).group(1))
        found = map(float, re.findall(pattern1+'(.+?)'+pattern2, output))
    except (AttributeError, ValueError):
        return False

    return found

# TODO: Change this into using pyslha. Where is the function being used?
def running_params(filename):
    """
    Collects every scale Q and corresponding running parameters.
    Returns dict containing all soft masses and mu, the three running couplings
    and the three coupligs at M_GUT
    """
    from collections import defaultdict
    data = defaultdict(list)

    start = False # Start after block SMINPUTS

    params = ['m0', 'm12', 'tanb at m_Z', 'cos(phase_mu)', 'A0', 'Q']
    masses = ['M_1','M_2','M_3','M^2_(H,d)','M^2_(H,u)','mu',
    'M_(L,11)','M_(L,22)','M_(L,33)','M_(E,11)','M_(E,22)',
    'M_(E,33)','M_(Q,11)','M_(Q,22)','M_(Q,33)','M_(U,11)',
    'M_(U,22)','M_(U,33)','M_(D,11)','M_(D,22)','M_(D,33)']
    gs = ["g'(Q)^DRbar","g(Q)^DRbar", "g3(Q)^DRbar"]
    gMGUT = ["g'(M_GUT)^DRbar","g(M_GUT)^DRbar", "g3(M_GUT)^DRbar"]


    with open(filename) as f:
	  l = f.readlines()
    for line in l:
       if not start:
          for g in gMGUT:
             if '# '+g+'\n' in line:
                #MGUT[str(g)].append(num(line))
                data[str(g)].append(num(line))
          for i in params:
             if 'Block gauge '+i in line and '# (GUT scale)' in line:
                data['Q_GUT'].append(num(line))
             elif '# '+i in line:
                #param[str(i)].append(num(line))
                data[str(i)].append(num(line))
       if 'Block SMINPUTS' in line:
          start = True
       if start:
          if 'Block gauge Q=' in line:
             data['Q'].append(num(line))
             Q_val = True
          for m in masses:
             if '# '+m+'\n' in line:
                data[str(m)].append(num(line))
          for g in gs:
             if '# '+g+'\n' in line:
                data[str(g)].append(num(line))
                #couplings[str(g)].append(num(line))
    #return param, data, couplings, MGUT
    return data
#  --- Dont use these. Use PYSLHA or write something with awk
#def LSP(filename):
#    """
#    Compares the absolute value of masses in the MASS Block
#    of LesHouches file, and returns a tuple (name, mass) of the LSP
#    """
#    with open(filename) as f:
#		l=f.readlines()
#	 	i=0
#		start=0
#		stop=0
#		t=[]
#		for line in l:
#			i+=1
#			if not line.strip():
#					break
#			if line.find("Block MASS")!=-1:
#				start=i+6
#			if line.find("Block alpha")!=-1:
#				stop=i-2
#				break
#				# Make list of masses in block MASS
#		for i in range (start,stop):
#			if start==0:
#				break
#			t.append(abs(float(num(l,i)[0])))
#		# Find smallest element in list of masses
#		if start==0:
#			return ['error','0']
#		lmass=min(t)
#		# Find name of particle associated with smallest mass (the name behind the '#', up until the \n))
#		n=start+t.index(min(t))
#		p=2+l[n].index("#")
#		lsp=l[n][p:-1]
#		return [lsp,str(lmass)]
#
#def findmass(mass,fil):
#	if os.path.exists(fil):
#		with open(fil) as f:
#			for line in f:
#				if not line.strip():
#					break
#				if line.find(mass)!=-1:
#					l=re.findall("[+-]?\d\.\d*[Ee][+-]\d*", line)
#					if l:
#						return l[0]
#					else:
#						return -1
#					break
#	else:
#		return -1
#
#
#def LSPN(filename):
#   """ Compares the absolute value of masses in the MASS Block
#   of LesHouches file, and returns (name, mass) of lsp and nlsp.
#   """
#   with open(filename) as f:
#		l=f.readlines()
#	 	i=0
#		start=0
#		stop=0
#		t=[]
#		for line in l:
#			i+=1
#			if not line.strip():
#					break
#			if line.find("Block MASS")!=-1:
#				start=i+6
#			if line.find("Block alpha")!=-1:
#				stop=i-2
#				break
#				# Make list of masses in block MASS
#		for i in range (start,stop):
#			if start==0:
#				break
#			t.append(abs(float(num(l,i)[0])))
#		# Find smallest element in list of masses
#		if start==0:
#			return ['error','0']
#		mass=min(t)
#		m=t.index(min(t))
#		# Replace smallest mass with large number, find next-to-smallest number
#		t[m]=9999999999
#		nmass=min(t)
#		mn=t.index(min(t))
#		# Find name of particle associated with smallest mass (the name behind the '#', up until the \n))
#		n=start+m
#		p=2+l[n].index("#")
#		lsp=l[n][p:-1]
#		# Find name of particle associated with next-to-smallest mass (the name behind the '#', up until the \n))
#		nn=start+mn
#		pn=2+l[nn].index("#")
#		nlsp=l[nn][pn:-1]
#		return [lsp,str(mass),nlsp,str(nmass)]
#
