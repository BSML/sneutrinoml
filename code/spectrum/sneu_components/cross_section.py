import os
from xs_calculation import create_herwig_input_file, run_herwig, get_xs_from_herwig
from xs_calculation import SIMULATIONS_DIR


def main(param):
    """calculates the cross section for the ew and qcd signal
       return the param dictionary with the keys xs_ew and xs_qcd
    """
    #try:
    if True:
        create_herwig_input_file('EW', param['slha_output_file'])
        run_herwig('EW')
        param['xs_ew'] = get_xs_from_herwig(process ='EW')

        create_herwig_input_file('QCD', param['slha_output_file'])
        run_herwig('QCD')
        param['xs_qcd'] = get_xs_from_herwig(process = 'QCD')

    return param



if __name__ == "__main__":
    slha_output = 'slha_output_file_577'

    
    create_herwig_input_file('EW', slha_output)
    run_herwig('EW')
    ew_output_xs_file = os.path.join(SIMULATIONS_DIR,'HerwigTest/crossSections_mrK.txt') 
    xs_ew = get_xs_from_herwig(ew_output_xs_file)
    
    print('XS EW: ' + str(xs_ew))
    
    create_herwig_input_file('QCD', slha_output)
    run_herwig('QCD')
    qcd_output_xs_file =  os.path.join(SIMULATIONS_DIR,'HerwigTest2/crossSections_mrK.txt')
    xs_qcd = get_xs_from_herwig(qcd_output_xs_file)
    
    print('XS QCD: ' + str(xs_qcd))

