import numpy as np
import matplotlib.pyplot as plt

import pandas as pd

import seaborn as sns

import os
import subprocess
import socket
import random
import sys

HOSTNAME = socket.gethostname()


PLOTFILES = ['grid_scan_0.csv', 'grid_scan_5.csv', 'grid_scan_10.csv', 'grid_scan_12.csv' ,'grid_scan_20.csv', 'grid_scan_30.csv', 'grid_scan_40.csv', 'grid_scan_50.csv']


def plot(pointFiles):
    for i,file in enumerate(pointFiles):
        df_point = pd.read_csv(file, delimiter='\t', names=['Iteration', 'm_Higgs', 'point', 'At', 'M2', 'M3'])
        if i == 0:
            df = df_point
        else:
            df = df.append(df_point)
    df.point = df.point.apply(lambda p: "Point " + str(p))
    ax = sns.lineplot(x="At", y="m_Higgs", data=df, hue="point")      
    ax.axhline(123, ls='--')
    ax.axhline(127, ls='--')
   
    #points = plt.scatter(df["point"], df["At"],
    #                c=df["m_Higgs"], s=20, cmap="Spectral") #set style options

    #plt.colorbar(points)

    plt.show()


if __name__ == "__main__":


    if HOSTNAME == '':
        ROOT_DIR = 'code/spectrum/'

    plotfiles = map(lambda x: os.path.join(ROOT_DIR,x), PLOTFILES)

    plot(plotfiles)