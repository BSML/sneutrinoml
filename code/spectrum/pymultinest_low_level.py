"""
--- Inga Strumke
Use PyMultinest to scan over A_t, M_(Q,33), M_(U,33), M_(D,33)
 Calculate spectrum.
   0) Spectrum valid. If not, loglike -1e99
   1a) Higgs mass 123< <127    Normal loglike
   1b) sneutrino LSP           Heaviside loglike
   2) BR(~nu_mu -> ~nu_tau mu^- tau^+) > 1.0E-01   Linearly increasing loglike
   3) TODO: Daniel's xsect script            Lin loglike
"""

#import sys
#import numpy as np
#import subprocess
#import threading
#import sneu_components.modules as mod

#from __future__ import absolute_import, unicode_literals #, print_function

from argparse import ArgumentParser
import shutil
import os
import math
import random
import pymultinest
import scipy as sp

import sneu_components.spectrum_low as spectrum

from sneu_components.likelihoods import loglike_normal
from sneu_components.likelihoods import loglike_lin
from sneu_components.likelihoods import LL_MIN

# =================================================================================================

# === General settings ============================================================================

GENERATOR = 'spheno'
SLHA_DIR = "slha_dir4"

# === Ranges and parameters =======================================================================

A_T_MIN = -1.0E-02
A_T_MAX = 0

M_MIN = 0
M_MAX = 1E+04

ML_MIN = 0
ML_MAX = 1E+03
#A_T_MIN = -7.0E-03
#A_T_MAX = -3.0E-03
#
#M_MIN = 1.0E+03
#M_MAX = 6.0E+03
#
#ML_MIN = 5.0E+01
#ML_MAX = 5.0E+02

SIGMA_HIGGS = 0.5

# === Prior functions =============================================================================

def logpriorflat(lower, upper, randvar):
    """
    Log(flat prior) function
    """
    outvar = lower*math.exp(randvar*math.log(upper/lower))
    return outvar

# =================================================================================================

# =================================================================================================
# === PyMultinest functions

def myprior(cube, ndim, nparams):
    """
    Cube entries varied by PyMultinest.
    The prior function scales the unit cube entries for the loglike function.
    """
    # --- Scan ranges -----------------------------------------------------------------------------

    # --- map [0,1] to required ranges:
    var_at = A_T_MIN  + (A_T_MAX-A_T_MIN)*cube[PARAMETERS.index('A_T')]
    var_mq33 = M_MIN + (M_MAX-M_MIN)*cube[PARAMETERS.index('MQ33')]
    var_mu33 = M_MIN + (M_MAX-M_MIN)*cube[PARAMETERS.index('MU33')]
    var_md33 = M_MIN + (M_MAX-M_MIN)*cube[PARAMETERS.index('MD33')]
    var_ml33 = ML_MIN + (ML_MAX-ML_MIN)*cube[PARAMETERS.index('ML33')]
    var_me33 = M_MIN + (M_MAX-M_MIN)*cube[PARAMETERS.index('ME33')]
    # mL, mE

    #----------------------------------------------------------------------------------------------

    # --- Fill the cube ---------------------------------------------------------------------------
    cube[PARAMETERS.index('A_T')] = var_at
    cube[PARAMETERS.index('MQ33')] = var_mq33
    cube[PARAMETERS.index('MU33')] = var_mu33
    cube[PARAMETERS.index('MD33')] = var_md33
    cube[PARAMETERS.index('ML33')] = var_ml33
    cube[PARAMETERS.index('ME33')] = var_me33

def myloglike(cube, ndim, nparams):

    # --- Random process ID. Hopefully temporary.
    #pid = int(round(100*random.random(), 4))

    # --- Read values from cube -------------------------------------------------------------------
    var_at = cube[PARAMETERS.index('A_T')]
    var_mq33 = cube[PARAMETERS.index('MQ33')]
    var_mu33 = cube[PARAMETERS.index('MU33')]
    var_md33 = cube[PARAMETERS.index('MD33')]
    var_ml33 = cube[PARAMETERS.index('ML33')]
    var_me33 = cube[PARAMETERS.index('ME33')]

    # --- Fill dictionary -------------------------------------------------------------------------

    # --- Associate SLHA output and input files name with process number
    slha_input_file = os.path.abspath(os.path.join(SLHA_DIR, "temp_slha_input_file"))#_proc_{0}'.format(pid))
    slha_output_file = os.path.abspath(os.path.join(SLHA_DIR, "temp_slha_output_file"))#_proc_{0}'.format(pid))
    param = {
        #... Varied during run:
        'A_T' : var_at,
        'MQ33': var_mq33,
        'MU33' : var_mu33,
        'MD33' : var_md33,
        'ML33' : var_ml33,
        'ME33' : var_me33,
        # ...
        # ... Constants not recorded ...
        'Qin' : 1000,
        'slha_input_file' : slha_input_file,
        'slha_output_file' : slha_output_file,
        'valid' : True,                                 # Failing modules can set this to False
        'warning' : None,                               # Modules can return warning message
        'generator' : GENERATOR,
        # ...
        }

    #----------------------------------------------------------------------------------------------
    # ---  Modules section ------------------------------------------------------------------------

    # --- Calculate spectrum ----------------------------------------------------------------------
    param = spectrum.main(param)                        # adds
                                                        # 'slha', m_higgs,
                                                        # 'br',
                                                        # can set
                                                        # 'valid' -> False


    param = spectrum.feynhiggs_correction(param)        # adds m_higgs_fh

    param = spectrum.xs_calc(param)                     # Daniel's module. Adds
                                                        # xsect for ew (xs_ew) and qcd (xs_qcd) signals
    #param['xsect'] = 0

    #----------------------------------------------------------------------------------------------
    if not param['valid']:
        #print 'not valid'
        ll_scan = LL_MIN
        cube[PARAMETERS.index('ll_scan')] = ll_scan
        return ll_scan

    # --- if point has valid spectrum:
    #try:
    if True: # For testing
        # --- CHECK -------------------------------------------------------------------------------
        # --- LSP ---------------------------------------------------------------------------------
        if param['lsp'] != 1000016:
            ll_scan = LL_MIN
            cube[PARAMETERS.index('ll_scan')] = ll_scan
            return ll_scan

        if param['br']>0.4:
            random_id = int(round(100*random.random(), 4))
            shutil.copyfile(param['slha_input_file'], os.path.join("hope_files", "slha_input_{0}".format(random_id)))

        # -----------------------------------------------------------------------------------------

        # --- Higgs mass --------------------------------------------------------------------------
        #ll_higgs = loglike_normal(param['m_higgs'], 125, SIGMA_HIGGS) #Mean=125GeV
        ll_higgs = loglike_normal(param['m_higgs_fh'], 125, SIGMA_HIGGS) #Mean=125GeV

        # --- Branching ratio for signal ----------------------------------------------------------
        ll_br = loglike_lin(param['br']) #max 0, when br=1, else negative


        # --- Cross section -----------------------------------------------------------------------
        ll_xsect = loglike_lin(param['xs_qcd'])

        # --- Record calculated parameters if the point is not discarded --------------------------
        cube[PARAMETERS.index('lsp')] = param['lsp']
        cube[PARAMETERS.index('mlsp')] = param['mlsp']
        cube[PARAMETERS.index('m_higgs')] = param['m_higgs']
        cube[PARAMETERS.index('m_higgs_fh')] = param['m_higgs_fh']
        cube[PARAMETERS.index('br')] = param['br']
        cube[PARAMETERS.index('xs_ew')] = param['xs_ew']
        cube[PARAMETERS.index('xs_qcd')] = param['xs_qcd']

        # --- end of modules and calculations -----------------------------------------------------
        # -----------------------------------------------------------------------------------------

        # =========================================================================================
        # === LIKELIHOODS =========================================================================

        # --- The likelihood that guides the scan -------------------------------------------------
        ll_scan = ll_br + ll_higgs + ll_xsect

        # --- Add likelihoods to the cube ---------------------------------------------------------
        cube[PARAMETERS.index('ll_higgs')] = ll_higgs
        cube[PARAMETERS.index('ll_br')] = ll_br
        cube[PARAMETERS.index('ll_xsect')] = ll_xsect
        cube[PARAMETERS.index('ll_scan')] = ll_scan


        # === END LIKELIHOODS =====================================================================
        # =========================================================================================


    # ---------------------------------------------------------------------------------------------
    else: # --- (if not True) --- For testing
    #except Exception: #TypeError:
        print "FATAL ERROR IN POINT {0}".format(param)

        ll_scan = LL_MIN
        cube[PARAMETERS.index('ll_scan')] = ll_scan

    # ---------------------------------------------------------------------------------------------

    # -- Clean up and return ----------------------------------------------------------------------
    try:
        os.remove(param['slha_input_file'])
        os.remove(param['slha_output_file'])
    except Exception:
        pass

    #print ll_scan
    return ll_scan


# =================================================================================================
# === MAIN

if __name__ == '__main__':

    # ---------------------------------------------------------------------------------------------
    # --- Directory for SLHA file Directory for SLHA filess
    # ---------------------------------------------------------------------------------------------
    if not os.path.isdir(SLHA_DIR):
        os.mkdir(SLHA_DIR)

    # ---------------------------------------------------------------------------------------------
    # --- All (in- and output) parameters
    # ---------------------------------------------------------------------------------------------

    PARAMETERS = [
        # --- Input parameters
        'A_T',
        'MQ33',
        'MU33',
        'MD33',
        'ML33',
        'ME33',
        'Qin',

        # --- Output parameters:
        "lsp", "mlsp",
        "m_higgs",
        "m_higgs_fh",
        "br", "xsect",

        # --- Likelihoods:
        "ll_higgs",
        "ll_br",
        "ll_xsect",
        "ll_scan",
        ]

    N_PARAMS = len(PARAMETERS)

    # ---------------------------------------------------------------------------------------------
    # --- Collect arguments
    # ---------------------------------------------------------------------------------------------

    PARSER = ArgumentParser(description='Plot scan result')
    PARSER.add_argument('-dir', '--directory', help='Save scan result to directory')
    PARSER.add_argument('-n', '--name', help='Run name')
    PARSER.add_argument('-v', '--verbose', help='Verbose PyMultinest run', action='store_true')
    PARSER.add_argument('-t', '--testrun', action='store_true',
                        help='Run for testing modules, few points checked.')
    PARGS = PARSER.parse_args()

    if PARGS.directory:
        DIR = PARGS.directory
    else:
        DIR = 'chains_sc'

    if not os.path.isdir(DIR):
        os.mkdir(DIR)

    if PARGS.name:
        CHAINS = os.path.join(DIR, PARGS.name)
    else:
        CHAINS = str(DIR)+'/testrun'

    print 'Result written to {0}'.format(CHAINS)

    print 'Running multinest...'

    VERBOSE = PARGS.verbose

    # ---------------------------------------------------------------------------------------------
    # --- Run PyMultinest
    # ---------------------------------------------------------------------------------------------

    if PARGS.testrun:
        pymultinest.run(
            myloglike, myprior, N_PARAMS,
            verbose=VERBOSE,
            importance_nested_sampling=False,
            resume=False,
            sampling_efficiency=0.8,
            n_live_points=100,
            evidence_tolerance=0.5,
            log_zero=-1.0e99,
            n_iter_before_update=50,
            outputfiles_basename=CHAINS
            )
    else:
        pymultinest.run(
            myloglike, myprior, N_PARAMS,
            verbose=VERBOSE,
            importance_nested_sampling=False,
            resume=False,
            sampling_efficiency=0.8, #0.01,
            n_live_points=500, # 5000,
            evidence_tolerance=0.1, #0.01,
            log_zero=-1.0e99,
            n_iter_before_update=500,
            outputfiles_basename=CHAINS
            )

    # ---------------------------------------------------------------------------------------------
    # --- Write relevant info to file after PyMultinest finishes
    # ---------------------------------------------------------------------------------------------

    HEADER = ("#                     weight                        -2ll "
              +(" ").join(str(x).rjust(27) for x in PARAMETERS)+"\n")
    open(CHAINS+"data.dat", "w").write(HEADER+open(CHAINS+".txt").read())

    # ---------------------------------------------------------------------------------------------
    # --- Clean up
    # ---------------------------------------------------------------------------------------------

    WORKING_DIR = '.'

    for fname in os.listdir(WORKING_DIR):
        if fname.startswith("_temp_slha_input_file_proc_") or fname == "Messages.out":
            os.remove(os.path.join(WORKING_DIR, fname))

# =================================================================================================
