"""Use KS test to compare kdes to those from point12.

The k-s test returns a D statistic and a p-value corresponding to the D
statistic. The D statistic is the absolute max distance (supremum) between the
CDFs of the two samples.
The closer D is to 0 the more likely it is that the two samples were drawn from
the same distribution.
You reject the null hypothesis (that the two samples were drawn from the same
distribution) if the p-value is less than your significance level.
"""

import sys
import os
import pickle
import re
import glob
import numpy as np
import matplotlib.pyplot as plt

from nn_kde import nn_kde
from utilities import format_alpha
from plots import plot_kdes
from global_settings import COLORS, LINESTYLES, LABELDICT, POINTS

from global_settings_xg import (NAME as NAME_xg, PRED_DIR as PRED_DIR_xg,
                                TEMPLATE_DIR as TEMPLATE_DIR_xg)
from global_settings_nn import (NAME as NAME_nn, PRED_DIR as PRED_DIR_nn,
                                TEMPLATE_DIR as TEMPLATE_DIR_nn)
from scipy.stats import kstest


# TODO: copy below function from MLE_fit to utilities
def kdes_from_files(files, plot=False, labels=["Unlabeled 0", "Unlabeled 1"]):
    """
    Send arrays from files to kdes_from_arrays
    """

    templates = [np.load(_file) for _file in files]

    return kdes_from_arrays(templates, plot=plot, labels=labels)

def kdes_from_arrays(arrays, plot=False, labels=["Unlabeled 0", "Unlabeled 1"], colors=COLORS):
    """
    Creates KDES per array in arrays
    Used by neuralnet.py and xgboost_model.py
    """

    kdes = make_nn_kdes(arrays, verbose=False, plot=plot, labels=labels, colors=colors)

    return kdes

def make_nn_kdes(arrays, verbose=True, plot=False,
        labels=["Unlabeled 0", "Unlabeled 1"], colors=COLORS):
    """
    Make kdes from nn_kde class NnKde using bf bandwidth values
    This function assumes that the input arrays are sorted [class0, class1]
    """

    bw_0 = 0.01
    bw_1 = 0.01

    kdes = [
        nn_kde.NnKde(bandwidth=bw_0,
                     verbose=verbose).fit(arrays[0]),
        nn_kde.NnKde(bandwidth=bw_1,
                     verbose=verbose).fit(arrays[1])
           ]

    if plot:
        plot_kdes(kdes, labels, colors=colors)

    return kdes


N_POINTS = 1000
PLOT = True

temp_dir_p12_nn = TEMPLATE_DIR_nn
labels_p12_nn = [re.findall("template_(.*?).npy", _file)[0] for _file in
          os.listdir(temp_dir_p12_nn)]
files_p12_nn = [os.path.join(temp_dir_p12_nn, _file) for _file in
         os.listdir(temp_dir_p12_nn)]
kdes_p12_nn = kdes_from_files(files_p12_nn, labels=labels_p12_nn, plot=PLOT)
plt.show()
x_vals = np.linspace(0.0, 1.0, N_POINTS)
pdfs12 = [_kde.evaluate(x_vals) for _kde in kdes_p12_nn]

temp_dir_p00_nn = "neuralnet/Templates/nn/point00"
labels_p00_nn = [re.findall("template_(.*?).npy", _file)[0] for _file in
          os.listdir(temp_dir_p00_nn)]
files_p00_nn = [os.path.join(temp_dir_p00_nn, _file) for _file in
         os.listdir(temp_dir_p00_nn)]
kdes_p00_nn = kdes_from_files(files_p00_nn, labels=labels_p00_nn, plot=PLOT)
plt.show()
x_vals = np.linspace(0.0, 1.0, N_POINTS)
pdfs00 = [_kde.evaluate(x_vals) for _kde in kdes_p00_nn]

print(kstest(pdfs12[0], pdfs00[0]))
print(kstest(pdfs12[1], pdfs00[1]))
