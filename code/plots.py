"""
Plot functions for autoencoder classifier framework
"""

import sys
import re
import os
from argparse import ArgumentParser
import pylab
import numpy as np
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as plt

from global_settings import (LINESTYLES, COLORS, NBINS, TRAIN_DIR, LABELDICT,
        get_color, get_linestyle)

def plot_kdes(kdes, labels, n_points=1000, colors=COLORS):
    """
    Plot scipy kde functions evaluated over n_points (default 1000) points.
    """

    x_vals = np.linspace(0.0, 1.0, n_points)
    pdfs = [_kde.evaluate(x_vals) for _kde in kdes]

    for _n, _pdf in enumerate(pdfs):
        plt.plot(x_vals, _pdf, label=labels[_n], lw=1.5,
                 #c=colors[_n], ls=LINESTYLES[_n],
                 c=get_color(labels[_n]), ls=get_linestyle(labels[_n])
                 )

    plt.legend()
    plt.xlabel("Model output")
    plt.ylabel("Event density")

def plot_predictions(y_true, y_pred, show=True, nbins=NBINS):
    """
    Plot histograms of true and predicted values
    """

    low, high = 0.0, 1.0
    rng = (low, high)
    ax1 = plt.gca()

    # --- Plot predictions
    ax1.hist(y_pred, bins=nbins, range=rng,
             histtype="step",
             color="black",
             linestyle='-',
            )
    ax1.plot([0], linestyle='-', c="black", label="Prediction")
    # ---

    # --- Plot predictions indicating true labels
    true_0 = y_pred[y_true == 0]
    true_1 = y_pred[y_true == 1]

    datalabels = ("True background class", "True signal class")

    for _k, data in enumerate([true_0, true_1]):

        ax1.hist(data, bins=nbins, range=rng,
                 histtype='step',
                 linestyle="--",
                 #linestyle=LINESTYLES[_k],
                 color=COLORS[_k]
                )

        ax1.plot([0],
                 linestyle="--",
                 #linestyle=LINESTYLES[_k],
                 c=COLORS[_k],
                 label=datalabels[_k])

        ax1.set_xlim([low, high])
        #ax1.set_autoscale_on(False)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    # ---


    n_signal = 1.0*sum(y_true)/len(y_true)
    ax1.legend()
    plt.title(r"Predictions with $\alpha_{{\mathrm{{true}}}}=${0:.3f}".format(n_signal))
    plt.xlabel("Classifier output")
    plt.ylabel("Event density")
    plt.ylim([0,5e2])
    if show: plt.show()

    print("Signal events: {0}".format(n_signal))
    print("Background events: {0}".format(1-n_signal))
