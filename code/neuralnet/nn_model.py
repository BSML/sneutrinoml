# pylint: disable=C0303

import pickle
import numpy as np
import os

import tensorflow as tf
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Activation, Lambda, InputLayer
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import AlphaDropout
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import LeakyReLU
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from sklearn.metrics import accuracy_score

from utilities import get_dataset_from_path
from global_settings_nn import (EPOCHS, BATCH_SIZE, VERBOSE, NAME, MODELFILE,
        SCALER, HISTORY, TEMPLATE_DIR, PRED_DIR, OPT_DIR)

def train(traindir,
          nametag,
          scalerfile="default_scaler.pkl",
          recreate_scaler=False,
          verbose=1):
    """
    Train a network.

    Arguments:
        traindir: Path to training data
        nametag: Name tag for saved files (model, scaler...)
        scalerfile: Name of file to read/write scaler instance
        recreate_scaler: Make a new scaler and save new scalerfile as pickle
        verbose: Keras verbosity in terminal during run
    """

    x_train, y_train = np.load("data/x_train.npy"), np.load("data/y_train.npy")
    x_test, y_test = np.load("data/x_val.npy"), np.load("data/y_val.npy")

    # --- First get the scaler object, recreate if it doesn't exist or if required
    if not os.path.exists(scalerfile) or recreate_scaler:

        print("Recreating scaler...")
        scaler = preprocessing.MinMaxScaler().fit(x_train)

        with open(scalerfile, 'wb') as hfile:
            pickle.dump(scaler, hfile)

    else:
        with open(scalerfile, 'rb') as hfile:
            scaler = pickle.load(hfile)

    x_train = scaler.transform(x_train)
    x_test = scaler.transform(x_test)


    # --- Create network model

    model = create_model(x_train.shape[1])

    print("Created model")

    # --- Callbacks
    reduce_lr = ReduceLROnPlateau(min_lr=1e-7, verbose=1, cooldown=10, patience=10)
    early_stop = EarlyStopping(monitor="val_loss",
                               min_delta=0,
                               patience=15,
                               verbose=1,
                               mode="auto")

    # --- Train
    #with tf.device('/CPU:0'):
    loss_history = model.fit(x_train, y_train,
                                epochs=EPOCHS,
                                batch_size=BATCH_SIZE,
                                validation_data=(x_test, y_test),
                                callbacks=[reduce_lr, early_stop],
                                verbose=verbose
                                )

    # --- Save model
    if ".h5" not in nametag:
        nametag += ".h5"
    model.save(MODELFILE)

    # --- Store training history
    with open(HISTORY, 'wb') as hfile:
        pickle.dump(loss_history.history, hfile)
    print("Training history written to Models/loss_history_%s.pkl" % nametag)

    # --- Prediction
    y_pred = model.predict(x_test)#[:, 1]

    predictions = [np.round(_value) for _value in y_pred]

    # --- Evaluate predictions
    accuracy = accuracy_score(y_test, predictions)

    print("Accuracy: %.2f%%" % (accuracy * 100.0))

    return


def optimization_nn_model(xdim,
                          num_dense_layers=2,
                          num_dense_nodes=50,
                          drop = 0.375,
                          initializer='he_uniform',
                          activationfn='LeakyReLU',
                          output_activation = 'sigmoid',
                          lossfn="binary_crossentropy",
                          learning_rate = 5e-5
                          ):
    """
    Modular nn spesified by the input parameters to the function
    (Used for the GridSearchCV algorithm)

    Input:
        xdim: number features
        drop: droput
        initializer: kernal initializer for each layer.
                     Can be specified as an array of strings
                     (one for each layer)
                     or a string applied to all layers
        activationfn: activation function for the hidden layers
                      Can be specified as an array
                      (one for each layer)
                      or a single activation functin for all
        output_activation: output layer activation function
        lossfn: Loss function
        lr: Learning rate

    """

    model = Sequential()
    metric = ["accuracy"]
    optim = Adam(lr=learning_rate)

    model.add(InputLayer(input_shape=(xdim)))


    for i in range(num_dense_layers):
        name = 'layer_dense_{0}'.format(i+1)

        # add dense layer
        model.add(Dense(num_dense_nodes,
                        activation=activation_function_map(activationfn),
                        kernel_initializer=initializer,
                        name=name))

        model.add(BatchNormalization())
        model.add(Dropout(drop))


    # --- Output layer
    model.add(Dense(1, kernel_initializer=initializer))
    model.add(BatchNormalization())
    model.add(Activation(output_activation))

    model.compile(optimizer=optim, loss=lossfn, metrics=metric)

    return model


def create_model(xdim):#, ydim):
    """
    Create a simple sequential model for two-class binary targets.
    """

    model = Sequential()

    initializer = 'he_uniform'
    drop = 0.21
    output_dim = 1

    output_activation = "sigmoid"
    lossfn = "binary_crossentropy"
    metric = ["accuracy"]
    optim = Adam(lr = 0.001)

    # --- Hidden layer 1
    model.add(Dense(500, input_dim=xdim, kernel_initializer=initializer))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(drop))

    # --- Hidden layer 2
    model.add(Dense(500, kernel_initializer=initializer))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(drop))

    # --- Hidden layer 3
    model.add(Dense(250, kernel_initializer=initializer))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(drop))

    # --- Hidden layer 4
    model.add(Dense(100, kernel_initializer=initializer))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(drop))

    ## --- Hidden layer 5
    model.add(Dense(50, kernel_initializer=initializer))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(drop))


    # --- Output layer
    model.add(Dense(output_dim, kernel_initializer=initializer))
    model.add(BatchNormalization())
    model.add(Activation(output_activation))

    model.compile(optimizer=optim, loss=lossfn, metrics=metric)

    return model
