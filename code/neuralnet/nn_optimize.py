# pylint: disable=C0303
"""
This module trains a classifier on a the dataset given in global_settings (run
with option --train), and can be used to predict (-pred) and make templates
(-temp) on balanced data.
"""

import sys
import os
import warnings
from argparse import ArgumentParser
import pickle
import numpy as np
import tensorflow as tf
from tensorflow.keras import backend as K
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau

import skopt
from skopt import gp_minimize, forest_minimize
from skopt.plots import plot_convergence
from skopt.plots import plot_objective, plot_evaluations
from skopt.plots import plot_histogram, plot_objective_2D
from skopt.utils import use_named_args
from skopt.space import Real, Categorical, Integer


from utilities import (get_dataset_from_path,)

from nn_model import optimization_nn_model

from global_settings_nn import (EPOCHS, BATCH_SIZE, VERBOSE, NAME, MODELFILE,
        SCALER, TEMPLATE_DIR, PRED_DIR, OPT_DIR)

from global_settings import TRAIN_DIR

try:
    import matplotlib.pyplot as plt
    DISABLE_GRAPHICS = False
except ImportError:
    DISABLE_GRAPHICS = True





configuration = tf.compat.v1.ConfigProto(device_count={"GPU": 1},intra_op_parallelism_threads=12)
session = tf.compat.v1.Session(config=configuration)

warnings.filterwarnings("ignore")




#################
# OPTIMIZATION
#################
dim_batch_size  = Integer(low=10, high=80, name='batch_size')
dim_drop        = Real(low=0.01, high=0.5, name='drop')

dim_initializer = Categorical(categories=['he_uniform'], name='initializer')
dim_output_activationfn = Categorical(categories=['sigmoid'], name='output_activation')
dim_lossfn = Categorical(categories=['binary_crossentropy'], name='lossfn')

dim_learning_rate    = Real(low=1e-6, high=1e-1, prior='log-uniform', name='learning_rate')
dim_num_dense_layers = Integer(low=3, high=8, name='num_dense_layers')
dim_num_dense_nodes  = Integer(low=5, high=600, name='num_dense_nodes')
dim_activation = Categorical(categories=['LeakyReLU'], name='activationfn')

OPT_DIMENSIONS = [dim_batch_size,
                dim_drop,
                dim_initializer,
                dim_output_activationfn,
                dim_lossfn,
                dim_learning_rate,
                dim_num_dense_layers,
                dim_num_dense_nodes,
                dim_activation]

OPT_DEFAULT_PARAMETERS = [50, 0.375, 'he_uniform', 'sigmoid', 'binary_crossentropy', 0.001, 3, 50, 'LeakyReLU']




# Load the data to be used in the fitness function.
x_data, y_data, _ = get_dataset_from_path(TRAIN_DIR, multiclass=False)
with open(SCALER, 'rb') as hfile:
            scaler = pickle.load(hfile)
x_data = scaler.transform(x_data)

x_train, x_val, y_train, y_val = train_test_split(x_data,y_data,test_size=0.1)



@use_named_args(dimensions=OPT_DIMENSIONS)
def fitness(batch_size=50,
            num_dense_layers=2,
            num_dense_nodes=50,
            drop = 0.375,
            initializer='he_uniform',
            activationfn='LeakyReLU',
            output_activation = 'sigmoid',
            lossfn="binary_crossentropy",
            learning_rate = 5e-5):
    """
    Hyper-parameters:
    learning_rate:     Learning-rate for the optimizer.
    num_dense_layers:  Number of dense layers.
    num_dense_nodes:   Number of nodes in each dense layer.
    activation:        Activation function for all layers.
    drop:              Dropput rate
    initializer:       Initialization of the nodes
    output_activation: Output activation function
    lossfn:            Loss function
    """

    # Print the hyper-parameters.
    print('batch_size: ', batch_size)
    print('learning rate: {0:.1e}'.format(learning_rate))
    print('num_dense_layers: ', num_dense_layers)
    print('num_dense_nodes: ', num_dense_nodes)
    print('activation: ', activationfn)
    print('drop: ', drop)
    print('initializer: ', initializer)
    print('output_activation: ', output_activation)
    print('lossfn: ',lossfn)
    print()

    # Create the neural network with these hyper-parameters.
    model = optimization_nn_model(x_train.shape[1],
        learning_rate=learning_rate,
        num_dense_layers=num_dense_layers,
        num_dense_nodes=num_dense_nodes,
        drop=drop,
        initializer=initializer,
        activationfn=activationfn,
        output_activation=output_activation,
        lossfn=lossfn
        )


    with tf.device("/device:CPU:0"):
        reduce_lr = ReduceLROnPlateau(min_lr=1e-7, verbose=1, cooldown=10,patience=5)
        early_stop = EarlyStopping(monitor="loss",
                                min_delta=0,
                                patience=10,
                                verbose=1,
                                mode="auto")
        # Use Keras to train the model.
        history = model.fit(x= x_train,
                            y= y_train,
                            epochs=EPOCHS,
                            batch_size=batch_size,
                            validation_data=(x_val,y_val),
                            callbacks=[reduce_lr, early_stop]
                            #callbacks=[callback_log])
    )
    # Get the classification accuracy on the validation-set
    # after the last training-epoch.
    accuracy = history.history['val_accuracy'][-1]

    # Print the classification accuracy.
    print()
    print("Accuracy: {0:.2%}".format(accuracy))
    print()

    # Delete the Keras model with these hyper-parameters from memory.
    del model

    # Clear the Keras session, otherwise it will keep adding new
    # models to the same TensorFlow graph each time we create
    # a model with a different set of hyper-parameters.
    K.clear_session()

    # NOTE: Scikit-optimize does minimization so it tries to
    # find a set of hyper-parameters with the LOWEST fitness-value.
    # Because we are interested in the HIGHEST classification
    # accuracy, we need to negate this number so it can be minimized.
    return -accuracy


def optimize_training(traindir, scalerfile="default_scaler.pkl"):
    """
    Train different deep neural networks with varying hyperparameters,
    print the best score and corresponding parameters.

    Argument traindir is the path to the training data
    """


    search_result = gp_minimize(func=fitness,
                            dimensions=OPT_DIMENSIONS,
                            acq_func='EI', # Expected Improvement.
                            n_calls=20,
                            x0=OPT_DEFAULT_PARAMETERS,
                            n_jobs=-1)

    print(type(search_result))
    print(search_result)
    print("\n\n******************************************\n")
    print("Best score: ",search_result.fun)
    print("\tOptimal parameters: ", search_result.x)
    print("******************************************\n")
    # --- Write results to file
    # --- In the Optimization folder (code/neuralnet/Optimization) the result
    # --- files are named "opt_nn_X", where X is the next number in line of
    # --- optimizations.  The code below finds the highest optimization number
    # --- and adds one for this optimization.
    OPT_FILE_NAME_STANDARD = 'opt_nn_'
    opt_files = [f for f in os.listdir(OPT_DIR) if os.path.isfile(os.path.join(OPT_DIR, f))]
    highest_opt_nr = max([eval(s[-1]) for s in opt_files if not s[-1].isalpha()])

    file_name = os.path.join(OPT_DIR,OPT_FILE_NAME_STANDARD + str(highest_opt_nr + 1))

    # --- First get the scaler object, recreate if it doesn't exist or if required
    if os.path.exists(file_name):
        print("Filename already exist")
        exit()
    else:
        with open(file_name, 'wb') as hfile:
            pickle.dump(search_result, hfile, pickle.HIGHEST_PROTOCOL)





def load_optimization_result(number=-1):

    # --- Write results to file
    # --- In the Optimization folder (code/neuralnet/Optimization) the result
    # --- files are named "opt_nn_X", where X is the next number in line of
    # --- optimizations.  The code below finds the highest optimization number
    # --- and adds one for this optimization.
    OPT_FILE_NAME_STANDARD = 'opt_nn_'

    if number == -1:
        opt_files = [f for f in os.listdir(OPT_DIR) if os.path.isfile(os.path.join(OPT_DIR, f))]
        highest_opt_nr = max([eval(s[-1]) for s in opt_files if not s[-1].isalpha()])

        file_name = os.path.join(OPT_DIR,OPT_FILE_NAME_STANDARD + str(highest_opt_nr))
    elif number >= 0:
        file_name = os.path.join(OPT_DIR,OPT_FILE_NAME_STANDARD + str(number))
    else:
        print("Must give a number larger than -1")

    if os.path.exists(file_name):
        with open(file_name, 'rb') as hfile:
                search_result = pickle.load(hfile)
    else:
        print("opt_file does not exist")


    print("\n\n******************************************")
    print("******************************************")
    print("Opened optimization file: ", file_name)
    print("Results:")
    print("Best score: ",search_result.fun)
    print("\tOptimal parameters: ", search_result.x)
    print("******************************************\n\n")

    print("Plotting convergence\n\n")
    plot_convergence(search_result)
    plt.show()


    print("\n\n******************************************")
    print("Printing all results sorted from best accuracy to lowest")
    print(sorted(zip(search_result.func_vals, search_result.x_iters)))
    print("******************************************\n\n")

    dim_names = ['batch_size','num_dense_nodes']
    fig, ax = plot_objective(result=search_result, plot_dims=dim_names)
    plt.show()


if __name__ == '__main__':

    plt.style.use("sneu.mplstyle")

    PARSER = ArgumentParser(description="Optimize hyperparameters of a networks to separate signal "\
                                        "from background")

    PARSER.add_argument('-opt', '--optimize', action="store_true")

    PARSER.add_argument('-lopt', '--load_optimize', action="store_true")

    PARSER.add_argument('-plot', "--plot", action="store_true", default=False,
                        help="Plot result.")

    PARGS = PARSER.parse_args()


    PLOT = PARGS.plot

    if PARGS.optimize:
        optimize_training(TRAIN_DIR, scalerfile=SCALER)

    if PARGS.load_optimize:
        load_optimization_result()





