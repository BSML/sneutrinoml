# pylint: disable=C0303
"""
This module trains a classifier on a the dataset given in global_settings (run
with option --train), and can be used to predict (-pred) and make templates
(-temp) on balanced data.
"""

import os, sys
import warnings
from argparse import ArgumentParser
import pickle
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.models import load_model
import pandas as pd
import matplotlib.pyplot as plt
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier

from MLE_fit import kdes_from_arrays
from plots import plot_predictions
from utilities import get_dataset_from_path, make_templates, predict

from nn_model import train

from global_settings_nn import (EPOCHS, BATCH_SIZE, VERBOSE, NAME, MODELFILE,
        SCALER, TEMPLATE_DIR, PRED_DIR)

from global_settings import (TRAIN_DIR, TEST_DIR, TEMP_DATA, FEATURES_TO_USE,
        LABELDICT)

configuration = tf.compat.v1.ConfigProto(device_count={"GPU": 1},intra_op_parallelism_threads=12)
session = tf.compat.v1.Session(config=configuration)
warnings.filterwarnings("ignore")


if __name__ == '__main__':

    plt.style.use("sneu.mplstyle")

    PARSER = ArgumentParser()

    PARSER.add_argument('-train', '--train', action="store_true",
                        help='Train network')

    PARSER.add_argument('-plot', "--plot", action="store_true", default=False,
                        help="Plot result.")

    PARSER.add_argument('-pred', '--predict', action="store_true",
                        help="Predict using network")

    PARSER.add_argument('-temp', '--templates', action="store_true",
                        help="Make templates for the two classes")

    PARSER.add_argument('-point', '--point', default="12",
                        help="Which point to test on? Input format: 00, 12,.., 50")

    PARGS = PARSER.parse_args()


    POINT = f"point{PARGS.point}"

    TEST_DIR = os.path.join(TEST_DIR, POINT)

    PLOT = PARGS.plot

    if PARGS.train:
        print("Training new model on data from", TRAIN_DIR)
        print("Name tag:", NAME)

        train(TRAIN_DIR, NAME, scalerfile=SCALER, recreate_scaler=False)

        print("Done.")

    if PARGS.predict:
        print(f"Predicting model {MODELFILE} on the files in {TEST_DIR}")

        if not os.path.isdir(PRED_DIR):
            os.makedirs(PRED_DIR)

        MODEL = load_model(MODELFILE)

        PRED_DIR = os.path.join(PRED_DIR, POINT)


        X_DATA, Y_DATA, _ = get_dataset_from_path(TEST_DIR)

        PREDS_NAME = f"y_pred_{NAME}_{POINT}"
        TRUE_NAME = f"y_true_{NAME}_{POINT}"

        if not os.path.isdir(PRED_DIR):
            os.makedirs(PRED_DIR)

        Y_PRED = predict(MODEL, X_DATA, scalerfile=SCALER)

        # --- Evaluate predictions
        Y_PRED_ACC = [np.round(_value) for _value in Y_PRED]
        ACCURACY = accuracy_score(Y_DATA, Y_PRED_ACC)
        print("Accuracy: %.2f%%" % (ACCURACY * 100.0))
        # ---

        print("True mixture parameter: ", 1.0*sum(Y_DATA) / len(Y_DATA))
        print("Predicted mixture parameter: ", (1.0*sum(Y_PRED)/len(Y_PRED))[0])

        SAVE_PREDS = os.path.join(PRED_DIR, PREDS_NAME)
        np.save(SAVE_PREDS, np.array(Y_PRED))

        SAVE_TRUE = os.path.join(PRED_DIR, TRUE_NAME)
        np.save(SAVE_TRUE, np.array(Y_DATA))

        print(f"Saved predictions and targets in\n{SAVE_PREDS}\n{SAVE_TRUE}")

        if PLOT:
            plot_predictions(Y_DATA, Y_PRED, show=True)

    if PARGS.templates:
        print("Making templates...\n")

        if not os.path.isdir(TEMPLATE_DIR):
            os.makedirs(TEMPLATE_DIR)

        MODEL = load_model(MODELFILE)

        TEMPLATES, TARGETS = make_templates(MODEL, TEMP_DATA, scalerfile=SCALER)
        TARGETDICT = {0 : "class0",
                      1 : "class1"}

        for _target, _template in zip(TARGETS, TEMPLATES):

            _targetname = TARGETDICT[_target]
            _templatename = os.path.join(TEMPLATE_DIR, f"template_{_targetname}_{NAME}")
            np.save(_templatename, np.array(_template))

        print(f"\nMade templates from model {MODELFILE}, using data from "\
              f"{TEMP_DATA}.\nStored result in {TEMPLATE_DIR}.")

        if PLOT:
            LABELS = [LABELDICT.get(f"{_label}_nn", _label) for _label in TARGETS]
            KDES = kdes_from_arrays(TEMPLATES, plot=True, labels=LABELS)
            plt.show()
