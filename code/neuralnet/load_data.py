# pylint: disable=C0303

"""
Load data from files to the trining and testing of the model
"""

import warnings
warnings.filterwarnings("ignore")

import os
import sys
import socket
from argparse import ArgumentParser

import h5py
import numpy as np
from collections import namedtuple

from sklearn.model_selection import train_test_split

#from cross_section_store import get_detector_XS
from constants import CONST


# Datatype for storing a pair of X and y
Xy_pair = namedtuple('Xy_pair', ['X', 'y'])


# ----- Get data from files -----

def get_signal_datasets_from_files(filelist, targetlist, data_contains_targets=False):
    """
    Open npy files as listed in filelist,
    return a dictionary with X and y pairs wihch are numpy arrays; X with data from files and Y with
    targets as specified by 'targetlist'.

    TODO:
    If select_features is specified, a subset of all available
    features are returned:
        select_features = 'basic': Return 4-vectors, MET, phistar
    """

    if not isinstance(filelist, list):
        filelist = [filelist]

    Xy_dict = dict()
    features = None

    print('Opening files:')
    for index, fin in enumerate(filelist):

        # --- Get Processname ---
        process = fin.split('.')[0].split('/')[-1]

        # --- For h5py datafiles
        if fin.endswith(".h5"):
            hf_fin = h5py.File(fin, 'r')
            data = hf_fin.get('data')

            if features is None:
                features = np.char.decode(data.attrs['features'])
                new_features = np.char.decode(data.attrs['features'])
            else:
                new_features = np.char.decode(data.attrs['features'])

            data = np.array(data)

            if any(features != new_features):
                print("Error: Provided datafiles have different features. Exiting")
                sys.exit()

        # --- For numpy files
        if fin.endswith(".npy"):
            data = np.load(fin)
            # TODO: fix the features thing
            features = new_features = []
            #if features is None:
            #    features = data.dtype.names
            #    new_features = data.dtype.names
            #else:
            #    new_features = data.dtype.names

        # --- Scale data relative to XS ---
        if not data_contains_targets:
            target = np.ones(data.shape[0]) * targetlist[index]

            Xy_dict[process] = Xy_pair(X=data, y=target)
            print ("Got data from {0}, containing {1} events with target {2}."
                   .format(fin, data.shape[0], targetlist[index]))

        if data_contains_targets:

            _X = np.delete(data, -1, axis=1)
            _y = np.array(data[:, -1])

            Xy_dict[process] = Xy_pair(X=_X, y=_y)

    # TODO: make function for picking out features
    #X, features = select_features(X, features)

    return Xy_dict, features


# ------ Preprocess data ------

def get_reference_xs(processes_dict,xs_det,train_test_ratio = CONST.DEFAULT_RATIO):
    """Get the maximized reference cross-section"""
    # Initial values to start out with
    xs_ref = 0.0001 # Small number
    N = 10000000 #Large number

    # Find the maximal number of events we can use
    # to ensure scaling relative to the individual XS
    for process, data in processes_dict.iteritems():
        _nr_of_events = data.y.shape[0]
        if _nr_of_events*train_test_ratio < N * xs_det[process] / xs_ref:
            N = _nr_of_events * train_test_ratio
            xs_ref = xs_det[process]

    # Return the reference process XS and the number of
    # events from the reference process
    return xs_ref, N


def train_test_split_processes(signals, xs_det, train_test_ratio=CONST.DEFAULT_RATIO):
    '''
    Splits the process tuples (X,y) into training and testing sets.
    The input and ouput is dictionary of all the processes; process : (X,y)
    '''

    # Dictinaries to stor the tuples
    train_dict = {}
    test_dict = {}

    # Get the reference XS and nr of events
    xs_ref, N = get_reference_xs(signals, xs_det, train_test_ratio=train_test_ratio)

    # Looping through all signlas and making the X,y pairs
    for process, (data, targets) in signals.iteritems():

        # Number of trainig events for this process
        train_size = int(N*xs_det[process]/xs_ref)

        X_train, X_test, y_train, y_test = train_test_split(data, targets,
                                                            train_size=train_size,
                                                            random_state=0)

        train_dict[process] = Xy_pair(X=X_train, y=y_train)
        test_dict[process] = Xy_pair(X=X_test, y=y_test)

    return train_dict, test_dict


def transform_to_Xy(signals):
    '''Transform an dictionary of the processes events into an X and y matrix'''

    X = np.array([])
    y = np.array([])

    for process, (data, target) in signals.iteritems():

        if len(X):
            assert data.shape[1] == X.shape[1]
            X = np.vstack((X, data))
            y = np.append(y, target)
        else:
            X = data
            y = target

    return X, y

# ------ Utilities ------
def get_data_filenames(DATA_DIR, filetype=".h5"):
    '''Returns the datafile_names in the directory DATA_DIR and the targetlist
       depending on signal or not signal in filename.
       INPUT: DATA_DIR, filetype (default .h5)
       RETURN: list of the .h5 files in the
    '''
    file_list = []
    targets = []

    for filename in os.listdir(DATA_DIR):

        filename = os.path.join(DATA_DIR, filename)

        if filename.endswith(filetype):
            file_list.append(filename)

            if "signal" in filename:
                targets.append(1)
            else:
                targets.append(0)

    return file_list, targets

def select_features(data, feature_names_in_data):
    """
    Select certain columns from a data vector, returns a new numpy array

    feature_names_in_data: List of the names of the columns in 'data'
    """

    # --- Available features
    features_to_use = ["n_jets",
                       "jet1_pt", "jet1_phi", "jet1_eta",
                       "n_taus",
                       "tau1_pt", "tau1_phi", "tau1_eta", "tau1_charge",
                       "n_muons",
                       "muon1_pt", "muon1_phi", "muon1_eta", "muon1_charge",
                       "n_electrons",
                       "electron1_pt", "electron1_phi", "electron1_eta", "electron1_charge",
                       "MET", "generatorweight", "ht","MET_phi"
                       ]

    # --- In case we extend to use all features
    #features_to_use= ["n_jets",
    #                  "jet1_pt", "jet1_phi", "jet1_eta",
    #                  "jet2_pt", "jet2_phi", "jet2_eta",
    #                  "jet3_pt", "jet3_phi", "jet3_eta",
    #                  "n_taus",
    #                  "tau1_pt", "tau1_phi", "tau1_eta", "tau1_charge",
    #                  "tau2_pt", "tau2_phi", "tau2_eta", "tau2_charge",
    #                  "n_muons",
    #                  "muon1_pt", "muon1_phi", "muon1_eta", "muon1_charge",
    #                  "muon2_pt", "muon2_phi", "muon2_eta", "muon2_charge",
    #                  "n_electrons",
    #                  "electron1_pt", "electron1_phi", "electron1_eta", "electron1_charge",
    #                  "electron2_pt", "electron2_phi", "electron2_eta", "electron2_charge",
    #                  "MET", "generatorweight", "ht"
    #                 ]

    features_to_use = np.array(features_to_use)

    cols = []
    for name in features_to_use:
        if name in feature_names_in_data:
            cols.append(np.where(feature_names_in_data == name)[0][0])

    data = data[:, cols]

    return data, features_to_use


# ------ Get out the datsets -----

def get_Xy_train_test(filelist,
                      targetlist,
                      data_contains_targets=False,
                      train_test_ratio=CONST.DEFAULT_RATIO):

    signal_datasets, features = get_signal_datasets_from_files(
        filelist, targetlist, data_contains_targets)

    # --- Get process XS ----
    xs_det = get_detector_XS()
    # TODO: Scale process data relative to XS

    train_dict, test_dict = train_test_split_processes(
        signal_datasets, xs_det, train_test_ratio=train_test_ratio)

    X_train, y_train = transform_to_Xy(train_dict)
    X_test, y_test = transform_to_Xy(test_dict)

    return X_train, y_train, X_test, y_test


def get_test_dataset(filelist, targetlist, data_contains_targets=False, train_test_ratio=CONST.DEFAULT_RATIO):

    signal_datasets, features = get_signal_datasets_from_files(
        filelist, targetlist, data_contains_targets)

    # --- Get process XS ----
    # xs_det = get_detector_XS()
    # TODO: Scale process data relative to XS

    #train_dict, test_dict = train_test_split_processes(
    #    signal_datasets, xs_det, train_test_ratio=train_test_ratio)

    return signal_datasets


def get_train_dataset(filelist, targetlist, data_contains_targets=False, train_test_ratio=CONST.DEFAULT_RATIO):

    signal_datasets, features = get_signal_datasets_from_files(
        filelist, targetlist, data_contains_targets)

    # --- Get process XS ----
    #xs_det = get_detector_XS()
    # TODO: Scale process data relative to XS

    #train_dict, test_dict = train_test_split_processes(
    #    signal_datasets, xs_det, train_test_ratio=train_test_ratio)

    return signal_datasets


if __name__ == '__main__':

    PARSER = ArgumentParser(description='Test loading of data from file or a directory of files')
    PARSER.add_argument('-trainratio', '--trainratio', help='Train size ratio', type=float, default=CONST.DEFAULT_RATIO)
    PARSER.add_argument('-f', '--filedir', help='File directory', type=str, default="data/new_post-cuts")
    PARGS = PARSER.parse_args()

    HOSTNAME = socket.gethostname()

    if HOSTNAME == "ift040070":
        ROOT_DIR = "/scratch3/sneutrinoML/code"
    if HOSTNAME == "Daniels-Macbook.local":
        ROOT_DIR = "/Users/Daniel/Developer/Projects/sneutrinoML/code"
    if HOSTNAME == "flua":
        ROOT_DIR = "/home/strumke/PhD/programs/dev/sneutrinoML/code"

    DATA_DIR = os.path.join(ROOT_DIR, PARGS.filedir)

    filelist, targetlist = get_data_filenames(DATA_DIR)
    x,y,x2,y2 = get_Xy_train_test(filelist, targetlist, train_test_ratio=PARGS.trainratio)
    train_dict = get_train_dataset(filelist,targetlist, train_test_ratio=PARGS.trainratio)
    test_dict = get_test_dataset(filelist,targetlist, train_test_ratio=PARGS.trainratio)
    print("Train and test data shapes: \n\tX_train: {0} \n\ty_train: {1}"\
          "\n\tX_test: {2}\n \ty_test: {3}".format(x.shape,y.shape,x2.shape,y2.shape))
    print("\nand this is how they distribute over the individual processes:")
    print("Train set")
    for p,d in train_dict.iteritems():
        print("\t" + p + " : " + str(d.X.shape[0]))

    print("\nTest set")
    for p, d in test_dict.iteritems():
        print("\t" + p + " : " + str(d.X.shape[0]))



