"""
Find mixture parameter for the two-components model using a max log likelihood
fit of the class 0 and class 1 templates
"""

import sys
import os
import pickle
import re
import glob
import numpy as np
import matplotlib.pyplot as plt
import scipy

from nn_kde import nn_kde
from utilities import format_alpha
from plots import plot_kdes
from global_settings import COLORS, LINESTYLES, LABELDICT, POINTS

from global_settings_xg import (NAME as NAME_xg, PRED_DIR as PRED_DIR_xg,
                                TEMPLATE_DIR as TEMPLATE_DIR_xg)
from global_settings_nn import (NAME as NAME_nn, PRED_DIR as PRED_DIR_nn,
                                TEMPLATE_DIR as TEMPLATE_DIR_nn)

def open_nikofile(filename):
    xs = []
    with open(filename) as _f:
        for line in _f:
            xs.extend(list(map(float,line.split(','))))
    return xs




def nll(mixpar, kdes, predictions):
    """
    Negative log likelihood function for a one-dimensional mixture model
    The mixture parameter is the prevalence of class described by kde_1
    """

    kde_0 = kdes[0]
    kde_1 = kdes[1]

    eval_1 = kde_1.evaluate(predictions)
    eval_0 = kde_0.evaluate(predictions)

    return -np.sum(
        np.logaddexp(
            np.log(mixpar) + np.log(eval_1),
            np.log1p(-mixpar) + np.log(eval_0)
            )
        )


def nll_weighted(mixpar, kdes, predictions, weights):
    """
    Negative log likelihood function for a one-dimensional mixture model
    The mixture parameter is the prevalence of class described by kde_1
    """

    kde_0 = kdes[0]
    kde_1 = kdes[1]

    return -np.sum(
        weights * np.logaddexp(
            np.log(mixpar) + np.log(kde_1.evaluate(predictions)),
            np.log1p(-mixpar) + np.log(kde_0.evaluate(predictions))
            )
        )


def kdes_from_arrays(arrays, plot=False, labels=["Unlabeled 0", "Unlabeled 1"], colors=COLORS):
    """
    Creates KDES per array in arrays
    Used by neuralnet.py and xgboost_model.py
    """

    kdes = make_nn_kdes(arrays, verbose=False, plot=plot, labels=labels, colors=colors)

    return kdes

def kdes_from_files(files, plot=False, labels=["Unlabeled 0", "Unlabeled 1"]):
    """
    Send arrays from files to kdes_from_arrays
    """

    templates = [np.load(_file) for _file in files]

    return kdes_from_arrays(templates, plot=plot, labels=labels)


def make_nn_kdes(arrays, verbose=True, plot=False,
        labels=["Unlabeled 0", "Unlabeled 1"], colors=COLORS):
    """
    Make kdes from nn_kde class NnKde using bf bandwidth values
    This function assumes that the input arrays are sorted [class0, class1]
    """

    bw_0 = 0.01
    bw_1 = 0.01

    kdes = [
        nn_kde.NnKde(bandwidth=bw_0,
                     verbose=verbose).fit(arrays[0]),
        nn_kde.NnKde(bandwidth=bw_1,
                     verbose=verbose).fit(arrays[1])
           ]

    if plot:
        plot_kdes(kdes, labels, colors=colors)

    return kdes


def make_loglike_curve(bf_alpha, alphas, kdes, predictions, point, name):

    """
    Make the negative loglike curve (the parabola) for a list of alphas, samples and kdes
    """

    nll_dir = f"nlls/{name}/"

    if not os.path.exists(nll_dir):
        os.makedirs(nll_dir)

    chi2_file = os.path.join(nll_dir, f"chi2_logspace_{point}_{len(alphas)}_alphas.npy")

    while True:
        try:
            chi2 = np.load(chi2_file)

        except IOError:
            # --- Save -2ll = chi2
            chi2 = 2*np.array([nll(_alpha, kdes, predictions) for _alpha in alphas])
            np.save(chi2_file, chi2)
        break


    # --- Find best fit values
    bf_chi2 = min(chi2)
    chi2 = chi2 - bf_chi2
    bf_chi2 = 0

    # --- Find 95% cl (at nll=4)
    idx = (np.abs(chi2-4)).argmin()
    minus_alpha = alphas[idx]


    if "xg" in name.lower():
        plt.scatter(bf_alpha, bf_chi2, marker='o',color=COLORS[1])
        plt.plot(alphas, chi2, color=COLORS[1], linestyle="--", label="XGBoost")
        print("Z(XGBoost): ", np.sqrt(chi2[0]))

    if "nn" in name.lower():
        plt.scatter(bf_alpha, bf_chi2, marker='o',color=COLORS[2])
        plt.plot(alphas, chi2, color=COLORS[2], label="DNN")
        print("Z(DNN): ", np.sqrt(chi2[0]))

    print("===================================================")
    diff = np.abs(bf_alpha-minus_alpha)
    print(f"Classifier: {name}")
    print(f"Best-fit mixture parameter: {bf_alpha}")
    print(f"Uncertainty: +- {diff}")
    print(f"typeset: {round(bf_alpha,4)} \pm {round(diff,3)}")
    print("===================================================")

def sigmas_and_alphas(sigma_max=5):
    """
    Decoration for loglike plot
    """

    xlow, xhigh = -0.2, 1.2

    # --- Gray shade for alpha
    _ax = plt.gca()
    _ax.axvspan(xlow, 0, alpha=0.15, color='gray')
    _ax.axvspan(1, xhigh, alpha=0.15, color='gray')
    # --- Plot sigma lines
    if sigma_max > 0:
        for _s in range(1, sigma_max+1):
            plt.hlines(_s*_s,
                       xmin=xlow, xmax=xhigh,
                       color="gray",
                       linestyle='--',
                       alpha=0.7,)
            plt.text(0.15, _s*_s+0.5, r"{0} $\sigma$".format(_s), color="gray")

    plt.xlim(xlow, xhigh)
    plt.ylim([-0.5, 30])


def load_preds(path_preds, path_true, verbose=True):
    """
    Loading and return predictions, targets and true mixture parameter
    """

    y_true = np.load(path_true)
    true_alpha = 1.0*sum(y_true)/len(y_true)
    n_samples= len(y_true)

    assert os.path.isfile(path_preds), f"Couldn't find predictions at {path_preds}"

    if verbose:
        print(f"Using {n_samples} samples with true mixture parameter {true_alpha}")
        print(f"Fitting to network predictions in:\n{path_preds}")

    predictions = np.load(path_preds).flatten()

    return predictions, y_true, true_alpha

if __name__ == "__main__":

    plt.style.use("sneu.mplstyle")

    from argparse import ArgumentParser

    PARSER = ArgumentParser(description="Template fits")

    PARSER.add_argument('-xg', "--xgboost", action="store_true",
                        help="Run with XGBoost model")

    PARSER.add_argument('-nn', "--nn", action="store_true",
                        help="Run with Neural Network model")

    PARSER.add_argument('-fit', "--fit", action="store_true",
                        help="Make template fit.")

    PARSER.add_argument('-plot', "--plot", action="store_true",
                        help="Plot result.")

    PARSER.add_argument("-ll", "--loglike", action="store_true",
                        help="Calculate negative loglikelihood for fit on "\
                        "different mixture parameters and plot the result.")

    PARGS = PARSER.parse_args()

    PLOT = PARGS.plot

    # --- Select model
    if PARGS.xgboost and PARGS.nn:
        NAME = [NAME_xg, NAME_nn]
        PRED_DIR = [PRED_DIR_xg, PRED_DIR_nn]
        TEMPLATE_DIR = [TEMPLATE_DIR_xg, TEMPLATE_DIR_nn]
        print("Using XGBoost and neural network")

    elif PARGS.xgboost:
        NAME = [NAME_xg]
        PRED_DIR = [PRED_DIR_xg]
        TEMPLATE_DIR = [TEMPLATE_DIR_xg]
        print("Using XGBoost")

    elif PARGS.nn:
        NAME = [NAME_nn]
        PRED_DIR = [PRED_DIR_nn]
        TEMPLATE_DIR = [TEMPLATE_DIR_nn]
        print("Using neural network")
    else:
        assert False, "Need to specify model: -xg or -nn"

    # ---------------------------------------------------------------------
    print("Assuming the predictions from the classifier exist! "\
            "Please specify which predictions to fit on:")

    while True:
        try:
            _POINT = input("Point to use, e.g. 00, 01, 12, 19: ")
            POINT = f"point{_POINT}"

            if not POINT in POINTS:
                raise ValueError("Misspelled input")

        except ValueError:
            print("That point doesn't exist. Please choose from\n", POINTS)

            continue
        break
    # ---------------------------------------------------------------------

    if PLOT:
        fig = plt.figure()
        plotname = f"nll_{POINT}"


    for _NAME, _PRED_DIR, _TEMPLATE_DIR in zip(NAME, PRED_DIR, TEMPLATE_DIR):
        if PLOT:
            plotname = '_'.join((plotname, _NAME))

        # ---------------------------------------------------------------------
        print(f"Creating KDEs from {_TEMPLATE_DIR} ...")

        FILES = [os.path.join(_TEMPLATE_DIR, _file) for _file in
                 os.listdir(_TEMPLATE_DIR)]

        LABELS = [re.findall("template_(.*?).npy", _file)[0] for _file in
                  os.listdir(_TEMPLATE_DIR)]

        LABELS = [LABELDICT.get(_label, _label) for _label in LABELS]

        KDES = kdes_from_files(FILES, labels=LABELS,
                plot=(PLOT and not PARGS.loglike))

        PATH_PRED = os.path.join(_PRED_DIR, POINT, f"y_pred_{_NAME}_{POINT}.npy")

        PATH_TRUE = os.path.join(_PRED_DIR, POINT, f"y_true_{_NAME}_{POINT}.npy")

        assert os.path.isfile(PATH_PRED) and os.path.isfile(PATH_TRUE), \
                "Predictions for these parameters don't exist."


        PREDICTIONS, Y_TRUE, TRUE_ALPHA = load_preds(PATH_PRED, PATH_TRUE, verbose=False)
        print("True mixture parameter: ", TRUE_ALPHA)

        # ---------------------------------------------------------------------

        if PARGS.fit or PARGS.loglike:

            # --- Minimise the negative log likelihood (or load if it exists)
            _nllsdir = os.path.join("nlls", _NAME)
            fitted_alpha_file = os.path.join(_nllsdir, f"opt_alpha_{POINT}.pkl")
            if not os.path.isdir(_nllsdir):
                os.makedirs(_nllsdir)

            if os.path.exists(fitted_alpha_file):
                with open(fitted_alpha_file, 'rb') as _f:
                    FITTED_ALPHA = pickle.load(_f)

            else:
                OPT_ALPHA = scipy.optimize.minimize(nll, 0.5, args=(KDES, PREDICTIONS),
                                                 bounds=((0.001, 0.999),))

                print("Result from scipy minimization of mixture parameter:")
                print(OPT_ALPHA)

                FITTED_ALPHA = OPT_ALPHA.x[0]
                with open(fitted_alpha_file, 'wb') as _f:
                    pickle.dump(FITTED_ALPHA, _f)

            print(f"\nFitted mixture parameter: {FITTED_ALPHA}\n")

        # ---------------------------------------------------------------------

        if PARGS.loglike:
            # --- Close in around a small best-fit point
            _DOWN = (np.logspace(0,np.log10(11),50)-1)/10*FITTED_ALPHA
            _UP =  np.logspace(np.log10(FITTED_ALPHA),np.log10(0.3))

            TEST_ALPHAS = np.append(_DOWN, _UP)

            PATH_PREDS = [os.path.join(_PRED_DIR, POINT,
                                     f"y_pred_{_NAME}_{POINT}.npy")]

            PATH_TRUES = [os.path.join(_PRED_DIR, POINT,
                                     f"y_true_{_NAME}_{POINT}.npy")]

            for PATH_PRED, PATH_TRUE in zip(PATH_PREDS, PATH_TRUES):

                PREDICTIONS, Y_TRUE, TRUE_ALPHA = load_preds(PATH_PRED, PATH_TRUE, verbose=False)

                print(f"Making log likelihood curve for {len(TEST_ALPHAS)}"\
                        f"mixture parameters in the range {TEST_ALPHAS[0]}-{TEST_ALPHAS[-1]},"\
                        f"using predictions {PATH_PRED}")

                if PLOT:
                    make_loglike_curve(FITTED_ALPHA, TEST_ALPHAS, KDES, PREDICTIONS, POINT, _NAME)

            if PLOT:
                sigma_max = 5
                sigmas_and_alphas(sigma_max)
                ymax = sigma_max*sigma_max

                plt.xlim([-0.07, 0.2])
                plt.ylim([-0.5, ymax+5])

                plt.xlabel(r"Mixture parameter $\alpha$")
                plt.ylabel(r"$-2\mathrm{log}\mathcal{L}$")


    if PLOT:
        plt.vlines(TRUE_ALPHA,
                ymin=0, ymax=ymax+5,
                color="red",
                linestyle='dotted',
                alpha=0.7,
                label=r"$\alpha_{{\mathrm{{true}}}}$")


        plt.legend(loc="upper left")
        plotname = '.'.join((plotname, "pdf"))
        fig.savefig(plotname, bbox_inches='tight')
        plt.show()
