import argparse
import sys, string, os, json
from ROOT import TFile, TNtuple, TTree
from array import array



parser = argparse.ArgumentParser(description='Read data from Sherpa/Herwig outputs and save it as Root TTrees')
parser.add_argument("--inputPath", help = 'location of raw data')
parser.add_argument("--output", help = 'locataion of output')


args = parser.parse_args()
print args.inputPath


ofn = 'ntuple.root'
if args.output:
   ofn=args.output   

datafile = "data.txt"
if args.inputPath:
    datafile=args.inputPath
infile = open(datafile, 'r')


#lines = infile.readlines()
lines = infile.read().splitlines()
counter = 0;
print "Writing file", ofn
#print lines
n = 0
evt = array('i', [0])
met = array('f', [0.])
weight = array('f', [0.])
ht = array('f', [0.])

jet_n = array( 'i', [0])
jet_pt = array( 'f',100*[0.])
jet_eta = array( 'f',100*[0.])
jet_phi = array( 'f',100*[0.])

mu_n = array( 'i', [0])
mu_pt = array( 'f',100*[0.])
mu_eta = array( 'f',100*[0.])
mu_phi = array( 'f',100*[0.])
mu_charge = array( 'i',100*[0])

el_n = array( 'i', [0])
el_pt = array( 'f',100*[0.])
el_eta = array( 'f',100*[0.])
el_phi = array( 'f',100*[0.])
el_charge = array( 'i',100*[0])

tau_n = array( 'i', [0])
tau_pt = array( 'f',100*[0.])
tau_eta = array( 'f',100*[0.])
tau_phi = array( 'f',100*[0.])
tau_charge = array( 'i',100*[0])




outfile = TFile(ofn, 'RECREATE', 'ROOT file with an NTuple')
tree = TTree("tree", "Tree with data")
tree.Branch("Event_number", evt, "Event_number/I")
tree.Branch("met", met, "met/F")
tree.Branch("ht", ht, "ht/F")
tree.Branch("weight", weight, "weight/F")


tree.Branch("jet_n", jet_n, "jet_n/I")
tree.Branch("jet_pt", jet_pt, "jet_pt[jet_n]/F")
tree.Branch("jet_eta", jet_eta, "jet_eta[jet_n]/F")
tree.Branch("jet_phi", jet_phi, "jet_phi[jet_n]/F")

tree.Branch("tau_n", tau_n, "tau_n/I")
tree.Branch("tau_pt", tau_pt, "tau_pt[tau_n]/F")
tree.Branch("tau_eta", tau_eta, "tau_eta[tau_n]/F")
tree.Branch("tau_phi", tau_phi, "tau_phi[tau_n]/F")
tree.Branch("tau_charge", tau_charge, "tau_charge[tau_n]/I")

tree.Branch("mu_n", mu_n, "mu_n/I")
tree.Branch("mu_pt", mu_pt, "mu_pt[mu_n]/F")
tree.Branch("mu_eta", mu_eta, "mu_eta[mu_n]/F")
tree.Branch("mu_phi", mu_phi, "mu_phi[mu_n]/F")
tree.Branch("mu_charge", mu_charge, "mu_charge[mu_n]/I")

tree.Branch("el_n", el_n, "el_n/I")
tree.Branch("el_pt", el_pt, "el_pt[el_n]/F")
tree.Branch("el_eta", el_eta, "el_eta[el_n]/F")
tree.Branch("el_phi", el_phi, "el_phi[el_n]/F")
tree.Branch("el_charge", el_charge, "el_charge[el_n]/I")

for line in lines:
    evt[0] = n
    n = n+1
    values = line.split("|")
    if n%1000==0:
        print n," events processed"

    met[0] = float(values[4])
    weight[0] = float(values[5])
    ht[0] = float(values[6])

    jet_counter = 0
    jet_n[0] = len(json.loads(values[0]))
    for jet in json.loads(values[0]):
#        print jet
        jet_pt[jet_counter] = jet[0]
        jet_eta[jet_counter] = jet[1]
        jet_phi[jet_counter] = jet[2]
        jet_counter = jet_counter+1    
        

    tau_counter = 0
    if json.loads(values[1])[0][0] != -10:
        tau_n[0] = len(json.loads(values[1]))
    else:
        tau_n[0] = 0
    for tau in json.loads(values[1]):
#        print tau
        tau_pt[tau_counter] = tau[0]
        tau_eta[tau_counter] = tau[1]
        tau_phi[tau_counter] = tau[2]
        tau_charge[tau_counter] = tau[3]
        tau_counter = tau_counter+1


    mu_counter = 0
    if json.loads(values[2])[0][0] != -10:
        mu_n[0] = len(json.loads(values[2]))
    else:
        mu_n[0] = 0
    for mu in json.loads(values[2]):
#        print mu
        mu_pt[mu_counter] = mu[0]
        mu_eta[mu_counter] = mu[1]
        mu_phi[mu_counter] = mu[2]
        mu_charge[mu_counter] = mu[3]
        mu_counter = mu_counter+1

    el_counter = 0
    if json.loads(values[3])[0][0] != -10:
        el_n[0] = len(json.loads(values[3]))
    else:
        el_n[0] = 0
    for el in json.loads(values[3]):
#        print el
        el_pt[el_counter] = el[0]
        el_eta[el_counter] = el[1]
        el_phi[el_counter] = el[2]
        el_charge[el_counter] = el[3]
        el_counter = el_counter+1

    tree.Fill()

outfile.Write()
outfile.Close()



print "done"
