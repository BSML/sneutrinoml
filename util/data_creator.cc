// -*- C++ -*-
#include <iostream>
#include <fstream>
#include <math.h>
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/TauFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Tools/Cutflow.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class data_creator : public Analysis {
  public:

    /// Constructor
    data_creator()
	: Analysis("data_creator")
    {}


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections

    const FinalState fs(-4.,4.,10 * GeV);

    declare(FastJets(fs, FastJets::ANTIKT,0.4), "Jets");

    IdentifiedFinalState efs(fs);
    efs.acceptId(11);
    efs.acceptId(-11);
    declare(efs, "Electrons");
    IdentifiedFinalState mufs(fs);
    mufs.acceptId(13);
    mufs.acceptId(-13);
    declare(mufs, "Muons");
    MissingMomentum missing(fs);
    declare(missing, "MET");

    TauFinder taufinderHad(TauFinder::HADRONIC, Cuts::pT > 15*GeV && Cuts::abseta < 2.8);
    declare(taufinderHad, "TauFinderHad");


    }


    /// Perform the per-event analysis
    void analyze(const Event& evt) {

      /// @todo Do the event by event analysis here
    const double weight = evt.weight();
    const FastJets& jetpro = apply<FastJets>(evt, "Jets");
    Jets jets = jetpro.jetsByPt(Cuts::pT > 20 * GeV && Cuts::abseta < 2.8);
    Particles tausHad = applyProjection<TauFinder>(evt, "TauFinderHad").taus();
    sortByPt(tausHad);
    const FinalState& efs = apply<FinalState>(evt, "Electrons");
    const FinalState& mufs = apply<FinalState>(evt, "Muons");
    const MissingMomentum& met = apply<MissingMomentum>(evt, "MET");
    Particles elecs = efs.particlesByPt(Cuts::pT > 10 * GeV && Cuts::abseta < 2.8);
    Particles muons = mufs.particlesByPt(Cuts::pT > 10 * GeV && Cuts::abseta < 2.8);



   //overlap removal

    for(const Particle& m: muons)
    {
    jets = ifilter_discard(jets,deltaRLess(m,0.2,PSEUDORAPIDITY));
    }


    for(const Particle& e: elecs)
    {
    jets = ifilter_discard(jets,deltaRLess(e,0.2,PSEUDORAPIDITY));
    }

    for(const Particle& t: tausHad)
    {
    jets = ifilter_discard(jets,deltaRLess(t,0.2,PSEUDORAPIDITY));
    }


    for(const Jet& j : jets)
    {
    ifilter_discard(elecs,deltaRLess(j,0.7,PSEUDORAPIDITY));
    ifilter_discard(muons,deltaRLess(j,0.7,PSEUDORAPIDITY));
    ifilter_discard(tausHad,deltaRLess(j,0.7,PSEUDORAPIDITY));
    }


    for( const Particle& tau: tausHad)
    {
//    ifilter_discard(tausHad,deltaRLess(tau,0.4,PSEUDORAPIDITY));
    ifilter_discard(muons,deltaRLess(tau,0.4,PSEUDORAPIDITY));
    ifilter_discard(elecs,deltaRLess(tau,0.4,PSEUDORAPIDITY));
    }


//   HT, scalar sum of jets and leptons pt. 
    double ht = 0;
    foreach (const Jet& j, jets) {ht += j.momentum().pT();}
    foreach (const Particle& tau, tausHad) {ht += tau.momentum().pT();} 
    foreach (const Particle& muon, muons) {ht += muon.momentum().pT();}
    foreach (const Particle& ele, elecs) {ht += ele.momentum().pT();}



    int jetcounter = 0;
    int taucounter = 0;
    int mucounter = 0;
    int ecounter = 0;

    ofstream ntuple;
    string ntuplename = "data.txt";
    ntuple.open(ntuplename, std::ofstream::out | std::ofstream::app);

// list of lists of jets 
    if(jets.size()>0){   
    ntuple << "[";
    foreach(const Jet jet, jets){
    const FourMomentum& pj = jet.momentum();
    if(jetcounter !=0){ntuple << ",";}
    ntuple << "[" << pj.pT()/GeV << "," << mapAngleMPiToPi(pj.phi()) << "," << pj.eta() << "]";
    jetcounter++;
	}
   ntuple << "]|";
	}
   else {ntuple << "[[-10,-10,-10]]|";}

// list of lists of taus


   if(tausHad.size()>0){
    ntuple << "[";
    foreach(const Particle tau, tausHad){
    const FourMomentum& tauj = tau.momentum();
    if(taucounter !=0){ntuple << ",";}
    ntuple << "[" << tauj.pT()/GeV << "," << mapAngleMPiToPi(tauj.phi()) << "," << tauj.eta() << "," << tau.charge()<< "]";
    taucounter++;
        }
   ntuple << "]|";
	}
   else {ntuple << "[[-10,-10,-10,-10]]|";}
// list of lists of muons

   if(muons.size()>0){
    ntuple << "[";
//    foreach(const Particle muon, mufs.particles()){
    foreach(const Particle& muon, muons){
    const FourMomentum& muj = muon.momentum();
    if(mucounter !=0){ntuple << ",";}
    ntuple << "[" << muj.pT()/GeV << "," << mapAngleMPiToPi(muj.phi()) << "," << muj.eta() << "," << muon.charge()<< "]";
    mucounter++;
        }
   ntuple << "]|";
        }
   else {ntuple << "[[-10,-10,-10,-10]]|";}


//list of lists of eles

   if(elecs.size()>0){
    ntuple << "[";
//    foreach(const Particle ele, efs.particles()){
    foreach(const Particle& ele, elecs){
    const FourMomentum& ej = ele.momentum();
    if(ecounter !=0){ntuple << ",";}
    ntuple << "[" << ej.pT()/GeV << "," << mapAngleMPiToPi(ej.phi()) << "," << ej.eta() << "," << ele.charge()<< "]";
    ecounter++;
        }
   ntuple << "]|";
        }
   else {ntuple << "[[-10,-10,-10,-10]]|";}


  ntuple << met.vectorEt().mod()/GeV << "|" << weight << "|" << ht;

ntuple << "\n";
ntuple.close();



} //end of Event loop


    /// Normalise histograms etc., after the run
    void finalize() {

    ofstream fileXS;
    string filename = "xsec.txt";
    fileXS.open(filename);
    fileXS << "UPDATED " << crossSection() / femtobarn << "\n\n";


    fileXS.close();

    }


  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(data_creator);


}
