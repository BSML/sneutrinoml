
# Beyond Cuts in Small Signal Scenarios - Enhanced Sneutrino Detectability Using Machine Learning

This repository contains the code used to produce results for the paper [Beyond Cuts in Small Signal Scenarios - Enhanced Sneutrino Detectability Using Machine Learning (open access)](https://link.springer.com/article/10.1140/epjc/s10052-023-11532-9) published in European Physical Journal C.

```BibTex
@article{Alvestad2023,
    author="Alvestad, Daniel
    and Fomin, Nikolai
    and Kersten, J{\"o}rn
    and Maeland, Steffen
    and Str{\"u}mke, Inga",
    title="Beyond cuts in small signal scenarios",
    journal="The European Physical Journal C",
    year="2023",
    month="May",
    day="8",
    volume="83",
    number="5",
    pages="379",
    issn="1434-6052",
    doi="10.1140/epjc/s10052-023-11532-9",
    url="https://doi.org/10.1140/epjc/s10052-023-11532-9"
}
```

### External software used
- [SPheno](https://spheno.hepforge.org/) For SUSY mass spectra and branching ratios
- [FeynHiggs](http://www.feynhiggs.de/) For Higgs mass spectra 
- [Herwig 7](https://herwig.hepforge.org) For SUSY signal event generation 
- [Sherpa](https://sherpa-team.gitlab.io/) For Standard Model background event generation
- [Rivet](https://rivet.hepforge.org/) Event selection and object definitions 
- [FastJet](https://fastjet.fr/) For jet clustering
- [Prospino2](https://www.thphys.uni-heidelberg.de/~plehn/index.php?show=prospino) NLO cross sections
- [XGBoost](https://xgboost.readthedocs.io/en/stable/) Boosted decision tree implementation
- [TensorFlow](https://www.tensorflow.org/overview) with Keras for the neural network implementation

### Data
The data files are not uploaded here, but can be obtained by contacting the authors.

### Directory structure
- `code/`: Code for training the classifiers, constructing the KDEs, and performing the final maximum likelihood fit. 
  - `data/`: File operations
  - `neuralnet/`: The neural network model
  - `kde/`: Kernel density estimation 
  - `Shapley/`: Calculation of Shapley values
  - `spectrum/`: SUSY spectrum
- `util/`: Data processing

### Contact 
The authors can be contacted by email, addresses are listed at the [journal webpage](link.springer.com/article/10.1140/epjc/s10052-023-11532-9).





